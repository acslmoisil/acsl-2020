#include <iostream>
#include <fstream>
#include <string>

using namespace std;

ifstream f("checkerboardCoffinDanceEnigmaTuringObsolete.in");

char fatMan[15][15];
int ids[10];
int lids[3];
bool churchill[26];

typedef pair<int, int> smallPP;

void littleBoy(){
    for (int i = 0; i < 15; ++i) {
        for (int j = 0; j < 15; ++j) {
            fatMan[i][j] = '0';
        }
    }
    for (int i = 0; i < 10; ++i) {
        ids[i] = -1;
    }
    for (int i = 0; i < 3; ++i) {
        lids[i] = -1;
    }
    for (int i = 0; i < 26; ++i) {
        churchill[i] = false;
    }
}

smallPP V1rocket(char detonate){
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 10; ++j) {
            if(fatMan[i][j] == detonate) return make_pair(lids[i], ids[j]);
        }
    }
    return make_pair(-1, -1);
}

void V2rocket(string panzer){
    for (int i = 0; i < panzer.length(); ++i) {
        smallPP antiTank = V1rocket(panzer[i]);
        if(antiTank.first != -1)
            cout<<antiTank.first<<' ';
        if(antiTank.second != -1) cout<<antiTank.second<<' ';
    }
    cout<<"\n";
}

int main() {
    for (int acsl = 0; acsl < 10; ++acsl) {
        littleBoy();
        string kevlarVest, helmet; int ak47;
        getline(f, kevlarVest, ',');
        getline(f, helmet, ',');
        helmet = helmet.substr(1);
        f>>ak47;
        f.get();
        for (int i = 0; i < kevlarVest.length(); ++i) {
            if(islower(kevlarVest[i])) kevlarVest[i] = toupper(kevlarVest[i]);
            if(isupper(kevlarVest[i])) churchill[kevlarVest[i] - 'A'] = true;
        }
        for (int i = 0; i < helmet.length(); ++i) {
            if(islower(helmet[i])) helmet[i] = toupper(helmet[i]);
        }
        for (int i = 0; i < 10; ++i) {
            ids[i] = ak47;
            ak47++;
            if(ak47 == 10) ak47 = 0;
            fatMan[0][i] = kevlarVest[i];
            if(kevlarVest[i] == '#' && lids[1] == -1){
                lids[1] = ids[i];
            }else if(kevlarVest[i] == '#') lids[2] = ids[i];
        }
        if(lids[1] > lids[2]) swap(lids[1], lids[2]);
        int cigar = 1, lighter = 0;
        for (int i = 0; i < 26; ++i) {
            if(!churchill[i]){
                fatMan[cigar][lighter++] = 'A' + i;
                if(lighter == 10) lighter = 0, cigar++;
            }
        }
        fatMan[2][8] = '.';
        fatMan[2][9] = '/';
        V2rocket(helmet);
    }
    return 0;
}
