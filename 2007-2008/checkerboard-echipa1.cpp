#include <iostream>
#include <fstream>
#include <cstring>
using namespace std;
ifstream f("checker.in");
char code[20], cuv[20];
int nr;
char mat[5][15];
int fr[26];
void citire()
{
    f>>code>>cuv>>nr;
    f.get();
    int l=strlen(code);
    code[l-1]=0;
    l=strlen(cuv);
    cuv[l-1]=0;
    for(int i=0; i<4; i++)
        for(int j=0; j<=10; j++)
            mat[i][j]=0;
    for(int i=0; i<26; i++)
        fr[i]=0;
}
void creare()
{
    int n=nr;
    int poz1=-1, poz2=-1;
    for(int j=1; j<=10; j++)
    {
        mat[0][j]=n+'0';
        n++;
        if(n>9)
            n=0;
        mat[1][j]=code[j-1];
        if(mat[1][j]>='A' && mat[1][j]<='Z')
            fr[mat[1][j]-'A']++;
        if(mat[1][j]=='#')
        {
            if(poz1==-1)
                poz1=mat[0][j]-'0';
            else
                poz2=mat[0][j]-'0';
        }
    }
    if(poz1>poz2)
    {
        int aux=poz1;
        poz1=poz2;
        poz2=aux;
    }
    mat[2][0]=poz1+'0';
    mat[3][0]=poz2+'0';
    int j=1, i=2;
    for(int k=0; k<26; k++)
        if(fr[k]==0)
        {
            fr[k]++;
            mat[i][j]=k+'A';
            if(j>9)
            {
                i++;
                j=1;
            }
            else
                j++;
        }
    mat[3][9]='.';
    mat[3][10]='/';
}
void afisare()
{
    for(int i=0; i<4; i++)
    {
        for(int j=0; j<=10; j++)
            cout<<mat[i][j]<<' ';
        cout<<'\n';
    }
    cout<<'\n';
}
void solve()
{
    int l=strlen(cuv);
    for(int k=0; k<l; k++)
    {
        int i, j, ok=0;
        for(i=1; i<4 && ok<1; i++)
            for(j=1; j<=10 && ok<1; j++)
                if(mat[i][j]==cuv[k])
                    ok=1;
        if(i>2)
        {
            int c1=mat[i-1][0]-'0';
            cout<<c1<<' ';
        }
        int c2=mat[0][j-1]-'0';
        cout<<c2<<' ';
    }
    cout<<'\n';
}
int main()
{
    for(int acsl=0; acsl<10; acsl++)
    {
        citire();
        creare();
        solve();
    }
    return 0;
}
