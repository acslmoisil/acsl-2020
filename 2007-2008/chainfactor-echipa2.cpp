#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <set>

using namespace std;

ifstream fin("chainfactor.in");

struct vec2{
	int x, y;
};

int CURRENT_ID = 0;

int chartodigit(char c){
	return (c-'0');
}

struct Disk{
	int val, id;
	Disk(){}
	Disk(int val) : val(val), id(CURRENT_ID++){}
	Disk(char c) : Disk(chartodigit(c)){}
};

vector<Disk> grawity[7];
void preread(){
	string uwu;
	for(int i = 0; i < 7; ++i){
		fin >> uwu;
		if(uwu.back() == ','){
			uwu.pop_back();
		}
		
		for(int j = 0; j < uwu.size(); ++j){
			if(uwu[j] != '0'){
				grawity[i].push_back(Disk(uwu[j]));
			}
		}
	}
}

vec2 finddisk(int id){
	for(int x = 0; x < 7; ++x){
		for(int y = 0; y < grawity[x].size(); ++y){
			if(grawity[x][y].id == id){
				return {x, y};
			}
		}
	}
	return {-1, -1};
}

set<int> qu;
void nuke(){
	qu.clear();
	for(int i = 0; i < 7; ++i){
		grawity[i].clear();
	}
}

void read(){
	string mv;
	fin >> mv;
	if(mv.back() == ','){
		mv.pop_back();
	}
	
	int pos = mv[0]-'A';
	Disk hard(mv[1]);
	
	qu.insert(hard.id);
	grawity[pos].push_back(hard);
}

bool inb(vec2 p){
	return p.x >= 0 && p.x < 7 && p.y >= 0 && p.y < grawity[p.x].size();
}

int zeppelin(vec2 p, vec2 inc){
	p = {p.x+inc.x, p.y+inc.y};
	int r = 0;
	while(inb(p)){
		r++;
		p = {p.x+inc.x, p.y+inc.y};
	}
	return r;
}

void addme(vec2 p){
	for(int y = p.y; y < grawity[p.x].size(); ++y){
		qu.insert(grawity[p.x][y].id);
	}
}

int razer(vec2 p, bool hori){
	int num = grawity[p.x][p.y].val;
	
	int r = 0;
	if(hori){
		vec2 np = {p.x+1, p.y};
		while(inb(np)){
			if(grawity[np.x][np.y].val == num){
				addme({np.x, np.y+1});
				grawity[np.x].erase(grawity[np.x].begin()+np.y);
				r++;
			}
			np = {np.x+1, np.y};
		}
		
		np = {p.x-1, p.y};
		while(inb(np)){
			if(grawity[np.x][np.y].val == num){
				addme({np.x, np.y+1});
				grawity[np.x].erase(grawity[np.x].begin()+np.y);
				r++;
			}
			np = {np.x-1, np.y};
		}
	}else{
		for(int i = grawity[p.x].size()-2; i >= 0; --i){
			if(grawity[p.x][i].val == num){
				grawity[p.x].erase(grawity[p.x].begin()+i);
				r++;
			}
		}
		addme({p.x, 0});
	}
	return r;
}

int ans;
void speedwagon(vec2 p){
	int num = grawity[p.x][p.y].val;
	
	int hori = zeppelin(p, {1, 0}) + 1 + zeppelin(p, {-1, 0});
	int vert = zeppelin(p, {0, -1}) + 1;
	if(hori == num || vert == num){
		if(hori == num){
			ans += razer(p, true);
		}
		if(vert == num){
			ans += razer(p, false);
		}
		ans++;
		grawity[p.x].pop_back();
	}
}

void solve(){
	ans = 0;
	while(!qu.empty()){
		vec2 p = finddisk(*qu.begin());
		qu.erase(qu.begin());
		if(p.x != -1){
			speedwagon(p);
		}
	}
}

void deb(){
	for(auto v : grawity){
		for(auto a : v){
			cout << a.val << " ";
		}
		cout << "\n";
	}
	cout << "***\n";
}

int main(){
	// ios_base::sync_with_stdio(false);
	nuke();
	preread();
	for(int acsl = 0; acsl < 10; ++acsl){
		read();
		solve();
		cout << ans << "\n";
	}
	return 0;
}