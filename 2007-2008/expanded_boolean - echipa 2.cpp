#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <stack>

using namespace std;

ifstream f("date.in");

map<char, bool(*)(vector<bool>)> funcs;
map<char, int> param;
map<char, bool> variables;

int trues;

bool opExclamation(vector<bool> inputs){
    if(inputs.size() != 2) return false;
    return inputs[0] == inputs[1];
}

bool opHash(vector<bool> inputs){
    if(inputs.size() != 3) return false;
    int number = 0;
    for (int i = 0; i < 3; ++i)
        if(inputs[i]) number++;
    return number > 1;
}

bool opDollar(vector<bool> inputs){
    if(inputs.size() != 3) return false;
    int number = 0;
    for (int i = 0; i < 3; ++i)
        if(!inputs[i]) number++;
    return number > 1;
}

bool opAnd(vector<bool> inputs){
    if(inputs.size() != 3) return false;
    int number = 0;
    for (int i = 0; i < 3; ++i)
        if(inputs[i]) number++;
    return number % 2;
}

bool opPercent(vector<bool> inputs){
    if(inputs.size() != 4) return false;
    int number = 0;
    for (int i = 0; i < 4; ++i)
        if(!inputs[i]) number++;
    return number % 2;
}

void assignFunctionReferencesToMap(){
    funcs['!'] = &opExclamation;
    param['!'] = 2;
    funcs['#'] = &opHash;
    param['#'] = 3;
    funcs['$'] = &opDollar;
    param['$'] = 3;
    funcs['&'] = &opAnd;
    param['&'] = 3;
    funcs['%'] = &opPercent;
    param['%'] = 4;
}

bool evalExp(string exp){
    stack<bool>op;
    for (int i = 0; i < exp.length(); ++i) {
        if(param.find(exp[i]) == param.end()) op.push(variables[exp[i]]);
        else{
            vector<bool> inp;
            for (int j = 0; j < param[exp[i]]; ++j) {
                inp.push_back(op.top());
                op.pop();
            }
            op.push(funcs[exp[i]](inp));
        }
    }
    return op.top();
}

void determineVariables(string &expression, vector<bool> &vals, int howManyVariables, int depth = 0){
    if(depth == howManyVariables){
        int i = 0;
        for (auto &it : variables)
            it.second = vals[i++];
        if(evalExp(expression)) trues++;
        return;
    }
    for (int i = 0; i <= 1; ++i) {
        vals[depth] = i;
        determineVariables(expression, vals, howManyVariables, depth + 1);
    }
}

int main() {
    assignFunctionReferencesToMap();
    for (int acsl = 0; acsl < 10; ++acsl) {
        string expression;
        trues = 0;
        getline(f, expression);
        variables.clear();
        for (int i = 0; i < expression.length(); ++i){
            if(isupper(expression[i]) && variables.find(expression[i]) == variables.end()) variables[expression[i]] = false;
        }
        vector<bool> vals;
        for (int i = 0; i < variables.size(); ++i) vals.push_back(false);
        determineVariables(expression, vals, (int)vals.size());
        cout<<trues<<"\n";
    }
    return 0;
}
