#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

ifstream fin("tali.in");

vector<int> bigpp = {6, 4, 3, 1};

struct Playa{
	char c;
	
	vector<int> v;
	int sum = 0;
	bool op;
	Playa(){}
	Playa(char c, string s) : c(c){
		for(auto c : s){
			v.push_back(c-'0');
			sum += v.back();
		}
		
		sort(v.begin(), v.end(), std::greater<int>());
		
		vector<int> sorted = v;
		sort(sorted.begin(), sorted.end());
		op = (v == bigpp);
	}
	bool operator<(const Playa &rhs){
		if(op){
			return true;
		}else if(rhs.op){
			return false;
		}else if(sum != rhs.sum){
			return sum > rhs.sum;
		}else{
			return v > rhs.v;
		}
	}
};

vector<Playa> playas;
void read(){
	playas.clear();
	
	string s;
	for(int i = 0; i < 4; ++i){
		fin >> s;
		if(s.back() == ','){
			s.pop_back();
		}
		playas.push_back(Playa('A'+i, s));
	}
}

void solve(){
	sort(playas.begin(), playas.end());
	for(auto &playa : playas){
		cout << playa.c;
	}
	cout << "\n";
}

int main(){
	for(int acsl = 0; acsl < 10; ++acsl){
		read();
		solve();
	}
	return 0;
}