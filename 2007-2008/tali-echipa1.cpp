#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
struct numar{
    int val;
    char c;
}a[5];
int scif(int x)
{
    int s=0;
    while(x)
    {
        s+=x%10;
        x/=10;
    }
    return s;
}
void sortare_cifre()
{
    for(int i=0; i<4; i++)
    {
        int cif[5]{0};
        int m=0;
        while(a[i].val)
        {
            cif[m++]=a[i].val%10;
            a[i].val/=10;
        }
        for(int j=0; j<m-1; j++)
            for(int k=j+1; k<m; k++)
                if(cif[j]<cif[k])
                {
                    int aux=cif[j];
                    cif[j]=cif[k];
                    cif[k]=aux;
                }
        for(int j=0; j<m; j++)
            a[i].val=a[i].val*10+cif[j];
    }
}
void sortare()
{
    for(int i=0; i<3; i++)
        for(int j=i+1; j<4; j++)
            if(scif(a[i].val)<scif(a[j].val))
            {
                 numar aux=a[i];
                 a[i]=a[j];
                 a[j]=aux;
            }
            else if(scif(a[i].val)==scif(a[j].val))
            {
                if(a[i].val<a[j].val)
                {
                    numar aux=a[i];
                    a[i]=a[j];
                    a[j]=aux;
                }
                else if(a[i].val==a[j].val)
                    if(a[i].c>a[j].c)
                    {
                        numar aux=a[i];
                        a[i]=a[j];
                        a[j]=aux;
                    }
            }
    for(int i=0; i<4; i++)
        if(a[i].val==6431)
        {
            numar aux=a[i];
            for(int j=i-1; j>=0; j--)
                a[j+1]=a[j];
            a[0]=aux;
        }
}
void citire()
{
    scanf("%d, %d, %d, %d\n", &a[0].val, &a[1].val, &a[2].val, &a[3].val);
    sortare_cifre();
    a[0].c='A';
    a[1].c='B';
    a[2].c='C';
    a[3].c='D';
}
void afisare()
{
    for(int i=0; i<4; i++)
        cout<<a[i].c;
    cout<<'\n';
}
int main()
{
    freopen("tali.in", "r", stdin);
    for(int acsl=0; acsl<10; acsl++)
    {
        citire();
        sortare();
        afisare();
    }
    return 0;
}
