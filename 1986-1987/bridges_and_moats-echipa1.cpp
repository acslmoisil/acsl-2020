#include <iostream>
#include <fstream>

using namespace std;

struct card
{
    char nr, suit;
};

ifstream f("bridges_and_moats.in");

card cards[15];

void read()
{
    for (int i = 0; i < 10; ++i)
        f>>cards[i].nr>>cards[i].suit;
}

void moat_value()
{
    int s=0;
    for (int i = 0; i < 10; ++i) {
        if(cards[i].nr=='A')
            s=s+15;
        else if (cards[i].nr=='J'||cards[i].nr=='Q'||cards[i].nr=='K')
            s=s+12;
        else if (cards[i].nr=='0')
            s=s+10;
        else
            s=s+cards[i].nr-'0';
    }
    cout<<"MOAT="<<s<<'\n';
}

void bridge_value()
{
    int s=0,nraces=0,nr[5]={0};
    for (int i = 0; i < 10; ++i) {
        if(cards[i].nr=='A')
        {
            s=s+4;
            nraces++;
        } else if (cards[i].nr=='K')
            s=s+3;
        else if (cards[i].nr=='Q')
            s=s+2;
        else if (cards[i].nr=='J')
            s++;
        if(cards[i].suit=='S')
            nr[0]++;
        else if (cards[i].suit=='D')
            nr[1]++;
        else if (cards[i].suit=='C')
            nr[2]++;
        else if (cards[i].suit=='H')
            nr[3]++;
    }
    if(nraces==4)
        s++;
    for (int i = 0; i < 4; ++i) {
        if(nr[i]==0)
            s=s+3;
        else if (nr[i]==1)
            s=s+2;
        else if (nr[i]==2)
            s++;
    }
    cout<<"BRIDGE="<<s<<'\n';
}

int main() {
    for (int acsl = 0; acsl < 5; ++acsl) {
        read();
        moat_value();
        bridge_value();
    }
    return 0;
}
