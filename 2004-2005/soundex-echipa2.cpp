#include <iostream>
#include <fstream>
#include <cstring>

using namespace std;

ifstream f("soundex.in");

char lit[50], output[10];
int i, pre, ind;

bool voc(char l) {
    return l == 'A' || l == 'E' || l == 'I' || l == 'O' || l == 'U';
}

void reset() {
    for (char &i : output)
        i = '0';
    pre = -3;
    ind = 0;
}

void put_in_output(int nr, int jump) {
    pre = nr;
    i += jump;
    if (nr > 9) {
        output[ind++] = nr / 10 + '0';
        output[ind++] = nr % 10 + '0';
    } else
        output[ind++] = nr + '0';
}

int grup_of_four(int poz) {
    if (lit[poz] == 'S' && lit[poz + 1] == 'C' && lit[poz + 2] == 'H' && (lit[poz + 3] == 'T' || lit[poz + 3] == 'D')) {
        if (poz == 0)
            return 2;
        else
            return 43;
    }
    return -2;
}

int grup_of_three(int poz) {
    if ((lit[poz] == 'T' && lit[poz + 1] == 'C' && lit[poz + 2] == 'H') ||
        (lit[poz] == 'C' && lit[poz + 1] == 'S' && lit[poz + 2] == 'Z') ||
        (lit[poz] == 'C' && lit[poz + 1] == 'Z' && lit[poz + 2] == 'S') ||
        (lit[poz] == 'S' && lit[poz + 1] == 'C' && lit[poz + 2] == 'H'))
        return 4;
    if (lit[poz] == 'C' && lit[poz + 1] == 'H' && lit[poz + 2] == 'S') {
        if (poz == 0)
            return 5;
        else
            return 54;
    }
    if ((lit[poz] == 'S' && lit[poz + 1] == 'H' && lit[poz + 2] == 'D') ||
        (lit[poz] == 'S' && lit[poz + 1] == 'Z' && lit[poz + 2] == 'D') ||
        (lit[poz] == 'S' && lit[poz + 1] == 'H' && lit[poz + 2] == 'T')) {
        if (poz == 0)
            return 2;
        else
            return 43;
    }
    return -2;
}

int grup_of_two(int poz) {
    if ((lit[poz] == 'A' && lit[poz + 1] == 'I') || (lit[poz] == 'A' && lit[poz + 1] == 'J') ||
        (lit[poz] == 'A' && lit[poz + 1] == 'Y') || (lit[poz] == 'E' && lit[poz + 1] == 'I') ||
        (lit[poz] == 'E' && lit[poz + 1] == 'U') || (lit[poz] == 'E' && lit[poz + 1] == 'Y') ||
        (lit[poz] == 'I' && lit[poz + 1] == 'E') || (lit[poz] == 'I' && lit[poz + 1] == 'O') ||
        (lit[poz] == 'I' && lit[poz + 1] == 'U') || (lit[poz] == 'O' && lit[poz + 1] == 'I') ||
        (lit[poz] == 'O' && lit[poz + 1] == 'J') || (lit[poz] == 'O' && lit[poz + 1] == 'Y')) {
        if (poz == 0)
            return 0;
        else if (voc(lit[poz + 2]))
            return 1;
        else
            return -1;
    }
    if (lit[poz] == 'A' && lit[poz + 1] == 'U') {
        if (poz == 0)
            return 0;
        else if (voc(lit[poz + 2]))
            return 7;
        else
            return -1;
    }
    if ((lit[poz] == 'F' && lit[poz + 1] == 'B') || (lit[poz] == 'P' && lit[poz + 1] == 'F') ||
        (lit[poz] == 'P' && lit[poz + 1] == 'H'))
        return 7;
    if ((lit[poz] == 'D' && lit[poz + 1] == 'T') || (lit[poz] == 'T' && lit[poz + 1] == 'H'))
        return 3;
    if ((lit[poz] == 'K' && lit[poz + 1] == 'H') || (lit[poz] == 'C' && lit[poz + 1] == 'K'))
        return 5;
    if ((lit[poz] == 'R' && lit[poz + 1] == 'Z') || (lit[poz] == 'R' && lit[poz + 1] == 'S'))
        return 9;
    if ((lit[poz] == 'C' && lit[poz + 1] == 'S') || (lit[poz] == 'D' && lit[poz + 1] == 'S') ||
        (lit[poz] == 'D' && lit[poz + 1] == 'Z') || (lit[poz] == 'C' && lit[poz + 1] == 'Z') ||
        (lit[poz] == 'T' && lit[poz + 1] == 'S') || (lit[poz] == 'S' && lit[poz + 1] == 'H') ||
        (lit[poz] == 'S' && lit[poz + 1] == 'Z') || (lit[poz] == 'T' && lit[poz + 1] == 'Z') ||
        (lit[poz] == 'C' && lit[poz + 1] == 'H'))
        return 4;
    if (lit[poz] == 'K' && lit[poz + 1] == 'S') {
        if (poz == 0)
            return 5;
        else
            return 54;
    }
    if (lit[poz] == 'E' && lit[poz + 1] == 'U') {
        if (poz == 0 || voc(lit[poz + 2]))
            return 1;
        else
            return -1;
    }
    if ((lit[poz] == 'M' && lit[poz + 1] == 'N') || (lit[poz] == 'N' && lit[poz + 1] == 'M'))
        return 66;
    if ((lit[poz] == 'S' && lit[poz + 1] == 'D') || (lit[poz] == 'S' && lit[poz + 1] == 'T')) {
        if (poz == 0)
            return 2;
        else
            return 43;
    }
    return -2;
}

int grup_of_one(int poz) {
    if ((lit[poz] == 'A') || (lit[poz] == 'E') || (lit[poz] == 'I') || (lit[poz] == 'O') || (lit[poz] == 'U')) {
        if (poz == 0)
            return 0;
        return -1;
    }
    if ((lit[poz] == 'B') || (lit[poz] == 'F') || (lit[poz] == 'P') || (lit[poz] == 'V') || (lit[poz] == 'W'))
        return 7;
    if ((lit[poz] == 'D') || (lit[poz] == 'T'))
        return 3;
    if ((lit[poz] == 'G') || (lit[poz] == 'K') || (lit[poz] == 'Q'))
        return 5;
    if (lit[poz] == 'H') {
        if (poz == 0 || voc(lit[poz + 1]))
            return 5;
        return -1;
    }
    if (lit[poz] == 'L')
        return 8;
    if ((lit[poz] == 'M') || (lit[poz] == 'N'))
        return 6;
    if (lit[poz] == 'R')
        return 9;
    if ((lit[poz] == 'C') || (lit[poz] == 'S') || (lit[poz] == 'Z'))
        return 4;
    if (lit[poz] == 'X') {
        if (poz == 0)
            return 5;
        return 54;
    }
    if (lit[poz] == 'Y') {
        if (poz == 0)
            return 1;
        return -1;
    }
    if (lit[poz] == 'J')
        return 2;
    return -2;
}

void afis() {
    for (int j = 0; j < 6; ++j)
        cout << output[j];
    cout << '\n';
}

int main() {
    for (int acsl = 0; acsl < 10; ++acsl) {
        reset();
        f.getline(lit, 50);
        int nrl = strlen(lit);
        for (i = 0; i < nrl; ++i) {
            if (lit[i] == ' ')
                continue;
            int aux = grup_of_four(i);
            if (aux != -2 && aux != pre) {
                put_in_output(aux, 3);
                continue;
            }
            aux = grup_of_three(i);
            if (aux != -2 && aux != pre) {
                put_in_output(aux, 2);
                continue;
            }
            aux = grup_of_two(i);
            if (aux == -1) {
                i++;
                continue;
            } else if (aux != -2 && aux != pre) {
                put_in_output(aux, 1);
                continue;
            }
            aux = grup_of_one(i);
            if (aux == -1) {
                continue;
            } else if (aux != -2 && aux != pre) {
                put_in_output(aux, 0);
                continue;
            }
        }
        afis();
    }
    return 0;
}
