#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cctype>

//God help me...

using namespace std;

ifstream fin("soundex.in");

string susta(string s){
	if(s == "NC"){
		return "";
	}else{
		return s;
	}
}

struct Row{
	vector<string> combos;
	string name, vowel, other;
	Row(string s){
		int col = 0;
		string cur = "";
		s.push_back(' ');
		for(int i = 0; i < s.size(); ++i){
			char c = s[i];
			if(c == ' '){
				if(!cur.empty()){
					if(col <= 1){
						combos.push_back(cur);
					}else if(col == 2){
						name = susta(cur);
					}else if(col == 3){
						vowel = susta(cur);
					}else if(col == 4){
						other = susta(cur);
					}
				}
				cur = "";
			}else if(c == '|'){
				col++;
			}else{
				cur += c;
			}
		}
	}
	string gime(bool name, bool vowel){
		if(name){
			return this->name;
		}else if(vowel){
			return this->vowel;
		}else{
			return other;
		}
	}
};

struct Table{
	vector<Row> rows;
	Table(){
		ifstream fml("fml.txt");
		for(int i = 0; i < 17; ++i){
			string s;getline(fml, s);
			rows.push_back(Row(s));
		}
	}
};

bool isvowel(char c){
	return string("AEIOU").find(c) != string::npos;
}

Table tab;

string s;
void read(){
	getline(fin, s);
}

pair<string, int> encode(int len, string s, bool name, bool vowel){
	for(auto row : tab.rows){
		for(auto combo : row.combos){
			if(combo.size() >= len && combo == s.substr(0, min(combo.size(), s.size()))){
				string s = row.gime(name, vowel);
				if(s != ""){
					return {s, combo.size()};
				}
			}
		}
	}
	return {"", 0};
}

pair<string, int> bigcode(string s, bool name, bool vowel){
	pair<string, int> r = encode(2, s, name, vowel);
	if(r.second == 0){
		r = encode(1, s, name, vowel);
	}
	return r;
}

void solve(){
	string ans = "";
	
	bool name = true, vowel = false;
	for(int i = 0; i < s.size(); ++i){
		pair<string, int> r = bigcode(s.substr(i), name, vowel);
		if(r.second <= 1){
			if(ans.empty() || r.first.empty() || ans.back() != r.first.back()){
				ans += r.first;
			}
		}else{
			ans += r.first;
		}
		i += max(r.second-1, 0);
		
		name = false;
		vowel = isvowel(s[i]);
	}
	
	while(ans.size() < 6){
		ans.push_back('0');
	}
	cout << ans << "\n";
}

int main(){
	// ios_base::sync_with_stdio(false);
	for(int acsl = 0; acsl < 10; ++acsl){
		read();
		solve();
	}
	return 0;
}