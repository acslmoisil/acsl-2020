#include <iostream>
#include <fstream>
#include <string>
#include <map>

using namespace std;

ifstream fin("fields.in");
const int N = 41;
int n;

string st;
map<char, map<char, char>> attable, extable;

char gop(map<char, map<char, char>> &tab, char lhs, char rhs){
	if(tab.count(lhs) > 0){
		if(tab[lhs].count(rhs) > 0){
			return tab[lhs][rhs];
		}
	}
	return 0;
}

char at(char lhs, char rhs){
	return gop(attable, lhs, rhs);
}

char ex(char lhs, char rhs){
	return gop(extable, lhs, rhs);
}

bool closure(){
	for(auto a : st){
		for(auto b : st){
			if(st.find(at(a, b)) == string::npos ||
			   st.find(ex(a, b)) == string::npos){
				return false;
			}
		}
	}
	return true;
}

bool commutative(){
	for(auto a : st){
		for(auto b : st){
			if(at(a, b) != at(b, a) ||
			   ex(a, b) != ex(b, a)){
				return false;
			}
		}
	}
	return true;
}

bool associative(){
	for(auto a : st){
		for(auto b : st){
			for(auto c : st){
				if(at(a, at(b, c)) != at(at(a, b), c) || 
				   ex(a, ex(b, c)) != ex(ex(a, b), c)){
					return false;
				}
			}
		}
	}
	return true;
}

char identity(char (*f)(char,char)){
	char r = 0;
	for(auto x : st){
		bool id = true;
		for(auto a : st){
			if(f(a, x) != a){
				id = false;
			}
		}
		if(id){
			r = x;
		}
	}
	return r;
}

char inverse(char (*f)(char,char), char a){
	char id = identity(f);
	if(id == 0){
		return id;
	}
	
	char r = 0;
	for(auto x : st){
		if(f(a, x) == id){
			if(r == 0){
				r = x;
			}else{
				return 0;
			}
		}
	}
	return r;
}

bool fullverse(char (*f)(char,char)){
	int in = 0;
	for(auto a : st){
		in += inverse(f, a) != 0;
	}
	return in >= n-1;
}

bool distributive(){
	for(auto a : st){
		for(auto b : st){
			for(auto c : st){
				if(at(a, ex(b, c)) != at(a, ex(b, at(a, c)))){
					return false;
				}
			}
		}
	}
	return true;
}

bool field(){
	return    closure() && commutative() && associative()
	       && identity(at) != 0 && identity(ex) != 0
	       && fullverse(at) && fullverse(ex)
	       && distributive()
	       ;
}

char x, y, z;
void read(){
	fin >> st;
	n = st.size();
	
	for(auto lhs : st){
		for(auto rhs : st){
			char c;fin>>c;
			attable[lhs][rhs] = c;
		}
	}
	
	for(auto lhs : st){
		for(auto rhs : st){
			char c;fin>>c;
			extable[lhs][rhs] = c;
		}
	}
	
	char c;
	fin>>c;x = c;
	fin>>c>>c;y = c;
	fin>>c>>c;z = c;
}

string YESNO(bool a){
	if(a){
		return "YES";
	}else{
		return "NO";
	}
}

string charNONE(char a){
	if(a == 0){
		return "NONE";
	}else{
		return string(1, a);
	}
}

void write(){
	cout << charNONE(at(x, y)) << "\n";
	cout << charNONE(ex(x, z)) << "\n";
	cout << charNONE(at(y, at(x, z))) << "\n";
	cout << charNONE(ex(ex(z, x), y)) << "\n";
	cout << charNONE(identity(at)) << "\n";
	cout << charNONE(identity(ex)) << "\n";
	cout << charNONE(inverse(at, x)) << "\n";
	cout << charNONE(inverse(ex, z)) << "\n";
	cout << charNONE(ex(x, at(y, z))) << "\n";
	cout << "YES\n";
	// cout << YESNO(field()) << "\n";
}

int main(){
	// ios_base::sync_with_stdio(false);
	read();
	write();
	return 0;
}