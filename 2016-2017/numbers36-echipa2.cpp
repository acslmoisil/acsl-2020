#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <vector>
#include <cmath>

using namespace std;

ifstream fin("thirtysix.in");

typedef long long lint;

string zerosbegone(string s){
	while(s.front() == '0'){
		s.erase(s.begin());
	}
	return s;
}

string next_permutize(string s, int k){
	for(int i = 0; i < k; ++i){
		next_permutation(s.begin(), s.end());
	}
	return s;
}

string prev_permutize(string s, int k){
	for(int i = 1; i < k; ++i){
		prev_permutation(s.begin(), s.end());
	}
	return s;
}

void decomma(string &s){
	if(s.back() == ','){
		s.pop_back();
	}
}

void safeprint(const string &s){
	cout << zerosbegone(s) << "\n";
}

lint digitize(char c){
	if(c >= '0' && c <= '9'){
		return c-'0';
	}else{
		return c-'A'+10;
	}
}

char charize(lint a){
	if(a >= 0 && a <= 9){
		return a+'0';
	}else{
		return a-10+'A';
	}
}

lint base = 36;
lint conv(string a){
	lint r = 0;
	for(auto c : a){
		r *= base;
		r += digitize(c);
	}
	return r;
}

string dontbemean(string a, string b){
	string s = a;
	sort(s.begin(), s.end(), std::greater<char>());
	lint lasty = conv(s);
	
	sort(s.begin(), s.end(), std::less<char>());
	lint mean = (conv(a)+conv(b))/2LL;
	
	lint code;
	lint besty;
	vector<string> stringy;
	do{
		code = conv(s);
		lint curry = abs(code-mean);
		if(stringy.empty() || curry < besty){
			besty = curry;
			stringy.clear();
		}
		if(curry == besty){
			stringy.push_back(s);
		}
		next_permutation(s.begin(), s.end());
	}while(code != lasty);
	
	if(stringy.size() == 1){
		return stringy.front();
	}else{
		return stringy.front() + " " + stringy.back();
	}
}

string s;
int k;

void read(){
	fin >> s >> k;
	decomma(s);
}

void solve(){
	sort(s.begin(), s.end(), std::less<char>());
	string juan = s;
	safeprint(s);
	
	sort(s.begin(), s.end(), std::greater<char>());
	string dos = s;
	safeprint(s);
	
	safeprint(prev_permutize(s, 50));
	safeprint(next_permutize(s, k));
	
	cout << dontbemean(juan, dos) << "\n";
}

int main(){
	// ios_base::sync_with_stdio(false);
	for(int acsl = 0; acsl < 2; ++acsl){
		read();
		solve();
	}
	return 0;
}