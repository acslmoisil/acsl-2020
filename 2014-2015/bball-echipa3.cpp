#include <iostream>
#include <fstream>
#include <cstring>
#include <iomanip>
#include <algorithm>

using namespace std;

ifstream f("ball.in");

struct team
{
    char name[20];
    int p1,p2,p3,p4,at,total;
};

team x[5],y[5];
char nr;

void read()
{
    for (auto & i : x) {
        f>>i.name;
        i.name[strlen(i.name)-1]='\000';
        f>>nr;
        i.p1=nr-'0';
        i.total+=i.p1;
        f>>nr;
        i.p2=nr-'0';
        i.total+=i.p2*2;
        f>>nr;
        i.p3=nr-'0';
        i.total+=i.p3*3;
        f>>nr;
        i.p4=nr-'0';
        i.total+=i.p4*4;
        f>>nr;
        if(nr<='9')
            i.at=nr-'0';
        else
            i.at=nr-'A'+10;
    }
    for (auto & i : y) {
        f>>i.name;
        i.name[strlen(i.name)-1]='\000';
        f>>nr;
        i.p1=nr-'0';
        i.total+=i.p1;
        f>>nr;
        i.p2=nr-'0';
        i.total+=i.p2*2;
        f>>nr;
        i.p3=nr-'0';
        i.total+=i.p3*3;
        f>>nr;
        i.p4=nr-'0';
        i.total+=i.p4*4;
        f>>nr;
        if(nr<='9')
            i.at=nr-'0';
        else
            i.at=nr-'A'+10;
    }
}

void line1()
{
    int sum=0;
    for (int i = 0; i < 5; ++i) {
        sum+=x[i].total;
        sum+=y[i].total;
    }
    cout<<fixed<<setprecision(1)<<(double)sum/10<<'\n';
}

void line2()
{
    int vec[11];
    for (int i = 0; i < 5; ++i) {
        vec[i]=x[i].total;
        vec[i+5]=y[i].total;
    }
    sort(vec,vec+10);
    cout<<fixed<<setprecision(1)<<((double)vec[4]+(double)vec[5])/2<<'\n';
}

void line3()
{
    int indx,indy,highx=0,highy=0;
    for (int i = 0; i < 5; ++i) {
        if(x[i].total>highx)
        {
            highx=x[i].total;
            indx=i;
        }
        if(y[i].total>highy)
        {
            highy=y[i].total;
            indy=i;
        }
    }
    cout<<x[indx].name<<", "<<y[indy].name<<'\n';
}

void line4()
{
    int ind1,ind2,max1=0,max2=0;
    for (int i = 0; i < 5; ++i) {
        if(x[i].total>max1)
        {
            max2=max1;
            ind2=ind1;
            max1=x[i].total;
            ind1=i;
        }
        else if(x[i].total>max2)
        {
            ind2=i;
            max2=x[i].total;
        }
        if(y[i].total>max1)
        {
            max2=max1;
            ind2=ind1;
            max1=y[i].total;
            ind1=i+5;
        }
        else if(y[i].total>max2)
        {
            ind2=i+5;
            max2=y[i].total;
        }
    }
    if(ind1>=5)
        cout<<y[ind1-5].name;
    else
        cout<<x[ind1].name;
    cout<<", ";
    if(ind2>=5)
        cout<<y[ind2-5].name;
    else
        cout<<x[ind2].name;
    cout<<'\n';
}

void line5()
{
    int totalx=0,totaly=0;
    for (int i = 0; i < 5; ++i) {
        totalx+=x[i].total;
        totaly+=y[i].total;
    }
    if(totalx>totaly)
        cout<<totalx<<", "<<totaly<<'\n';
    else
        cout<<totaly<<", "<<totalx<<'\n';
}

void line6()
{
    int mini=1000000;
    bool ok=false;
    for (int i = 0; i < 5; ++i) {
        if(x[i].total<mini)
            mini=x[i].total;
        if(y[i].total<mini)
            mini=y[i].total;
    }
    for (int i = 0; i < 5; ++i) {
        if(x[i].total==mini)
        {
            if(ok)
                cout<<", ";
            cout<<x[i].name;
            ok=true;
        }
        if(y[i].total==mini)
        {
            if(ok)
                cout<<", ";
            cout<<y[i].name;
            ok=true;
        }
    }
    cout<<'\n';
}

void line7()
{
    double high=0,cur;
    bool ok=false;
    for (auto & i : x) {
        cur=(double)(i.p1+i.p2+i.p3+i.p4)/i.at;
        if(cur>high)
            high=cur;
    }
    for (auto & i : x) {
        cur=(double)(i.p1+i.p2+i.p3+i.p4)/i.at;
        if(cur==high)
        {
            if(ok)
                cout<<", ";
            cout<<i.name;
            ok=true;
        }
    }
    cout<<'\n';
}

void line8()
{
    double low=1000,cur;
    bool ok=false;
    for (auto & i : y) {
        cur=(double)(i.p1+i.p2+i.p3+i.p4)/i.at;
        if(cur<low)
            low=cur;
    }
    for (auto & i : y) {
        cur=(double)(i.p1+i.p2+i.p3+i.p4)/i.at;
        if(cur==low)
        {
            if(ok)
                cout<<'\n';
            cout<<i.name;
            ok=true;
        }
    }
    cout<<'\n';
}

void line9()
{
    int high=0;
    bool ok=false;
    for (int i = 0; i < 5; ++i) {
        if(x[i].p2>high)
            high=x[i].p2;
        if(y[i].p2>high)
            high=y[i].p2;
    }
    for (int i = 0; i < 5; ++i) {
        if(x[i].p2==high)
        {
            if(ok)
                cout<<", ";
            cout<<x[i].name;
            ok=true;
        }
        if(y[i].p2==high)
        {
            if(ok)
                cout<<", ";
            cout<<y[i].name;
            ok=true;
        }
    }
    cout<<'\n';
}

void line10()
{
    int high=0;
    bool ok=false;
    for (int i = 0; i < 5; ++i) {
        if(x[i].p1>high)
            high=x[i].p1;
        if(y[i].p1>high)
            high=y[i].p1;
    }
    for (int i = 0; i < 5; ++i) {
        if(x[i].p1==high)
        {
            if(ok)
                cout<<", ";
            cout<<x[i].name;
            ok=true;
        }
        if(y[i].p1==high)
        {
            if(ok)
                cout<<", ";
            cout<<y[i].name;
            ok=true;
        }
    }
}

int main() {
    read();
    line1();
    line2();
    line3();
    line4();
    line5();
    line6();
    line7();
    line8();
    line9();
    line10();
    return 0;
}