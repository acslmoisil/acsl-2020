#include <iostream>
#include <fstream>
#include <cstring>
#define separatori " ,.!?();:'-=_`"

using namespace std;
int fr[26], ap[26];
ifstream f("date.in");

void reset(){
    for (int i = 0; i < 26; ++i) {
        fr[i] = 0;
    }
}

void reset2(){
    for (int i = 0; i < 26; ++i) {
        ap[i] = 0;
    }
}

int main() {
    for (int acsl = 0; acsl < 2; ++acsl) {
        char sentence[200];
        char cuv[50][100];
        reset();
        f.getline(sentence, 200);
        int l=strlen(sentence);
        for(int i=0;i<l;i++)
        {
            if(sentence[i]>='A' && sentence[i]<='Z')
                fr[sentence[i]-'A']++;
            if(sentence[i]>='a' && sentence[i]<='z')
                fr[sentence[i]-'a']++;
        }
        int maxi=0, pozmax=0;
        for(int i=0;i<26;i++)
            if(fr[i]>maxi)
            {
                maxi=fr[i];
                pozmax=i;
            }
        cout<<(char)(pozmax + 'A')<<' '<<maxi<<'\n';
        char *p = strtok(sentence, separatori);
        int cuvinte = 0;
        while(p){
            strcpy(cuv[cuvinte++], p);
            p = strtok(NULL, separatori);
        }
        reset();
        for(int i=0;i<cuvinte;i++)
        {
            for(int j=0;j<26;j++)
                if(strchr(cuv[i],(char)('a'+j))!=NULL || strchr(cuv[i],(char)('A'+j))!=NULL )
                   fr[j]++;

        }
        maxi=0, pozmax=0;
        for(int i=0;i<26;i++)
            if(fr[i]>maxi)
            {
                maxi=fr[i];
                pozmax=i;
            }
        cout<<(char)(pozmax + 'A')<<' '<<maxi<<'\n';
        for(int i=25;i>=0;i--)
            if(fr[i])
                cout<<(char)(i+'A');
        cout<<'\n';
        reset();
        for(int i=0;i<cuvinte;i++)
        {
            reset2();
            for(int j=0;j<strlen(cuv[i]);j++)
            {
                if(cuv[i][j]>='A' && cuv[i][j]<='Z')
                    ap[cuv[i][j]-'A']++;
                if(cuv[i][j]>='a' && cuv[i][j]<='z')
                    ap[cuv[i][j]-'a']++;
            }
            for(int j=0;j<26;j++)
                if(ap[j] >= 2)
                    fr[j]++;
        }
        for(int i=0;i<26;i++)
            if(fr[i])
                cout<<(char)(i+'A');
        cout<<'\n';
        int a=200;
        reset();
        for(int i=0;i<l;i++)
        {
            if(sentence[i]>='A' && sentence[i]<='Z')
                fr[sentence[i]-'A']++;
            if(sentence[i]>='a' && sentence[i]<='z')
                fr[sentence[i]-'a']++;
        }
        for(int i=200;i>=2;i--)
            for(int j=0;j<=26;j++)
                if(fr[j]==i)
                    cout<<(char)(j+'A');
        cout<<'\n';
    }
    return 0;
}
