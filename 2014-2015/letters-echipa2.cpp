#include <iostream>
#include <cstdio>
#include <cstring>
using namespace std;
char s[300];
int fr[30];
struct litera{
    char l;
    int ap;
}a[300];
int k=0;
void resetare()
{
    for(int i=0; i<k; i++)
    {
        a[i].ap=0;
        a[i].l=' ';
    }
    k=0;
    for(int i=0; i<26; i++)
        fr[i]=0;
}
void frecventa_lit()
{
    int n=strlen(s);
    for(int i=0; i<n; i++)
        if(s[i]>='A' && s[i]<='Z')
            fr[s[i]-'A']++;
}
void cerinta_1()
{
    char l;
    int maxim=-1;
    frecventa_lit();
    for(int i=0; i<26; i++)
        if(maxim<fr[i])
        {
            maxim=fr[i];
            l=i+'A';
        }
    printf("%c %d\n", l, maxim);
}
void cerinta_2()
{
    char aux[300];
    strcpy(aux, s);
    int frecv[30]{0};
    char *p=strtok(aux, " .,/?!-");
    while(p)
    {
        int ok[30]{0};
        int n=strlen(p);
        for(int i=0; i<n; i++)
            if(ok[p[i]-'A']==0)
            {
                frecv[p[i]-'A']++;
                ok[p[i]-'A']=1;
            }
        p=strtok(NULL, " .,/?!-");
    }
    int maxim=-1;
    char l;
    for(int i=0; i<26; i++)
        if(maxim<frecv[i])
        {
            maxim=frecv[i];
            l=i+'A';
        }
    printf("%c %d\n", l, maxim);
}
void cerinta_3()
{
    for(int i=25; i>=0; i--)
        if(fr[i]>0)
            printf("%c", i+'A');
    printf("\n");
}
void sort_alfabetic(char litere[30], int m)
{
    for(int i=0; i<m-1; i++)
        for(int j=i+1; j<m; j++)
            if(litere[i]>litere[j])
            {
                char aux=litere[i];
                litere[i]=litere[j];
                litere[j]=aux;
            }
}
int verificare(char litere[30], int m, char c)
{
    for(int i=0; i<m; i++)
        if(litere[i]==c)
            return 0;
    return 1;
}
void cerinta_4()
{
    char aux[300];
    char litere[30];
    int m=0;
    strcpy(aux, s);
    char *p=strtok(aux, " .,/?!-");
    while(p)
    {
        int frecv[30]{0};
        int n=strlen(p);
        for(int i=0; i<n; i++)
        {
            frecv[p[i]-'A']++;
            if(frecv[p[i]-'A']>1 && verificare(litere, m, p[i])==1)
                litere[m++]=p[i];
        }
        p=strtok(NULL, " .,/?!-");
    }
    sort_alfabetic(litere, m);
    for(int i=0; i<m; i++)
        printf("%c", litere[i]);
    printf("\n");
}
void sortare()
{
    for(int i=0; i<k-1; i++)
        for(int j=i+1; j<k; j++)
            if(a[i].ap<a[j].ap)
            {
                litera aux=a[i];
                a[i]=a[j];
                a[j]=aux;
            }
            else if(a[i].ap==a[j].ap && a[i].l>a[j].l)
            {
                litera aux=a[i];
                a[i]=a[j];
                a[j]=aux;
            }
}
void cerinta_5()
{
    for(int i=0; i<26; i++)
        if(fr[i]>1)
        {
            a[k].l=i+'A';
            a[k++].ap=fr[i];
        }
    sortare();
    for(int i=0; i<k; i++)
        printf("%c", a[i].l);
    printf("\n");
}
int main()
{
    freopen("letters.in", "r", stdin);
    for(int acsl=0; acsl<2; acsl++)
    {
        gets(s);
        int n=strlen(s);
        for(int i=0; i<n; i++)
            if(s[i]>='a' && s[i]<='z')
                s[i]=s[i]-32;
        resetare();
        cerinta_1();
        cerinta_2();
        cerinta_3();
        cerinta_4();
        cerinta_5();
    }
    return 0;
}
