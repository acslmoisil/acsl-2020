#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

struct vec2{
    int x, y;
};

struct answer{
    int captured, pinned, height;
};

ifstream fin("plateau.in");

vec2 s = {5, 5};

vec2 inttovec2(int a){
    a--;
    return {a%s.x+1, a/s.x+1};
}

int mat[14][14];
void nuke(){
    for(int i = 0; i < 14; ++i){
        for(int j = 0; j < 14; ++j){
            mat[i][j] = 0;
        }
    }
}

void read(){
    char c, color;
    int pos, amt;
    fin >> pos;
    while(pos != 0){
        fin >> c >> amt >> color >> c;

        if(color == 'W'){
            amt *= -1;
        }
        vec2 posv = inttovec2(pos);
        mat[posv.x][posv.y] = amt;

        fin >> pos;
    }
}

bool inbounds(vec2 a){
    return (a.x >= 1 && a.x <= s.x && a.y >= 1 && a.y <= s.y);
}

bool betteranswer(answer lhs, answer rhs){
    if(lhs.captured != rhs.captured){
        return lhs.captured > rhs.captured;
    }else if(lhs.pinned != rhs.pinned){
        return lhs.pinned > rhs.pinned;
    }else{
        return lhs.height > rhs.height;
    }
}

answer addanswer(answer lhs, answer rhs){
    return {lhs.captured+rhs.captured, lhs.pinned+rhs.pinned, lhs.height+rhs.height};
}

answer choosebetter(answer lhs, answer rhs){
    if(betteranswer(lhs, rhs)){
        return lhs;
    }else{
        return rhs;
    }
}

answer bt(vec2 p, vec2 d, int m, int t=0){
    vec2 np = {p.x+d.x, p.y+d.y};
    answer a = {0,0,0};
    if(inbounds(p) && m >= 0){
        answer na;
        if(mat[p.x][p.y] >= 0){
            t += mat[p.x][p.y];
            for(int nt = 0; nt <= t; ++nt){
                answer na = bt(np, d, m-1, nt);
                na.height = max(na.height, t-nt);
                a = choosebetter(a, na);
            }
        }else{
            int whites = -mat[p.x][p.y];
            for(int nt = 0; nt <= t; ++nt){
                if(t-nt <= whites){
                    na = bt(np, d, m-1, nt);
                    if(t-nt == whites){
                        na.captured += whites;
                    }else if(t-nt < whites && t-nt > 0){
                        na.pinned += whites;
                    }
                    na.height = max(na.height, t-nt);

                    a = choosebetter(a, na);
                }
            }
        }
        vec2 jnp = {np.x+d.x, np.y+d.y};
        if(mat[np.x][np.y] != 0){
            a = choosebetter(a, bt(jnp, d, m-1, t));
        }
    }
    return a;
}

vector<vec2> dv = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}};
answer ans;
void allbt(vec2 p){
    for(auto d : dv){
        ans = choosebetter(ans, bt(p, d, mat[p.x][p.y]));
    }
}

void solve(){
    ans = {0,0,0};
    for(int x = 1; x <= s.x; ++x){
        for(int y = 1; y <= s.y; ++y){
            if(mat[x][y] > 0){
                allbt({x, y});
            }
        }
    }
    cout << ans.captured << " " << ans.pinned << " " << ans.height << "\n";
}

int main(){
    // ios_base::sync_with_stdio(false);
    for(int acsl = 0; acsl < 10; ++acsl){
        nuke();
        read();
        solve();
    }
    return 0;
}