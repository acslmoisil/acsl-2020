#include <iostream>
#include <cstdio>

using namespace std;

FILE *F = freopen("date.in", "r", stdin);

int matrix[10][10];

void reset(){
    for (int i = 0; i < 10; ++i) {
        for (int j = 0; j < 10; ++j) {
            matrix[i][j] = 0;
            if(i == 0 || j == 0 || i > 7 || j > 7)
                matrix[i][j] = -1;
        }
    }
    matrix[1][2] = matrix[1][3] = matrix[1][5] = matrix[1][6] = -1;
    matrix[2][1] = matrix[2][3] = matrix[2][5] = matrix[2][7] = -1;
    matrix[3][1] = matrix[3][2] = matrix[3][6] = matrix[3][7] = -1;
    matrix[4][4] = -1;
    matrix[5][1] = matrix[5][2] = matrix[5][6] = matrix[5][7] = -1;
    matrix[6][1] = matrix[6][3] = matrix[6][5] = matrix[6][7] = -1;
    matrix[7][2] = matrix[7][3] = matrix[7][5] = matrix[7][6] = -1;
}

bool mill(int lin, int col, int colour){
    if(lin == 1 && col == 1){
        if(colour == 3 && ((matrix[1][1] > 0 && matrix[1][4] > 0 && matrix[1][7] > 0) || (matrix[1][1] > 0 && matrix[4][1] > 0 && matrix[7][1] > 0)))
            return true;
        else if(colour < 3 && ((matrix[1][1] == colour && matrix[1][4] == colour && matrix[1][7] == colour) || (matrix[1][1] == colour && matrix[4][1] == colour && matrix[7][1] == colour)))
            return true;
    }
    if(lin == 1 && col == 4){
        if(colour == 3 && ((matrix[1][1] > 0 && matrix[1][4] > 0 && matrix[1][7] > 0) || (matrix[1][4] > 0 && matrix[2][4] > 0 && matrix[3][4] > 0)))
            return true;
        else if(colour < 3 && ((matrix[1][1] == colour && matrix[1][4] == colour && matrix[1][7] == colour) || (matrix[1][4] == colour && matrix[2][4] == colour && matrix[3][4] == colour)))
            return true;
    }
    if(lin == 1 && col == 7){
        if(colour == 3 && ((matrix[1][1] > 0 && matrix[1][4] > 0 && matrix[1][7] > 0) || (matrix[1][7] > 0 && matrix[4][7] > 0 && matrix[7][7] > 0)))
            return true;
        else if(colour < 3 && ((matrix[1][1] == colour && matrix[1][4] == colour && matrix[1][7] == colour) || (matrix[1][7] == colour && matrix[4][7] == colour && matrix[7][7] == colour)))
            return true;
    }
    if(lin == 2 && col == 2){
        if(colour == 3 && ((matrix[2][2] > 0 && matrix[2][4] > 0 && matrix[2][6] > 0) || (matrix[2][2] > 0 && matrix[4][2] > 0 && matrix[6][2] > 0)))
            return true;
        else if(colour < 3 && ((matrix[2][2] == colour && matrix[2][4] == colour && matrix[2][6] == colour) || (matrix[2][2] == colour && matrix[4][2] == colour && matrix[6][2] == colour)))
            return true;
    }
    if(lin == 2 && col == 4){
        if(colour == 3 && ((matrix[2][2] > 0 && matrix[2][4] > 0 && matrix[2][6] > 0) || (matrix[1][4] > 0 && matrix[2][4] > 0 && matrix[3][4] > 0)))
            return true;
        else if(colour < 3 && ((matrix[2][2] == colour && matrix[2][4] == colour && matrix[2][6] == colour) || (matrix[1][4] == colour && matrix[2][4] == colour && matrix[3][4] == colour)))
            return true;
    }
    if(lin == 2 && col == 6){
        if(colour == 3 && ((matrix[2][2] > 0 && matrix[2][4] > 0 && matrix[2][6] > 0) || (matrix[2][6] > 0 && matrix[4][6] > 0 && matrix[6][6] > 0)))
            return true;
        else if(colour < 3 && ((matrix[2][2] == colour && matrix[2][4] == colour && matrix[2][6] == colour) || (matrix[2][6] == colour && matrix[4][6] == colour && matrix[6][6] == colour)))
            return true;
    }
    if(lin == 3 && col == 3){
        if(colour == 3 && ((matrix[3][3] > 0 && matrix[3][4] > 0 && matrix[3][5] > 0) || (matrix[3][3] > 0 && matrix[4][3] > 0 && matrix[5][3] > 0)))
            return true;
        else if(colour < 3 && ((matrix[3][3] == colour && matrix[3][4] == colour && matrix[3][5] == colour) || (matrix[3][3] == colour && matrix[4][3] == colour && matrix[5][3] == colour)))
            return true;
    }
    if(lin == 3 && col == 4){
        if(colour == 3 && ((matrix[3][3] > 0 && matrix[3][4] > 0 && matrix[3][5] > 0) || (matrix[1][4] > 0 && matrix[2][4] > 0 && matrix[3][4] > 0)))
            return true;
        else if(colour < 3 && ((matrix[3][3] == colour && matrix[3][4] == colour && matrix[3][5] == colour) || (matrix[1][4] == colour && matrix[2][4] == colour && matrix[3][4] == colour)))
            return true;
    }
    if(lin == 3 && col == 5){
        if(colour == 3 && ((matrix[3][3] > 0 && matrix[3][4] > 0 && matrix[3][5] > 0) || (matrix[3][5] > 0 && matrix[4][5] > 0 && matrix[5][5] > 0)))
            return true;
        else if(colour < 3 && ((matrix[3][3] == colour && matrix[3][4] == colour && matrix[3][5] == colour) || (matrix[3][5] == colour && matrix[4][5] == colour && matrix[5][5] == colour)))
            return true;
    }
    if(lin == 4 && col == 1){
        if(colour == 3 && ((matrix[4][1] > 0 && matrix[4][2] > 0 && matrix[4][3] > 0) || (matrix[1][1] > 0 && matrix[4][1] > 0 && matrix[7][1] > 0)))
            return true;
        else if(colour < 3 && ((matrix[4][1] == colour && matrix[4][2] == colour && matrix[4][3] == colour) || (matrix[1][1] == colour && matrix[4][1] == colour && matrix[7][1] == colour)))
            return true;
    }
    if(lin == 4 && col == 2){
        if(colour == 3 && ((matrix[4][1] > 0 && matrix[4][2] > 0 && matrix[4][3] > 0) || (matrix[2][2] > 0 && matrix[4][2] > 0 && matrix[6][2] > 0)))
            return true;
        else if(colour < 3 && ((matrix[4][1] == colour && matrix[4][2] == colour && matrix[4][3] == colour) || (matrix[2][2] == colour && matrix[4][2] == colour && matrix[6][2] == colour)))
            return true;
    }
    if(lin == 4 && col == 3){
        if(colour == 3 && ((matrix[4][1] > 0 && matrix[4][2] > 0 && matrix[4][3] > 0) || (matrix[3][3] > 0 && matrix[4][3] > 0 && matrix[5][3] > 0)))
            return true;
        else if(colour < 3 && ((matrix[4][1] == colour && matrix[4][2] == colour && matrix[4][3] == colour) || (matrix[3][3] == colour && matrix[4][3] == colour && matrix[5][3] == colour)))
            return true;
    }
    if(lin == 4 && col == 5){
        if(colour == 3 && ((matrix[4][5] > 0 && matrix[4][6] > 0 && matrix[4][7] > 0) || (matrix[3][5] > 0 && matrix[4][5] > 0 && matrix[5][5] > 0)))
            return true;
        else if(colour < 3 && ((matrix[4][5] == colour && matrix[4][6] == colour && matrix[4][7] == colour) || (matrix[3][5] == colour && matrix[4][5] == colour && matrix[5][5] == colour)))
            return true;
    }
    if(lin == 4 && col == 6){
        if(colour == 3 && ((matrix[4][5] > 0 && matrix[4][6] > 0 && matrix[4][7] > 0) || (matrix[2][6] > 0 && matrix[4][6] > 0 && matrix[6][6] > 0)))
            return true;
        else if(colour < 3 && ((matrix[4][5] == colour && matrix[4][6] == colour && matrix[4][7] == colour) || (matrix[2][6] == colour && matrix[4][6] == colour && matrix[6][6] == colour)))
            return true;
    }
    if(lin == 4 && col == 7){
        if(colour == 3 && ((matrix[4][5] > 0 && matrix[4][6] > 0 && matrix[4][7] > 0) || (matrix[1][7] > 0 && matrix[4][7] > 0 && matrix[7][7] > 0)))
            return true;
        else if(colour < 3 && ((matrix[4][5] == colour && matrix[4][6] == colour && matrix[4][7] == colour) || (matrix[1][7] == colour && matrix[4][7] == colour && matrix[7][7] == colour)))
            return true;
    }
    if(lin == 5 && col == 3){
        if(colour == 3 && ((matrix[5][3] > 0 && matrix[5][4] > 0 && matrix[5][5] > 0) || (matrix[3][3] > 0 && matrix[4][3] > 0 && matrix[5][3] > 0)))
            return true;
        else if(colour < 3 && ((matrix[5][3] == colour && matrix[5][4] == colour && matrix[5][5] == colour) || (matrix[3][3] == colour && matrix[4][3] == colour && matrix[5][3] == colour)))
            return true;
    }
    if(lin == 5 && col == 4){
        if(colour == 3 && ((matrix[5][3] > 0 && matrix[5][4] > 0 && matrix[5][5] > 0) || (matrix[5][4] > 0 && matrix[6][4] > 0 && matrix[7][4] > 0)))
            return true;
        else if(colour < 3 && ((matrix[5][3] == colour && matrix[5][4] == colour && matrix[5][5] == colour) || (matrix[5][4] == colour && matrix[6][4] == colour && matrix[7][4] == colour)))
            return true;
    }
    if(lin == 5 && col == 5){
        if(colour == 3 && ((matrix[5][3] > 0 && matrix[5][4] > 0 && matrix[5][5] > 0) || (matrix[3][5] > 0 && matrix[4][5] > 0 && matrix[5][5] > 0)))
            return true;
        else if(colour < 3 && ((matrix[5][3] == colour && matrix[5][4] == colour && matrix[5][5] == colour) || (matrix[3][5] == colour && matrix[4][5] == colour && matrix[5][5] == colour)))
            return true;
    }
    if(lin == 6 && col == 2){
        if(colour == 3 && ((matrix[6][2] > 0 && matrix[6][4] > 0 && matrix[6][6] > 0) || (matrix[2][2] > 0 && matrix[4][2] > 0 && matrix[6][2] > 0)))
            return true;
        else if(colour < 3 && ((matrix[6][2] == colour && matrix[6][4] == colour && matrix[6][6] == colour) || (matrix[2][2] == colour && matrix[4][2] == colour && matrix[6][2] == colour)))
            return true;
    }
    if(lin == 6 && col == 4){
        if(colour == 3 && ((matrix[6][2] > 0 && matrix[6][4] > 0 && matrix[6][6] > 0) || (matrix[5][4] > 0 && matrix[6][4] > 0 && matrix[7][4] > 0)))
            return true;
        else if(colour < 3 && ((matrix[6][2] == colour && matrix[6][4] == colour && matrix[6][6] == colour) || (matrix[5][4] == colour && matrix[6][4] == colour && matrix[7][4] == colour)))
            return true;
    }
    if(lin == 6 && col == 6){
        if(colour == 3 && ((matrix[6][2] > 0 && matrix[6][4] > 0 && matrix[6][6] > 0) || (matrix[2][6] > 0 && matrix[4][6] > 0 && matrix[6][6] > 0)))
            return true;
        else if(colour < 3 && ((matrix[6][2] == colour && matrix[6][4] == colour && matrix[6][6] == colour) || (matrix[2][6] == colour && matrix[4][6] == colour && matrix[6][6] == colour)))
            return true;
    }
    if(lin == 7 && col == 1){
        if(colour == 3 && ((matrix[7][1] > 0 && matrix[7][4] > 0 && matrix[7][7] > 0) || (matrix[1][1] > 0 && matrix[4][1] > 0 && matrix[7][1] > 0)))
            return true;
        else if(colour < 3 && ((matrix[7][1] == colour && matrix[7][4] == colour && matrix[7][7] == colour) || (matrix[1][1] == colour && matrix[4][1] == colour && matrix[7][1] == colour)))
            return true;
    }
    if(lin == 7 && col == 4){
        if(colour == 3 && ((matrix[7][1] > 0 && matrix[7][4] > 0 && matrix[7][7] > 0) || (matrix[5][4] > 0 && matrix[6][4] > 0 && matrix[7][4] > 0)))
            return true;
        else if(colour < 3 && ((matrix[7][1] == colour && matrix[7][4] == colour && matrix[7][7] == colour) || (matrix[5][4] == colour && matrix[6][4] == colour && matrix[7][4] == colour)))
            return true;
    }
    if(lin == 7 && col == 7){
        if(colour == 3 && ((matrix[7][1] > 0 && matrix[7][4] > 0 && matrix[7][7] > 0) || (matrix[1][7] > 0 && matrix[4][7] > 0 && matrix[7][7] > 0)))
            return true;
        else if(colour < 3 && ((matrix[7][1] == colour && matrix[7][4] == colour && matrix[7][7] == colour) || (matrix[1][7] == colour && matrix[4][7] == colour && matrix[7][7] == colour)))
            return true;
    }
    return false;
}

bool parc1(int lin, int col){
    int colour = matrix[lin][col];
    matrix[lin][col] = 0;
    for (int i = 1; i < 8; ++i) {
        for (int j = 1; j < 8; ++j) {
            if((i == lin && j == col) || matrix[i][j] != 0)
                continue;
            matrix[i][j] = colour;
            if(mill(i, j, colour)){
                cout<<(char)(j - 1 + 'a')<<i<<"\n";
                matrix[lin][col] = colour;
                matrix[i][j] = 0;
                return true;
            }
            matrix[i][j] = 0;
        }
    }
    matrix[lin][col] = colour;
    return false;
}

bool parc2(int lin, int col){
    int colour = matrix[lin][col];
    matrix[lin][col] = 0;
    for (int i = 1; i < 8; ++i) {
        for (int j = 1; j < 8; ++j) {
            if((i == lin && j == col) || matrix[i][j] != 0)
                continue;
            matrix[i][j] = colour;
            if(mill(i, j, 3)){
                cout<<(char)(j - 1 + 'a')<<i<<"\n";
                matrix[lin][col] = colour;
                matrix[i][j] = 0;
                return true;
            }
            matrix[i][j] = 0;
        }
    }
    matrix[lin][col] = colour;
    return false;
}

bool parc3(int lin, int col){
    for (int i = 1; i < 8; ++i) {
        for (int j = 1; j < 8; ++j) {
            if(matrix[i][j] == 0){
                cout<<(char)(j - 1 + 'a')<<i<<"\n";
                return true;
            }
        }
    }
    return false;
}

int main() {
    for (int acsl = 0; acsl < 2; ++acsl) {
        int pieseA, pieseN;
        reset();
        scanf("%d", &pieseN);
        for (int i = 0; i < pieseN; ++i) {
            char col;
            int lin;
            scanf(", %c%d", &col, &lin);
            matrix[lin][col - 'a' + 1] = 1;
        }
        scanf("\n%d", &pieseA);
        for (int i = 0; i < pieseA; ++i) {
            char col;
            int lin;
            scanf(", %c%d", &col, &lin);
            matrix[lin][col - 'a' + 1] = 2;
        }
        for (int inputs = 0; inputs < 5; ++inputs) {
            char col;
            int lin;
            scanf("\n%c%d", &col, &lin);
            if(parc1(lin, (int)(col - 'a' + 1))){
                continue;
            }else if(parc2(lin, (int)(col - 'a' + 1))){
                continue;
            }else parc3(lin, (int)(col - 'a' + 1));
        }
        scanf("\n");
    }
    return 0;
}
