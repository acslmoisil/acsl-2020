#include <iostream>
#include <fstream>
#include <vector>
#include <string>

using namespace std;

ifstream fin("morris.in");

struct vec2{
	int x, y;
};

typedef vector<vec2> Moara;

vector<Moara> mori = {
	{{1, 1}, {4, 1}, {7, 1}},
	{{2, 2}, {4, 2}, {6, 2}},
	{{3, 3}, {4, 3}, {5, 3}},
	
	{{1, 4}, {2, 4}, {3, 4}},
	{{5, 4}, {6, 4}, {7, 4}},
	
	{{3, 5}, {4, 5}, {5, 5}},
	{{2, 6}, {4, 6}, {6, 6}},
	{{1, 7}, {4, 7}, {7, 7}},
	
	{{1, 1}, {1, 4}, {1, 7}},
	{{2, 2}, {2, 4}, {2, 6}},
	{{3, 3}, {3, 4}, {3, 5}},
	
	{{4, 1}, {4, 2}, {4, 3}},
	{{4, 5}, {4, 6}, {4, 7}},
	
	{{5, 3}, {5, 4}, {5, 5}},
	{{6, 2}, {6, 4}, {6, 6}},
	{{7, 1}, {7, 4}, {7, 7}},
};

int mat[14][14];
//0 - liber
//1 - negru
//2 - alb

vec2 tovec2(string a){
	return {a[0]-'a'+1, a[1]-'0'};
}

void nuke(){
	for(int i = 0; i < 14; ++i){
		for(int j = 0; j < 14; ++j){
			mat[i][j] = 0;
		}
	}
}

void read(){
	int n;
	string s;
	
	fin >> n >> s;
	for(int i = 0; i < n; ++i){
		fin >> s;
		if(s.back() == ','){
			s.pop_back();
		}
		vec2 pos = tovec2(s);
		mat[pos.x][pos.y] = 1;
	}
	
	fin >> n >> s;
	for(int i = 0; i < n; ++i){
		fin >> s;
		if(s.back() == ','){
			s.pop_back();
		}
		vec2 pos = tovec2(s);
		mat[pos.x][pos.y] = 2;
	}
}

int countcolor(const Moara &m, int c){
	int r = 0;
	for(auto p : m){
		r += (mat[p.x][p.y] == c);
	}
	return r;
}

vec2 findcolor(const Moara &m, int c){
	vec2 r;
	for(auto p : m){
		if(mat[p.x][p.y] == c){
			r = p;
		}
	}
	return r;
}

bool bettervec2(vec2 lhs, vec2 rhs){
	if(lhs.y != rhs.y){
		return lhs.y < rhs.y;
	}else{
		return lhs.x < rhs.x;
	}
}

vec2 findbestcolor(const Moara &m, int c){
	vec2 r = {0, 0};
	for(auto p : m){
		if(mat[p.x][p.y] == c && (r.x == 0 || bettervec2(p, r))){
			r = p;
		}
	}
	return r;
}

string tostring(vec2 a){
	string r = "";
	r += (a.x)+'a'-1;
	r += a.y+'0';
	return r;
}

vec2 findnewpos(vec2 pos, int color, int othercolor){
	vec2 newpos = {0, 0};
	
	for(auto &m : mori){
		if(countcolor(m, color) == 2 && countcolor(m, 0) == 1){
			vec2 p = findcolor(m, 0);
			if(newpos.x == 0 || bettervec2(p, newpos)){
				newpos = p;
			}
		}
	}
	if(newpos.x != 0)
		return newpos;
	
	for(auto &m : mori){
		if(countcolor(m, othercolor) == 2 && countcolor(m, 0) == 1){
			vec2 p = findcolor(m, 0);
			if(newpos.x == 0 || bettervec2(p, newpos)){
				newpos = p;
			}
		}
	}
	if(newpos.x != 0)
		return newpos;
	
	
	for(auto &m : mori){
		vec2 p = findbestcolor(m, 0);
		if(newpos.x == 0 || bettervec2(p, newpos)){
			newpos = p;
		}
	}
	return newpos;
}

void solve(){
	string s;
	fin >> s;
	
	vec2 pos = tovec2(s);
	int color = mat[pos.x][pos.y];
	int othercolor = 3-color;
	
	mat[pos.x][pos.y] = -1;
	vec2 newpos = findnewpos(pos, color, othercolor);
	mat[pos.x][pos.y] = color;
	
	cout << tostring(newpos) << "\n";
}

int main(){
	// ios_base::sync_with_stdio(false);
	for(int acsl = 0; acsl < 2; ++acsl){
		nuke();
		read();
		for(int acsl2 = 0; acsl2 < 5; ++acsl2){
			solve();
		}
	}
	return 0;
}