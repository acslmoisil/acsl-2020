#include <iostream>
#include <fstream>
#include <cstring>
#include <iomanip>
using namespace std;
struct player
{
    char name[40],goal[10];
    double punctaj, procentaj, incerc;
    int echipa;
    int zona1, zona2;
} a[10];

int g[10];
double s;
int e1, e2;
void sortare()
{
    for(int i=0; i<9; i++)
        for(int j=i+1; j<10; j++)
            if(a[i].punctaj>a[j].punctaj)
            {
                player aux=a[i];
                a[i]=a[j];
                a[j]=aux;
            }
}
void sortare_zona2()
{
    for(int i=0; i<9; i++)
        for(int j=i+1; j<10; j++)
            if(a[i].zona2<a[j].zona2)
            {
                player aux=a[i];
                a[i]=a[j];
                a[j]=aux;
            }
}
void sortare_zona1()
{
    for(int i=0; i<9; i++)
        for(int j=i+1; j<10; j++)
            if(a[i].zona1<a[j].zona1)
            {
                player aux=a[i];
                a[i]=a[j];
                a[j]=aux;
            }
}
int baza16(char c)
{
    if(c>='0' && c<='9')
        return c-'0';
    if(c=='A')
        return 10;
    if(c=='B')
        return 11;
    if(c=='C')
        return 12;
    if(c=='D')
        return 13;
    if(c=='E')
        return 14;
    if(c=='F')
        return 15;
}
int main()
{
    ifstream f("ACSLBall.in");
    for(int acsl=0; acsl<10; acsl++)
    {
        f>>a[acsl].name>>a[acsl].goal;
        int l=strlen(a[acsl].name);
        a[acsl].name[l-1]='\0';
        if(acsl<=4)
            a[acsl].echipa=1;
        else
            a[acsl].echipa=2;
        a[acsl].incerc=baza16(a[acsl].goal[4]);
        double goluri=0;
        int n=strlen(a[acsl].goal);
        for(int i=0; i<n-1; i++)
        {
            g[i]=a[acsl].goal[i]-'0';
            goluri+=g[i];
        }
        a[acsl].zona1=g[0];
        a[acsl].zona2=g[1];
        for(int i=0; i<4; i++)
            a[acsl].punctaj+=g[i]*(i+1);
        if(a[acsl].echipa==1)
            e1+=a[acsl].punctaj;
        else
            e2+=a[acsl].punctaj;
        a[acsl].procentaj=goluri/a[acsl].incerc;
    }
    cout<<'\n';
    for(int i=0; i<10; i++)
        s+=a[i].punctaj;
    cout<<fixed<<setprecision(1)<<s/10<<'\n';
    sortare();
    cout<<fixed<<setprecision(1)<<(a[4].punctaj+a[5].punctaj)/2<<'\n';
    char ech1[20]="";
    char ech2[20]="";
    int ok1=0, ok2=0;
    for(int i=9; i>=0 && (ok1==0 || ok2==0); i--)
        if(ok1==0 && a[i].echipa==1)
        {
            strcpy(ech1, a[i].name);
            ok1=1;
        }
        else if(ok2==0 && a[i].echipa==2)
        {
            strcpy(ech2, a[i].name);
            ok2=1;
        }
    cout<<ech1<<' '<<ech2<<'\n';
    cout<<a[9].name<<' '<<a[8].name<<'\n';
    if(e1>e2)
        cout<<e1<<' '<<e2<<'\n';
    else
        cout<<e2<<' '<<e1<<'\n';
    cout<<a[0].name<<' ';
    for(int i=1; i<10; i++)
        if(a[i].punctaj!=a[0].punctaj)
            break;
        else
            cout<<a[i].name<<' ';
    cout<<'\n';
    double ep1=-999, ep2=999;
    char num1[20], num2[20];
    for(int i=0; i<10; i++)
        if(a[i].echipa==1 && a[i].procentaj>ep1)
        {
            ep1=a[i].procentaj;
            strcpy(num1, a[i].name);
        }
        else if(a[i].echipa==2 && a[i].procentaj<ep2)
        {
            ep2=a[i].procentaj;
            strcpy(num2, a[i].name);
        }
    cout<<num1<<'\n'<<num2<<'\n';
    sortare_zona2();
    cout<<a[0].name<<' ';
    for(int i=1; i<10; i++)
        if(a[i].zona2!=a[0].zona2)
            break;
        else
            cout<<a[i].name<<' ';
    cout<<'\n';
    sortare_zona1();
    cout<<a[0].name<<' ';
    for(int i=1; i<10; i++)
        if(a[i].zona1!=a[0].zona1)
            break;
        else
            cout<<a[i].name<<' ';
    cout<<'\n';
    return 0;
}