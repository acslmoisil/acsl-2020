#include <iostream>
#include <cstdio>
using namespace std;
int a[10][10];
int pi, pj;
int linie(char c)
{
    if(c=='7')
        return 0;
    if(c=='6')
        return 1;
    if(c=='5')
        return 2;
    if(c=='4')
        return 3;
    if(c=='3')
        return 4;
    if(c=='2')
        return 5;
    if(c=='1')
        return 6;
}
int coloana(char c)
{
    return c-'a';
}
void creare_mat()
{
    for(int i=0; i<7; i++)
        for(int j=0; j<7; j++)
            a[i][j]=0;
    a[0][1]=-1;
    a[0][2]=-1;
    a[0][4]=-1;
    a[0][5]=-1;
    a[1][0]=-1;
    a[1][2]=-1;
    a[1][4]=-1;
    a[1][6]=-1;
    a[2][0]=-1;
    a[2][6]=-1;
    a[3][3]=-1;
    a[4][0]=-1;
    a[4][6]=-1;
    a[5][0]=-1;
    a[5][2]=-1;
    a[5][4]=-1;
    a[5][6]=-1;
    a[6][1]=-1;
    a[6][2]=-1;
    a[6][4]=-1;
    a[6][5]=-1;
}
int verificare(int color, int i, int j)
{
    if(a[i][j]!=0)
        return 0;
    if(color==1)
    {
        ////////////////////////////////
        if(i==0 && j==0)
        {
            if(a[3][0]==1 && a[6][0]==1)
                return 1;
            if(a[0][3]==1 && a[0][6]==1)
                return 1;
        }
        else if(i==3 && j==0)
        {
            if(a[0][0]==1 && a[6][0]==1)
                return 1;
            if(a[3][1]==1 && a[3][2]==1)
                return 1;
        }
        else if(i==6 && j==0)
        {
            if(a[4][0]==1 && a[0][0]==1)
                return 1;
            if(a[6][3]==1 && a[6][6]==1)
                return 1;
        }
        else if(i==1 && j==1)
        {
            if(a[1][3]==1 && a[1][5]==1)
                return 1;
            if(a[3][1]==1 && a[5][1]==1)
                return 1;
        }
        else if(i==3 && j==1)
        {
            if(a[1][1]==1 && a[5][1]==1)
                return 1;
            if(a[3][0]==1 && a[3][2]==1)
                return 1;
        }
        else if(i==5 && j==1)
        {
            if(a[1][1]==1 && a[3][1]==1)
                return 1;
            if(a[5][3]==1 && a[5][5]==1)
                return 1;
        }
        else if(i==2 && j==2)
        {
            if(a[2][3]==1 && a[2][4]==1)
                return 1;
            if(a[3][2]==1 && a[4][2]==1)
                return 1;
        }
        else if(i==3 && j==2)
        {
            if(a[3][0]==1 && a[3][1]==1)
                return 1;
            if(a[2][2]==1 && a[2][4]==1)
                return 1;
        }
        else if(i==4 && j==2)
        {
            if(a[4][3]==1 && a[4][4]==1)
                return 1;
            if(a[2][2]==1 && a[3][2]==1)
                return 1;
        }
        else if(i==0 && j==3)
        {
            if(a[1][3]==1 && a[2][3]==1)
                return 1;
            if(a[0][0]==1 && a[0][6]==1)
                return 1;
        }
        else if(i==1 && j==3)
        {
            if(a[1][1]==1 && a[1][5]==1)
                return 1;
            if(a[0][3]==1 && a[2][3]==1)
                return 1;
        }
        else if(i==2 && j==3)
        {
            if(a[2][2]==1 && a[2][4]==1)
                return 1;
            if(a[0][3]==1 && a[1][3]==1)
                return 1;
        }
        else if(i==4 && j==3)
        {
            if(a[4][2]==1 && a[4][4]==1)
                return 1;
            if(a[5][3]==1 && a[6][3]==1)
                return 1;
        }
        else if(i==5 && j==3)
        {
            if(a[5][1]==1 && a[5][5]==1)
                return 1;
            if(a[4][3]==1 && a[6][3]==1)
                return 1;
        }
        else if(i==6 && j==3)
        {
            if(a[6][0]==1 && a[6][6]==1)
                return 1;
            if(a[4][3]==1 && a[5][3]==1)
                return 1;
        }
        else if(i==2 && j==4)
        {
            if(a[2][2]==1 && a[2][3]==1)
                return 1;
            if(a[3][4]==1 && a[4][4]==1)
                return 1;
        }
        else if(i==3 && j==4)
        {
            if(a[3][5]==1 && a[3][6]==1)
                return 1;
            if(a[2][4]==1 && a[4][4]==1)
                return 1;
        }
        else if(i==4 && j==4)
        {
            if(a[4][2]==1 && a[4][3]==1)
                return 1;
            if(a[2][4]==1 && a[3][4]==1)
                return 1;
        }
        else if(i==1 && j==5)
        {
            if(a[3][5]==1 && a[5][5]==1)
                return 1;
            if(a[1][1]==1 && a[1][3]==1)
                return 1;
        }
        else if(i==3 && j==5)
        {
            if(a[1][5]==1 && a[5][5]==1)
                return 1;
            if(a[3][4]==1 && a[3][6]==1)
                return 1;
        }
        else if(i==5 && j==5)
        {
            if(a[1][5]==1 && a[3][5]==1)
                return 1;
            if(a[5][1]==1 && a[5][3]==1)
                return 1;
        }
        else if(i==0 && j==6)
        {
            if(a[0][0]==1 && a[0][3]==1)
                return 1;
            if(a[3][6]==1 && a[6][6]==1)
                return 1;
        }
        else if(i==3 && j==6)
        {
            if(a[3][4]==1 && a[3][5]==1)
                return 1;
            if(a[0][0]==1 && a[0][6]==1)
                return 1;
        }
        else if(i==6 && j==6)
        {
            if(a[6][0]==1 && a[6][3]==1)
                return 1;
            if(a[0][6]==1 && a[3][6]==1)
                return 1;
        }
        ///////////////////////////////
        if(i==0 && j==0)
        {
            if(a[3][0]==2 && a[6][0]==2)
                return 1;
            if(a[0][3]==2 && a[0][6]==2)
                return 1;
        }
        else if(i==3 && j==0)
        {
            if(a[0][0]==2 && a[6][0]==2)
                return 1;
            if(a[3][1]==2 && a[3][2]==2)
                return 1;
        }
        else if(i==6 && j==0)
        {
            if(a[4][0]==2 && a[0][0]==2)
                return 1;
            if(a[6][3]==2 && a[6][6]==2)
                return 1;
        }
        else if(i==1 && j==1)
        {
            if(a[1][3]==2 && a[1][5]==2)
                return 1;
            if(a[3][1]==2 && a[5][1]==2)
                return 1;
        }
        else if(i==3 && j==1)
        {
            if(a[1][1]==2 && a[5][1]==2)
                return 1;
            if(a[3][0]==2 && a[3][2]==2)
                return 1;
        }
        else if(i==5 && j==1)
        {
            if(a[1][1]==2 && a[3][1]==2)
                return 1;
            if(a[5][3]==2 && a[5][5]==2)
                return 1;
        }
        else if(i==2 && j==2)
        {
            if(a[2][3]==2 && a[2][4]==2)
                return 1;
            if(a[3][2]==2 && a[4][2]==2)
                return 1;
        }
        else if(i==3 && j==2)
        {
            if(a[3][0]==2 && a[3][1]==2)
                return 1;
            if(a[2][2]==2 && a[2][4]==2)
                return 1;
        }
        else if(i==4 && j==2)
        {
            if(a[4][3]==2 && a[4][4]==2)
                return 1;
            if(a[2][2]==2 && a[3][2]==2)
                return 1;
        }
        else if(i==0 && j==3)
        {
            if(a[1][3]==2 && a[2][3]==2)
                return 1;
            if(a[0][0]==2 && a[0][6]==2)
                return 1;
        }
        else if(i==1 && j==3)
        {
            if(a[1][1]==2 && a[1][5]==2)
                return 1;
            if(a[0][3]==2 && a[2][3]==2)
                return 1;
        }
        else if(i==2 && j==3)
        {
            if(a[2][2]==2 && a[2][4]==2)
                return 1;
            if(a[0][3]==2 && a[1][3]==2)
                return 1;
        }
        else if(i==4 && j==3)
        {
            if(a[4][2]==2 && a[4][4]==2)
                return 1;
            if(a[5][3]==2 && a[6][3]==2)
                return 1;
        }
        else if(i==5 && j==3)
        {
            if(a[5][1]==2 && a[5][5]==2)
                return 1;
            if(a[4][3]==2 && a[6][3]==2)
                return 1;
        }
        else if(i==6 && j==3)
        {
            if(a[6][0]==2 && a[6][6]==2)
                return 1;
            if(a[4][3]==2 && a[5][3]==2)
                return 1;
        }
        else if(i==2 && j==4)
        {
            if(a[2][2]==2 && a[2][3]==2)
                return 1;
            if(a[3][4]==2 && a[4][4]==2)
                return 1;
        }
        else if(i==3 && j==4)
        {
            if(a[3][5]==2 && a[3][6]==2)
                return 1;
            if(a[2][4]==2 && a[4][4]==2)
                return 1;
        }
        else if(i==4 && j==4)
        {
            if(a[4][2]==2 && a[4][3]==2)
                return 1;
            if(a[2][4]==2 && a[3][4]==2)
                return 1;
        }
        else if(i==1 && j==5)
        {
            if(a[3][5]==2 && a[5][5]==2)
                return 1;
            if(a[1][1]==2 && a[1][3]==2)
                return 1;
        }
        else if(i==3 && j==5)
        {
            if(a[1][5]==2 && a[5][5]==2)
                return 1;
            if(a[3][4]==2 && a[3][6]==2)
                return 1;
        }
        else if(i==5 && j==5)
        {
            if(a[1][5]==2 && a[3][5]==2)
                return 1;
            if(a[5][1]==2 && a[5][3]==2)
                return 1;
        }
        else if(i==0 && j==6)
        {
            if(a[0][0]==2 && a[0][3]==2)
                return 1;
            if(a[3][6]==2 && a[6][6]==2)
                return 1;
        }
        else if(i==3 && j==6)
        {
            if(a[3][4]==2 && a[3][5]==2)
                return 1;
            if(a[0][0]==2 && a[0][6]==2)
                return 1;
        }
        else if(i==6 && j==6)
        {
            if(a[6][0]==2 && a[6][3]==2)
                return 1;
            if(a[0][6]==2 && a[3][6]==2)
                return 1;
        }
    }
    else
    {
        if(i==0 && j==0)
        {
            if(a[3][0]==2 && a[6][0]==2)
                return 1;
            if(a[0][3]==2 && a[0][6]==2)
                return 1;
        }
        else if(i==3 && j==0)
        {
            if(a[0][0]==2 && a[6][0]==2)
                return 1;
            if(a[3][1]==2 && a[3][2]==2)
                return 1;
        }
        else if(i==6 && j==0)
        {
            if(a[4][0]==2 && a[0][0]==2)
                return 1;
            if(a[6][3]==2 && a[6][6]==2)
                return 1;
        }
        else if(i==1 && j==1)
        {
            if(a[1][3]==2 && a[1][5]==2)
                return 1;
            if(a[3][1]==2 && a[5][1]==2)
                return 1;
        }
        else if(i==3 && j==1)
        {
            if(a[1][1]==2 && a[5][1]==2)
                return 1;
            if(a[3][0]==2 && a[3][2]==2)
                return 1;
        }
        else if(i==5 && j==1)
        {
            if(a[1][1]==2 && a[3][1]==2)
                return 1;
            if(a[5][3]==2 && a[5][5]==2)
                return 1;
        }
        else if(i==2 && j==2)
        {
            if(a[2][3]==2 && a[2][4]==2)
                return 1;
            if(a[3][2]==2 && a[4][2]==2)
                return 1;
        }
        else if(i==3 && j==2)
        {
            if(a[3][0]==2 && a[3][1]==2)
                return 1;
            if(a[2][2]==2 && a[2][4]==2)
                return 1;
        }
        else if(i==4 && j==2)
        {
            if(a[4][3]==2 && a[4][4]==2)
                return 1;
            if(a[2][2]==2 && a[3][2]==2)
                return 1;
        }
        else if(i==0 && j==3)
        {
            if(a[1][3]==2 && a[2][3]==2)
                return 1;
            if(a[0][0]==2 && a[0][6]==2)
                return 1;
        }
        else if(i==1 && j==3)
        {
            if(a[1][1]==2 && a[1][5]==2)
                return 1;
            if(a[0][3]==2 && a[2][3]==2)
                return 1;
        }
        else if(i==2 && j==3)
        {
            if(a[2][2]==2 && a[2][4]==2)
                return 1;
            if(a[0][3]==2 && a[1][3]==2)
                return 1;
        }
        else if(i==4 && j==3)
        {
            if(a[4][2]==2 && a[4][4]==2)
                return 1;
            if(a[5][3]==2 && a[6][3]==2)
                return 1;
        }
        else if(i==5 && j==3)
        {
            if(a[5][1]==2 && a[5][5]==2)
                return 1;
            if(a[4][3]==2 && a[6][3]==2)
                return 1;
        }
        else if(i==6 && j==3)
        {
            if(a[6][0]==2 && a[6][6]==2)
                return 1;
            if(a[4][3]==2 && a[5][3]==2)
                return 1;
        }
        else if(i==2 && j==4)
        {
            if(a[2][2]==2 && a[2][3]==2)
                return 1;
            if(a[3][4]==2 && a[4][4]==2)
                return 1;
        }
        else if(i==3 && j==4)
        {
            if(a[3][5]==2 && a[3][6]==2)
                return 1;
            if(a[2][4]==2 && a[4][4]==2)
                return 1;
        }
        else if(i==4 && j==4)
        {
            if(a[4][2]==2 && a[4][3]==2)
                return 1;
            if(a[2][4]==2 && a[3][4]==2)
                return 1;
        }
        else if(i==1 && j==5)
        {
            if(a[3][5]==2 && a[5][5]==2)
                return 1;
            if(a[1][1]==2 && a[1][3]==2)
                return 1;
        }
        else if(i==3 && j==5)
        {
            if(a[1][5]==2 && a[5][5]==2)
                return 1;
            if(a[3][4]==2 && a[3][6]==2)
                return 1;
        }
        else if(i==5 && j==5)
        {
            if(a[1][5]==2 && a[3][5]==2)
                return 1;
            if(a[5][1]==2 && a[5][3]==2)
                return 1;
        }
        else if(i==0 && j==6)
        {
            if(a[0][0]==2 && a[0][3]==2)
                return 1;
            if(a[3][6]==2 && a[6][6]==2)
                return 1;
        }
        else if(i==3 && j==6)
        {
            if(a[3][4]==2 && a[3][5]==2)
                return 1;
            if(a[0][0]==2 && a[0][6]==2)
                return 1;
        }
        else if(i==6 && j==6)
        {
            if(a[6][0]==2 && a[6][3]==2)
                return 1;
            if(a[0][6]==2 && a[3][6]==2)
                return 1;
        }
        ////////////////////////////////
        if(i==0 && j==0)
        {
            if(a[3][0]==1 && a[6][0]==1)
                return 1;
            if(a[0][3]==1 && a[0][6]==1)
                return 1;
        }
        else if(i==3 && j==0)
        {
            if(a[0][0]==1 && a[6][0]==1)
                return 1;
            if(a[3][1]==1 && a[3][2]==1)
                return 1;
        }
        else if(i==6 && j==0)
        {
            if(a[4][0]==1 && a[0][0]==1)
                return 1;
            if(a[6][3]==1 && a[6][6]==1)
                return 1;
        }
        else if(i==1 && j==1)
        {
            if(a[1][3]==1 && a[1][5]==1)
                return 1;
            if(a[3][1]==1 && a[5][1]==1)
                return 1;
        }
        else if(i==3 && j==1)
        {
            if(a[1][1]==1 && a[5][1]==1)
                return 1;
            if(a[3][0]==1 && a[3][2]==1)
                return 1;
        }
        else if(i==5 && j==1)
        {
            if(a[1][1]==1 && a[3][1]==1)
                return 1;
            if(a[5][3]==1 && a[5][5]==1)
                return 1;
        }
        else if(i==2 && j==2)
        {
            if(a[2][3]==1 && a[2][4]==1)
                return 1;
            if(a[3][2]==1 && a[4][2]==1)
                return 1;
        }
        else if(i==3 && j==2)
        {
            if(a[3][0]==1 && a[3][1]==1)
                return 1;
            if(a[2][2]==1 && a[2][4]==1)
                return 1;
        }
        else if(i==4 && j==2)
        {
            if(a[4][3]==1 && a[4][4]==1)
                return 1;
            if(a[2][2]==1 && a[3][2]==1)
                return 1;
        }
        else if(i==0 && j==3)
        {
            if(a[1][3]==1 && a[2][3]==1)
                return 1;
            if(a[0][0]==1 && a[0][6]==1)
                return 1;
        }
        else if(i==1 && j==3)
        {
            if(a[1][1]==1 && a[1][5]==1)
                return 1;
            if(a[0][3]==1 && a[2][3]==1)
                return 1;
        }
        else if(i==2 && j==3)
        {
            if(a[2][2]==1 && a[2][4]==1)
                return 1;
            if(a[0][3]==1 && a[1][3]==1)
                return 1;
        }
        else if(i==4 && j==3)
        {
            if(a[4][2]==1 && a[4][4]==1)
                return 1;
            if(a[5][3]==1 && a[6][3]==1)
                return 1;
        }
        else if(i==5 && j==3)
        {
            if(a[5][1]==1 && a[5][5]==1)
                return 1;
            if(a[4][3]==1 && a[6][3]==1)
                return 1;
        }
        else if(i==6 && j==3)
        {
            if(a[6][0]==1 && a[6][6]==1)
                return 1;
            if(a[4][3]==1 && a[5][3]==1)
                return 1;
        }
        else if(i==2 && j==4)
        {
            if(a[2][2]==1 && a[2][3]==1)
                return 1;
            if(a[3][4]==1 && a[4][4]==1)
                return 1;
        }
        else if(i==3 && j==4)
        {
            if(a[3][5]==1 && a[3][6]==1)
                return 1;
            if(a[2][4]==1 && a[4][4]==1)
                return 1;
        }
        else if(i==4 && j==4)
        {
            if(a[4][2]==1 && a[4][3]==1)
                return 1;
            if(a[2][4]==1 && a[3][4]==1)
                return 1;
        }
        else if(i==1 && j==5)
        {
            if(a[3][5]==1 && a[5][5]==1)
                return 1;
            if(a[1][1]==1 && a[1][3]==1)
                return 1;
        }
        else if(i==3 && j==5)
        {
            if(a[1][5]==1 && a[5][5]==1)
                return 1;
            if(a[3][4]==1 && a[3][6]==1)
                return 1;
        }
        else if(i==5 && j==5)
        {
            if(a[1][5]==1 && a[3][5]==1)
                return 1;
            if(a[5][1]==1 && a[5][3]==1)
                return 1;
        }
        else if(i==0 && j==6)
        {
            if(a[0][0]==1 && a[0][3]==1)
                return 1;
            if(a[3][6]==1 && a[6][6]==1)
                return 1;
        }
        else if(i==3 && j==6)
        {
            if(a[3][4]==1 && a[3][5]==1)
                return 1;
            if(a[0][0]==1 && a[0][6]==1)
                return 1;
        }
        else if(i==6 && j==6)
        {
            if(a[6][0]==1 && a[6][3]==1)
                return 1;
            if(a[0][6]==1 && a[3][6]==1)
                return 1;
        }
    }
}
void afisare(int i, int j)
{
    char c=j+'a';
    cout<<c;
    cout<<7-i<<'\n';
}
void bak(int i, int j, int ok)
{
    if(ok==1)
        return;
    if(verificare(a[i][j], i, j)==1)
    {
        afisare(i, j);
        bak(i, j+1, 1);
        return;
    }
    if(i==-1)
        return;
    if(j==7)
    {
        bak(i-1, 0, ok);
        return;
    }
    bak(i, j+1, ok);
}
int main()
{
    freopen("morris.in", "r", stdin);
    for(int acsl=0; acsl<2; acsl++)
    {
        creare_mat();
        for(int i=0; i<2; i++)
        {
            int n;
            char poz[5];
            scanf("%d, ", &n);
            for(int j=0; j<n; j++)
            {
                scanf("%s ", poz);
                poz[2]=0;
                int l=linie(poz[1]);
                int c=coloana(poz[0]);
                a[l][c]=i+1;
            }
            scanf("\n");
        }
        for(int mac=0; mac<5; mac++)
        {
            char poz[2];
            scanf("%s\n", poz);
            int l=linie(poz[1]);
            int c=coloana(poz[0]);
            int ok=a[l][c];
            a[l][c]=0;
            bak(6, 0, 0);
            a[l][c]=ok;

        }
    }
    return 0;
}