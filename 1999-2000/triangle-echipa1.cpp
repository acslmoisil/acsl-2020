#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <cmath>
#include <vector>
#include <iomanip>

using namespace std;

struct vec2{
	double x, y;
};

struct dr{
	double a, b, c;
	dr(){}
	dr(vec2 v1, vec2 v2){
		a = v1.y - v2.y;
		b = v2.x - v1.x;
		c = v1.x*v2.y - v2.x*v1.y;
	}
};

vec2 intersect(dr d1, dr d2){
	double w = (d1.a*d2.b - d2.a*d1.b);
	return {
		(d1.b*d2.c - d2.b*d1.c) / w,
		(d2.a*d1.c - d1.a*d2.c) / w
	};
}

double pythagora(double a, double b){
	return sqrt(a*a + b*b);
}

double distance(vec2 v1, vec2 v2){
	return pythagora(v1.x-v2.x, v1.y-v2.y);
}

map<char, vec2> pts = {
	{'A', {0, 0}}, {'B', {2, 3}}, {'C', {-2, 5}},
	{'D', {-2, -4}}, {'E', {1, -2}}, {'F', {4, 1}},
	{'G', {-4, 1}}, {'H', {-2, 0}}, {'I', {0, -6}},
	{'J', {3, -4}}, {'K', {5, 5}}, {'L', {3, 0}},
};

ifstream fin("triangle.in");

vector<dr> ds;

void read(){
	ds.clear();
	
	string s;
	for(int i = 0; i < 3; ++i){
		fin >> s;
		if(s.back() == ','){
			s.pop_back();
		}
		ds.push_back(dr(pts[s[0]], pts[s[1]]));
	}
}

vector<vec2> ps;

void solvewrite(){
	ps.clear();
	double ans = 0;
	
	for(int i = 0; i < 3; ++i){
		ps.push_back(intersect(ds[i], ds[(i+1)%3]));
	}
	for(int i = 0; i < 3; ++i){
		ans += distance(ps[i], ps[(i+1)%3]);
	}
	
	cout << fixed << setprecision(6);
	cout << ans << "\n";
}

int main(){
	// ios_base::sync_with_stdio(false);
	for(int ascl = 0; ascl < 10; ++ascl){
		read();
		solvewrite();
	}
	return 0;
}