#include <iostream>
#include <fstream>
#include <stack>
#include <cstring>

using namespace std;

ifstream f("post2in.in");

char line[150];

int main() {
    for (int acsl=0; acsl < 10; ++acsl) {
        stack<string> exp;
        f.getline(line, 150);
        int nrl = strlen(line), ind = 0;
        while (ind < nrl) {
            int i;
            string cur, formare;
            for (i = ind; line[i] != ' ' && i < nrl; ++i)
                cur += line[i];
            ind = i + 1;
            if (cur != "min" && cur != "max" && cur != "abs")
                exp.push(cur);
            else {
                if (cur == "min" || cur == "max") {
                    string s1, s2;
                    s2 = exp.top();
                    exp.pop();
                    s1 = exp.top();
                    exp.pop();
                    formare += cur;
                    formare += "(";
                    formare += s1;
                    formare += ", ";
                    formare += s2;
                    formare += ")";
                    exp.push(formare);
                } else {
                    string s1 = exp.top();
                    exp.pop();
                    formare += "abs(";
                    formare += s1;
                    formare += ")";
                    exp.push(formare);
                }
            }
        }
        cout << exp.top() << '\n';
        exp.pop();
    }
    return 0;
}
