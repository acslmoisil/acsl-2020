#include <iostream>
#include <fstream>
#include <string>
#include <stack>

using namespace std;

ifstream fin("post2in.in");

string s;
void read(){
	getline(fin, s);
	s += ' ';
}

stack<string> operands;
void solve(){
	string t;
	for(auto c : s){
		if(c == ' ' || c == '\t'){
			if(t == "min" || t == "max"){
				string b = operands.top();operands.pop();
				string a = operands.top();operands.pop();
				operands.push(t + "(" + a + ", " + b + ")");
			}else if(t == "abs"){
				string a = operands.top();operands.pop();
				operands.push(t + "(" + a + ")");
			}else if(!t.empty()){
				operands.push(t);
			}
			t.clear();
		}else{
			t += c;
		}
	}
}

void write(){
	cout << operands.top() << "\n";
	operands = stack<string>();
}

int main(){
	// ios_base::sync_with_stdio(false);
	for(int acsl = 0; acsl < 10; ++acsl){
		read();
		solve();
		write();
	}
	return 0;
}