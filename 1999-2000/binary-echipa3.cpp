#include <iostream>
#include <fstream>
#include <string>

using namespace std;

struct nod{
    char val;
    nod *left = nullptr, *right = nullptr;

    nod(){
        this->val = 0;
        this->left = nullptr;
        this->right = nullptr;
    }

    nod(char valo){
        this->val = valo;
        this->left = nullptr;
        this->right = nullptr;
    }

    void insertNode(char valo){
        if(valo <= this->val){
            if(this->left == nullptr)
                this->left = new nod(valo);
            else this->left->insertNode(valo);
        }else if(this->right == nullptr)
            this->right = new nod(valo);
        else this->right->insertNode(valo);
    }

    void deleteComp(){
        if(this->left != nullptr){
            this->left->deleteComp();
            delete this->left;
        }
        if(this->right != nullptr){
            this->right->deleteComp();
            delete this->right;
        }
    }
};

int calc(int fr[], nod* node, int depth = 0){
    int sum = fr[node->val - 'a'] * depth;
//    cout<<node->val<<' ';
    if(node->left != nullptr){
        sum += calc(fr, node->left, depth + 1);
    }
    if(node->right != nullptr){
        sum += calc(fr, node->right, depth + 1);
    }
    return sum;
}

int main()
{
    ifstream f("date.in");
    for(int acsl = 0; acsl < 10; ++acsl){
        string initial;
        string transformat;
        int fr[26] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

        getline(f, initial);
        for(int i = 0; i < initial.length(); ++i){
            if(islower(initial[i])) {
                    transformat.push_back(initial[i]);
                    fr[initial[i] - 'a']++;
            }
            if(isupper(initial[i])){
                    transformat.push_back(tolower(initial[i]));
                    fr[initial[i] - 'A']++;
            }

        }
        nod *root = new nod(transformat[0]);
        for(int i = 1; i < transformat.length(); ++i){
            int pos = transformat.substr(0, i).find(transformat[i]);
            if(pos != transformat.npos){
                root->deleteComp();
                delete root;
                transformat.erase(pos, 1);
                i--;

                transformat.insert(0, transformat.substr(i, 1));
                i++;
                transformat.erase(i, 1);
                i = 0;
                root = new nod(transformat[0]);
            }else root->insertNode(transformat[i]);
        }
        cout<<calc(fr, root)<<'\n';
    }
    return 0;
}
