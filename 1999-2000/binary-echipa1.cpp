#include <iostream>
#include <cstdio>
#include <cstring>
using namespace std;
struct nod{
    char inf;
    nod *st, *dr;
}*rad;
char a[300], s[300], b[300];
int m=0, p=0;
void adaugare(nod *&r, char c)
{
    if(!r)
    {
        r=new nod;
        r->inf=c;
        r->st=0;
        r->dr=0;
        return;
    }
    if(r->inf<c)
    {
        adaugare(r->dr, c);
        return;
    }
    adaugare(r->st, c);
}
void rearanjare(char c, int i)
{
    char aux[300]="";
    int n=0;
    aux[n++]=c;
    for(int j=0; j<p; j++)
        if(b[j]!=c)
            aux[n++]=b[j];
    strcpy(b, aux);
}
void zerozare()
{
    rad=0;
    for(int i=0; i<m; i++)
        s[i]=0;
    m=0;
    p=0;
    s[m]=0;
    b[p]=0;
}
void create_tree()
{
    zerozare();
    int fr[26]{0};
    gets(a);
    scanf("\n");
    int l=strlen(a);
    for(int i=0; i<l; i++)
        if(a[i]>='a' && a[i]<='z')
            a[i]=a[i]-32;
    for(int i=0; i<l; i++)
        if(a[i]>='A' && a[i]<='Z')
            s[m++]=a[i];
    strcpy(a, s);
    l=strlen(a);
    for(int i=0; i<l; i++)
        if(a[i]>='A' && a[i]<='Z')
        {
            fr[a[i]-'A']++;
            if(fr[a[i]-'A']>1)
            {
                rearanjare(a[i], i);
                fr[a[i]-'A']--;
            }
            else
                b[p++]=a[i];
        }
    for(int i=0; i<p; i++)
        if(b[i]>='A' && b[i]<='Z')
            adaugare(rad, b[i]);
}
int processing_time(nod *r, int niv, char c)
{
    if(r->inf < c)
        return processing_time(r->dr, niv+1, c);
    else if(r->inf > c)
        return processing_time(r->st, niv+1, c);
    else
        return niv;
}
void afisare(nod *r)
{
    if(!r)
        return;
    afisare(r->st);
    cout<<r->inf<<' ';
    afisare(r->dr);
}
int main()
{
    freopen("binary_tree.in", "r", stdin);
    for(int acsl=0; acsl<10; acsl++)
    {
        create_tree();
        //afisare(rad);
        int time=0;
        int l=strlen(s);
        for(int i=0; i<l; i++)
            if(s[i]>='A' && s[i]<='Z')
                time+=processing_time(rad, 0, s[i]);
        cout<<time<<'\n';
    }
    return 0;
}