//ceva nu e bine
//la ei la teste prolly

#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>

using namespace std;

typedef unsigned long long lint;

ifstream fin("saptele.in");

lint chartolint(char c){
	if(c >= '0' && c <= '9'){
		return c-'0';
	}else{
		return c-'A'+10;
	}
}

lint stringtolint(string s){
	lint r = 0;
	for(auto c : s){
		r <<= 4;
		r |= chartolint(c);
	}
	return r;
}

char linttochar(lint a){
	if(a >= 0 && a <= 9){
		return a+'0';
	}else{
		return a-10+'A';
	}
}

string linttostring(lint a){
	string r = "";
	for(int i = 0; i < 10; ++i){
		r += linttochar(a&lint(0b1111));
		a >>= 4;
	}
	reverse(r.begin(), r.end());
	return r;
}

lint readlint(){
	string s;fin >> s;
	return stringtolint(s);
}

lint x, y, z;
void read(){
	x = readlint();
	y = readlint();
	z = readlint();
}

void writestring(lint a){
	cout << linttostring(a) << "\n";
}

void write(){
	writestring(x | y);
	writestring(y & z);
	writestring(x & (y | z));
	writestring(~(~(x & y) | z));
	writestring(~((~(x | y)) ^ (~(y | z))));
}

int main(){
	// ios_base::sync_with_stdio(false);
	read();
	write();
	return 0;
}