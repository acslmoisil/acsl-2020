#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>

using namespace std;

ifstream fin("postficc.in");

string operation;

struct nod{
    bool op;
    int val;
    nod *left=nullptr, *right=nullptr, *parent=nullptr;

    nod()
    {
       this->op = false;
       this->left = nullptr;
       this->right = nullptr;
    }

    nod (int val, nod* parr)
    {
        this->op = false;
        this->val = val;
        this->parent = parr;
    }

    nod(string oper, nod* parr){
        this->op = true;
        if(oper=="nim")
            val=1;
        else if(oper== "xam")
            val=2;
        else
            val=3;
        this->parent = parr;
        }

};

void read()
{
    getline(fin, operation);
}

void display(nod* root){
    if(root == nullptr)
        return;
    if(!root->op){
       cout<<root->val;
       return;
    }
    if(root->val == 3){
        cout<<"abs(";
        display(root->right);
        cout<<")";
    }else{
        if(root->val == 1)
            cout<<"min";
        else cout<<"max";
        cout<<"(";
        display(root->left);
        cout<<", ";
        display(root->right);
        cout<<")";
    }
}

int main()
{
    for(int acsl=1; acsl<=10; acsl++)
    {
        read();
        reverse(operation.begin(), operation.end());
        int poz=operation.find(' ');
        nod *curnod = nullptr;
        nod *root = nullptr;
        bool ok = true;
        while(ok)
        {
        if(poz == operation.npos)
            ok = false;
        string current=operation.substr(0, poz);
//        cout<<current<<'/';
        if(islower(current[0])){
            TOP:
            if(curnod == nullptr){
                root = curnod = new nod(current, nullptr);
            }else if(curnod->op && curnod->right == nullptr){
                curnod->right = new nod(current, curnod);
                curnod  = curnod->right;
            }else if(curnod->op && curnod->val != 3 && curnod->left == nullptr){
                curnod->left = new nod(current, curnod);
                curnod = curnod->left;
            }else {
                curnod = curnod->parent;
                goto TOP;
            }
        }else{
            int number = 0, p = 1;
            for(int i =0; i <current.length(); ++i){
                if(current[i] == '-'){
                    number*=-1;
                    break;
                }

                number += p * (int)(current[i] - '0');
                p*=10;
            }
            TOP1:
            if(curnod == nullptr){
                curnod = new nod(number, nullptr);
            }else if(curnod->op && curnod->right == nullptr){
                curnod->right = new nod(number, curnod);
                curnod  = curnod->right;
            }else if(curnod->op && curnod->val != 3 && curnod->left == nullptr){
                curnod->left = new nod(number, curnod);
                curnod = curnod->left;
            }else {
                curnod = curnod->parent;
                goto TOP1;
            }
        }
        operation = operation.substr(poz + 1);
//        cout<<operation<<'/';
        poz=operation.find(' ');
    }
        display(root);
        cout<<"\n";
    }
    return 0;
}
