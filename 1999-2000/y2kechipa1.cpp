#include <iostream>
#include <cstdio>

using namespace std;

int day, h, m, sum;
char month[20], z[3];

void minute()
{
    day-=1;
    if(month[0]=='J' && month[1]=='a')
        sum+=(day*24*60);
    if(month[0]=='F')
        sum+=(31*24*60+day*24*60);
    if(month[0]=='M' && month[2]=='r')
        sum+=((31+29)*24*60+day*24*60);
    if(month[0]=='A' && month[1]=='p')
    {
        sum+=((31+29+31)*24*60+day*24*60);
        if(day>1)
            sum-=60;
        if(day==1 && h>2 && z[0]=='a')
            sum-=60;
    }
    if(month[0]=='M' && month[2]=='y')
        sum+=((31+29+31+30)*24*60+day*24*60), sum-=60;
    if(month[0]=='J' && month[1]=='u' && month[2]=='n')
        sum+=((31+29+31+30+31)*24*60+day*24*60), sum-=60;
    if(month[0]=='J' && month[2]=='l')
        sum+=((31+29+31+30+31+30)*24*60+day*24*60), sum-=60;
    if(month[0]=='A' && month[1]=='u')
        sum+=((31+29+31+30+31+30+31)*24*60+day*24*60), sum-=60;
    if(month[0]=='S')
        sum+=((31+29+31+30+31+30+31+31)*24*60+day*24*60), sum-=60;
    if(month[0]=='O')
    {
        sum+=((31+29+31+30+31+30+31+31+30)*24*60+day*24*60);
        if(day<28)
            sum-=60;
        if(day==28 && h<2 && z[0]=='a')
            sum-=60;
    }
    if(month[0]=='N')
        sum+=((31+29+31+30+31+30+31+31+30+31)*24*60+day*24*60);
    if(month[0]=='D')
        sum+=((31+29+31+30+31+30+31+31+30+31+30)*24*60+day*24*60);
}

int main()
{
    freopen("date.in", "r", stdin);

    for(int acsl=0; acsl<10; acsl++)
    {
        sum=0;
        scanf("%s %d, %d:%d%s", &month, &day, &h, &m, &z);
        sum+=m;
        if(h==12)
            h=0;
        if(z[0]=='a')
            sum+=(h*60);
        else
            sum+=((h+12)*60);
        minute();
        cout<<sum<<'\n';

    }
    return 0;
}