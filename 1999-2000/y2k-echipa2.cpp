#include <iostream>
#include <fstream>
#include <cstring>

using namespace std;

ifstream f("y2k.in");

char month[15],v,part[5];
int day,hour,minute;
long long total;

int verif_month()
{
    if(strcmp(month,"February")==0)
        return 31;
    if(strcmp(month,"March")==0)
        return 60;
    if(strcmp(month,"April")==0)
        return 91;
    if(strcmp(month,"May")==0)
        return 121;
    if(strcmp(month,"June")==0)
        return 152;
    if(strcmp(month,"July")==0)
        return 182;
    if(strcmp(month,"August")==0)
        return 213;
    if(strcmp(month,"September")==0)
        return 244;
    if(strcmp(month,"October")==0)
        return 274;
    if(strcmp(month,"November")==0)
        return 305;
    if(strcmp(month,"December")==0)
        return 335;
    return 0;
}

void verif_daylight_saving_time()
{
    if(strcmp(month,"April")==0&&day>=2)
        total--;
    if(strcmp(month,"May")==0)
        total--;
    if(strcmp(month,"June")==0)
        total--;
    if(strcmp(month,"July")==0)
        total--;
    if(strcmp(month,"August")==0)
        total--;
    if(strcmp(month,"September")==0)
        total--;
    if(strcmp(month,"October")==0&&day<=29)
        total--;
}

int main() {
    for (int acsl = 0; acsl < 10; ++acsl) {
        total=0;
        f>>month>>day>>v>>hour>>v>>minute>>part;
        total=verif_month();
        total=total+day-1;
        total=total*24;
        verif_daylight_saving_time();
        if(strcmp(part,"am")==0&&hour==12)
            total=total+0;
        else if(strcmp(part,"pm")==0&&hour==12)
            total=total+12;
        else if(strcmp(part,"am")==0)
            total=total+hour;
        else
            total=total+hour+12;
        total=total*60;
        total=total+minute;
        cout<<total<<'\n';
    }
    return 0;
}
