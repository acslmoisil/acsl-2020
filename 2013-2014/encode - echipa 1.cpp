#include <iostream>
#include <map>
#include <string>
#include <fstream>
#include <vector>

using namespace std;

string key1 = "ABCDEFGHIJKLMN";
string key2 = "AABB";
string toEncode;
string result;

map<char, int> values;

unsigned int posKey1;
unsigned int posKey2;

void advanceKey(){
    posKey1++;
    posKey2++;
    if(posKey1 > 14) posKey1 -= 14;
    if(posKey2 > 4) posKey2 -= 4;
}

char encodeLetter(char input = 0){
    if(islower(input)) input = toupper(input);
    int code = values[input];
    code = (code ^ values[key1[posKey1 - 1]] ^ values[key2[posKey2 - 1]]);
    advanceKey();
    for (auto it : values) {
        if(code == it.second)
            return it.first;
    }
}

int main() {
    values['A'] = 0b11000;
    values['B'] = 0b10011;
    values['C'] = 0b01110;
    values['D'] = 0b10010;
    values['E'] = 0b10000;
    values['F'] = 0b10110;
    values['G'] = 0b01011;
    values['H'] = 0b00101;
    values['I'] = 0b01100;
    values['J'] = 0b11010;
    values['K'] = 0b11110;
    values['L'] = 0b01001;
    values['M'] = 0b00111;
    values['N'] = 0b00110;
    values['O'] = 0b00011;
    values['P'] = 0b01101;
    values['Q'] = 0b11101;
    values['R'] = 0b01010;
    values['S'] = 0b10100;
    values['T'] = 0b00001;
    values['U'] = 0b11100;
    values['V'] = 0b01111;
    values['W'] = 0b11001;
    values['X'] = 0b10111;
    values['Y'] = 0b10101;
    values['Z'] = 0b10001;
    values['+'] = 0b11011;
    values['/'] = 0b00000;
    values['9'] = 0b00100;
    values['8'] = 0b11111;
    values['4'] = 0b01000;
    values['3'] = 0b00010;
    ifstream f("date.in");
    for (int acsl = 0; acsl < 10; ++acsl) {
        toEncode = "";
        result = "";
        char comma;
        f>>posKey1>>comma>>posKey2>>comma>>toEncode;
        for (auto it : toEncode) {
            result.push_back(encodeLetter(it));
        }
        cout<<result<<'\n';
    }
    return 0;
}
