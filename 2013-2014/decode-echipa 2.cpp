#include <iostream>
#include <cstdio>
#include <cstring>
using namespace std;
char z[50], dz[50];
char k1[55]="ABCDEFGHIJKLMN";
char k2[50]="AABB";
char dk2[50];
int start;
int s1[50], s2[50], s3[50];
int l, poz;
void transformare(int s[50], char c)
{
    if(c=='A')
    {
        s[0]=1;
        s[1]=1;
        s[2]=0;
        s[3]=0;
        s[4]=0;
    }
    else if(c=='B')
    {
        s[0]=1;
        s[1]=0;
        s[2]=0;
        s[3]=1;
        s[4]=1;
    }
    else if(c=='C')
    {
        s[0]=0;
        s[1]=1;
        s[2]=1;
        s[3]=1;
        s[4]=0;
    }
    else if(c=='D')
    {
        s[0]=1;
        s[1]=0;
        s[2]=0;
        s[3]=1;
        s[4]=0;
    }
    else if(c=='E')
    {
        s[0]=1;
        s[1]=0;
        s[2]=0;
        s[3]=0;
        s[4]=0;
    }
    else if(c=='F')
    {
        s[0]=1;
        s[1]=0;
        s[2]=1;
        s[3]=1;
        s[4]=0;
    }
    else if(c=='G')
    {
        s[0]=0;
        s[1]=1;
        s[2]=0;
        s[3]=1;
        s[4]=1;
    }
    else if(c=='H')
    {
        s[0]=0;
        s[1]=0;
        s[2]=1;
        s[3]=0;
        s[4]=1;
    }
    else if(c=='I')
    {
        s[0]=0;
        s[1]=1;
        s[2]=1;
        s[3]=0;
        s[4]=0;
    }
    else if(c=='J')
    {
        s[0]=1;
        s[1]=1;
        s[2]=0;
        s[3]=1;
        s[4]=0;
    }
    else if(c=='K')
    {
        s[0]=1;
        s[1]=1;
        s[2]=1;
        s[3]=1;
        s[4]=0;
    }
    else if(c=='L')
    {
        s[0]=0;
        s[1]=1;
        s[2]=0;
        s[3]=0;
        s[4]=1;
    }
    else if(c=='M')
    {
        s[0]=0;
        s[1]=0;
        s[2]=1;
        s[3]=1;
        s[4]=1;
    }
    else if(c=='N')
    {
        s[0]=0;
        s[1]=0;
        s[2]=1;
        s[3]=1;
        s[4]=0;
    }
    else if(c=='O')
    {
        s[0]=0;
        s[1]=0;
        s[2]=0;
        s[3]=1;
        s[4]=1;
    }
    else if(c=='P')
    {
        s[0]=0;
        s[1]=1;
        s[2]=1;
        s[3]=0;
        s[4]=1;
    }
    else if(c=='Q')
    {
        s[0]=1;
        s[1]=1;
        s[2]=1;
        s[3]=0;
        s[4]=1;
    }
    else if(c=='R')
    {
        s[0]=0;
        s[1]=1;
        s[2]=0;
        s[3]=1;
        s[4]=0;
    }
    else if(c=='S')
    {
        s[0]=1;
        s[1]=0;
        s[2]=1;
        s[3]=0;
        s[4]=0;
    }
    else  if(c=='T')
    {
        s[0]=0;
        s[1]=0;
        s[2]=0;
        s[3]=0;
        s[4]=1;
    }
    else  if(c=='U')
    {
        s[0]=1;
        s[1]=1;
        s[2]=1;
        s[3]=0;
        s[4]=0;
    }
    else  if(c=='V')
    {
        s[0]=0;
        s[1]=1;
        s[2]=1;
        s[3]=1;
        s[4]=1;
    }
    else  if(c=='W')
    {
        s[0]=1;
        s[1]=1;
        s[2]=0;
        s[3]=0;
        s[4]=1;
    }
    else  if(c=='X')
    {
        s[0]=1;
        s[1]=0;
        s[2]=1;
        s[3]=1;
        s[4]=1;
    }
    else  if(c=='Y')
    {
        s[0]=1;
        s[1]=0;
        s[2]=1;
        s[3]=0;
        s[4]=1;
    }
    else  if(c=='Z')
    {
        s[0]=1;
        s[1]=0;
        s[2]=0;
        s[3]=0;
        s[4]=1;
    }
    else  if(c=='+')
    {
        s[0]=1;
        s[1]=1;
        s[2]=0;
        s[3]=1;
        s[4]=1;
    }
    else  if(c=='/')
    {
        s[0]=0;
        s[1]=0;
        s[2]=0;
        s[3]=0;
        s[4]=0;
    }
    else  if(c=='9')
    {
        s[0]=0;
        s[1]=0;
        s[2]=1;
        s[3]=0;
        s[4]=0;
    }
    else  if(c=='8')
    {
        s[0]=1;
        s[1]=1;
        s[2]=1;
        s[3]=1;
        s[4]=1;
    }
    else  if(c=='4')
    {
        s[0]=0;
        s[1]=1;
        s[2]=0;
        s[3]=0;
        s[4]=0;
    }
    else  if(c=='3')
    {
        s[0]=0;
        s[1]=0;
        s[2]=0;
        s[3]=1;
        s[4]=0;
    }
}
char transformare_invers(int s[50])
{
    if(s[0]==1 && s[1]==1 && s[2]==0 && s[3]==0 && s[4]==0)
        return 'A';
    else if(s[0]==1 && s[1]==0 && s[2]==0 && s[3]==1 && s[4]==1)
        return 'B';
    else if(s[0]==0 && s[1]==1 && s[2]==1 && s[3]==1 && s[4]==0)
        return 'C';
    else if(s[0]==1 && s[1]==0 && s[2]==0 && s[3]==1 && s[4]==0)
        return 'D';
    else if(s[0]==1 && s[1]==0 && s[2]==0 && s[3]==0 && s[4]==0)
        return 'E';
    else if(s[0]==1 && s[1]==0 && s[2]==1 && s[3]==1 && s[4]==0)
        return 'F';
    else if(s[0]==0 && s[1]==1 && s[2]==0 && s[3]==1 && s[4]==1)
        return 'G';
    else if(s[0]==0 && s[1]==0 && s[2]==1 && s[3]==0 && s[4]==1)
        return 'H';
    else if(s[0]==0 && s[1]==1 && s[2]==1 && s[3]==0 && s[4]==0)
        return 'I';
    else if(s[0]==1 && s[1]==1 && s[2]==0 && s[3]==1 && s[4]==0)
        return 'J';
    else if(s[0]==1 && s[1]==1 && s[2]==1 && s[3]==1 && s[4]==0)
        return 'K';
    else if(s[0]==0 && s[1]==1 && s[2]==0 && s[3]==0 && s[4]==1)
        return 'L';
    else if(s[0]==0 && s[1]==0 && s[2]==1 && s[3]==1 && s[4]==1)
        return 'M';
    else if(s[0]==0 && s[1]==0 && s[2]==1 && s[3]==1 && s[4]==0)
        return 'N';
    else if(s[0]==0 && s[1]==0 && s[2]==0 && s[3]==1 && s[4]==1)
        return 'O';
    else if(s[0]==0 && s[1]==1 && s[2]==1 && s[3]==0 && s[4]==1)
        return 'P';
    else if(s[0]==1 && s[1]==1 && s[2]==1 && s[3]==0 && s[4]==1)
        return 'Q';
    else if(s[0]==0 && s[1]==1 && s[2]==0 && s[3]==1 && s[4]==0)
        return 'R';
    else if(s[0]==1 && s[1]==0 && s[2]==1 && s[3]==0 && s[4]==0)
        return 'S';
    else if(s[0]==0 && s[1]==0 && s[2]==0 && s[3]==0 && s[4]==1)
        return 'T';
    else if(s[0]==1 && s[1]==1 && s[2]==1 && s[3]==0 && s[4]==0)
        return 'U';
    else if(s[0]==0 && s[1]==1 && s[2]==1 && s[3]==1 && s[4]==1)
        return 'V';
    else if(s[0]==1 && s[1]==1 && s[2]==0 && s[3]==0 && s[4]==1)
        return 'W';
    else if(s[0]==1 && s[1]==0 && s[2]==1 && s[3]==1 && s[4]==1)
        return 'X';
    else if(s[0]==1 && s[1]==0 && s[2]==1 && s[3]==0 && s[4]==1)
        return 'Y';
    else if(s[0]==1 && s[1]==0 && s[2]==0 && s[3]==0 && s[4]==1)
        return 'Z';
    else if(s[0]==1 && s[1]==1 && s[2]==0 && s[3]==1 && s[4]==1)
        return '+';
    else if(s[0]==0 && s[1]==0 && s[2]==0 && s[3]==0 && s[4]==0)
        return '/';
    else if(s[0]==0 && s[1]==0 && s[2]==1 && s[3]==0 && s[4]==0)
        return '9';
    else if(s[0]==1 && s[1]==1 && s[2]==1 && s[3]==1 && s[4]==1)
        return '8';
    else if(s[0]==0 && s[1]==1 && s[2]==0 && s[3]==0 && s[4]==0)
        return '4';
    else if(s[0]==0 && s[1]==0 && s[2]==0 && s[3]==1 && s[4]==0)
        return '3';
}
void litera_noua()
{
    for(int i=0; i<5; i++)
        if(s1[i]!=s2[i])
            s3[i]=1;
        else
            s3[i]=0;
}
void read()
{
    scanf("%s %d\n", z, &start);
    l=strlen(z);
    z[l-1]=0;
    l=strlen(z);
    for(int i=0; i<l-1; i++)
    {
        transformare(s1, z[i]);
        transformare(s2, z[i+1]);
        litera_noua();
        dz[i]=transformare_invers(s3);
    }
    dz[l-1]=0;
}
void solve()
{
    int nrslash=0;
    char dk1[50];
    for(int i=14; i<28; i++)
        k1[i]=k1[i-14];
    for(int i=28; i<28+14; i++)
        k1[i]=k1[i-28];
    for(int i=0; i<28; i++)
    {
        transformare(s1, k1[i]);
        transformare(s2, k1[i+1]);
        litera_noua();
        dk1[i]=transformare_invers(s3);
    }
    for(int j=0; j<=14; j++)
    {
        int nr=0;
        char cuv[50];
        for(int i=0; i<l; i++)
        {
            transformare(s1, dz[i]);
            transformare(s2, dk1[i+j]);
            litera_noua();
            cuv[i]=transformare_invers(s3);
            if(cuv[i]=='/')
                nr++;
        }
        cuv[l-1]=0;
        if(nr>=nrslash)
        {
            nrslash=nr;
            poz=j;
        }
    }
}
void creare_dk2()
{
    start--;
    for(int i=0; i<l; i++)
    {
        dk2[i]=k2[start];
        start++;
        if(start==4)
            start=0;
    }
}
int main()
{
    freopen("decode.in", "r", stdin);
    for(int acsl=0; acsl<5; acsl++)
    {
        poz=0;
        read();
        solve();
        printf("%d\n", poz+1);
        char meow[50];
        for(int i=0; i<l; i++)
        {
            transformare(s1, z[i]);
            transformare(s2, k1[i+poz]);
            litera_noua();
            meow[i]=transformare_invers(s3);
        }
        creare_dk2();
        meow[l]=0;
        for(int i=0; i<l; i++)
        {
            transformare(s1, meow[i]);
            transformare(s2, dk2[i]);
            litera_noua();
            printf("%c",transformare_invers(s3));
        }
        printf("\n");
    }
    return 0;
}
