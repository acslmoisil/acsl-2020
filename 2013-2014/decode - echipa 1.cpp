#include <iostream>
#include <map>
#include <string>
#include <fstream>
#include <vector>

using namespace std;

string key1 = "ABCDEFGHIJKLMN";
string key2 = "AABB";
string toEncode;
string result;

map<char, int> values;

unsigned int posKey1;
unsigned int posKey2;

void advanceKey(){
    posKey1++;
    posKey2++;
    if(posKey1 > 14) posKey1 -= 14;
    if(posKey2 > 4) posKey2 -= 4;
}

char encodeLetter(char input = 0){
    if(islower(input)) input = toupper(input);
    int code = values[input];
    code = (code ^ values[key1[posKey1 - 1]] ^ values[key2[posKey2 - 1]]);
    advanceKey();
    for (auto it : values) {
        if(code == it.second)
            return it.first;
    }
}
string deltaz(string param = toEncode)
{
    vector<int> z;
    string dz;
    for(int i=0;i<param.length() - 1;++i){
       z.push_back(values[param[i]]^ values[param[i+1]] );
    }
    for(auto i : z)
        for(auto it : values) // it.second = values[it.first]
            if(i==it.second)
                dz.push_back(it.first);
    return dz;
}
string build(int x)
{
    string res = "";
    for(int i=x, j = 0; j < toEncode.length(); i++, j++){
        if(i>13)
            i=0;
        res.push_back(key1[i]);
    }
    return res;
}
string k[14], deltaK[14];
char xor1(char a, char b)
{
    int ccodede=(values[a] ^ values[b]);
    for(auto it: values)
        if(ccodede==it.second)
            return it.first;
}

string xor2(string s1, string s2){
    string res = "";
    for (int i = 0; i < s1.length(); ++i) {
        res.push_back(xor1(s1[i],  s2[i]));
    }
    return res;
}

int slashes(string x){
    int sl = 0;
    for (auto it : x) {
        if(it == '/') sl++;
    }
    return sl;
}

void make() {
    int pos = -1, maxSL = -1;
    for (int i = 0; i < 14; ++i) {
        k[i] = build(i);
        deltaK[i] = xor2(deltaz(k[i]), deltaz(toEncode));
        int sl = slashes(deltaK[i]);
        if(sl > maxSL)
            pos = i, maxSL = sl;
    }
    posKey1 = pos;
}

string stringKey2(){
    string res = "";
    for(int i = 0, j = posKey2; i < toEncode.length(); ++i, ++j){
        if(j >= 4) j -= 4;
        res.push_back(key2[j]);
    }
    return res;
}

int main() {
    values['A'] = 0b11000;
    values['B'] = 0b10011;
    values['C'] = 0b01110;
    values['D'] = 0b10010;
    values['E'] = 0b10000;
    values['F'] = 0b10110;
    values['G'] = 0b01011;
    values['H'] = 0b00101;
    values['I'] = 0b01100;
    values['J'] = 0b11010;
    values['K'] = 0b11110;
    values['L'] = 0b01001;
    values['M'] = 0b00111;
    values['N'] = 0b00110;
    values['O'] = 0b00011;
    values['P'] = 0b01101;
    values['Q'] = 0b11101;
    values['R'] = 0b01010;
    values['S'] = 0b10100;
    values['T'] = 0b00001;
    values['U'] = 0b11100;
    values['V'] = 0b01111;
    values['W'] = 0b11001;
    values['X'] = 0b10111;
    values['Y'] = 0b10101;
    values['Z'] = 0b10001;
    values['+'] = 0b11011;
    values['/'] = 0b00000;
    values['9'] = 0b00100;
    values['8'] = 0b11111;
    values['4'] = 0b01000;
    values['3'] = 0b00010;
    ifstream f("data.in");
    for (int acsl = 0; acsl < 5; ++acsl) {
        toEncode = "";
        result = "";
        f>>toEncode>>posKey2;
        toEncode.pop_back();
        for (auto &it : toEncode) {
            if(islower(it)) it = toupper(it);
        }
        posKey2--;
        make();
        cout<<posKey1 + 1<<"\n"<<xor2(xor2(toEncode, k[posKey1]), stringKey2())<<"\n";
    }
    return 0;
}
