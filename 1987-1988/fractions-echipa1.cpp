#include <iostream>
#include <fstream>

using namespace std;

ifstream f("fractions.in");

struct fraction
{
    int num,den;
};

char vir;
fraction frac;
int num[20],ind;

void output_1_5()
{
    int a[100]={0},ind=0;
    if(frac.num==1)
    {
        cout<<"0, "<<frac.den<<'\n';
        return;
    }
    while(frac.num!=1)
    {
        a[ind++]=frac.num/frac.den;
        frac.num=frac.num%frac.den;
        swap(frac.num,frac.den);
    }
    for (int i = 0; i < ind; ++i) {
        if(i!=0)
            cout<<", ";
        cout<<a[i];
    }
    cout<<'\n';
}

void read_line()
{
    int x;
    char vir;
    ind=0;
    f>>x;
    while(x!=-1)
    {
        num[ind++]=x;
        f>>vir>>x;
    }
    ind--;
}

void output_5_10()
{
    frac.num=1;
    frac.den=num[ind--];
    if(ind!=-1)
        frac.num=frac.num+frac.den*num[ind--];
    while(ind!=-1)
    {
        swap(frac.num,frac.den);
        frac.num=frac.num+frac.den*num[ind--];
    }
    cout<<frac.num<<", "<<frac.den<<'\n';
}

int main() {
    for (int acsl = 0; acsl < 10; ++acsl) {
        if(acsl<5)
        {
           f>>frac.num>>vir>>frac.den;
           output_1_5();
        } else
        {
            read_line();
            output_5_10();
        }
    }
    return 0;
}
