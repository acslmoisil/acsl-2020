#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <set>

using namespace std;

ifstream fin("vowel.in");

string vowels = "AEIOU";

string phrase;
map<char, int> frec;
set<char> guessed;
void read(){
	getline(fin, phrase);
}

void makefrec(){
	for(auto c : phrase){
		if(c != ' '){
			if(frec.count(c) == 0){
				frec[c] = 0;
			}
			frec[c]++;
		}
	}
}

int curplayer;
int players[2];
void nuke(){
	players[0] = players[1] = 0;
	curplayer = 0;
	frec.clear();
	guessed.clear();
	phrase.clear();
}

void solve(){
	makefrec();
	
	char letter, c;
	int points;
	
	for(fin >> letter >> c >> points; letter != '*'; fin >> c >> letter >> c >> points){
		int &playa = players[curplayer];
		if(frec.count(letter) > 0){
			if(vowels.find(letter) != string::npos){
				playa -= 200;
			}else{
				playa += points*frec[letter];
			}
		}else{
			curplayer = 1-curplayer;
		}
		guessed.insert(letter);
	}
}

void write(){
	bool word = true;
	int correct = 0, words = 0;
	string endphrase = "";
	for(auto c : phrase){
		if(guessed.count(c) > 0){
			endphrase.push_back(c);
			correct++;
		}else if(c == ' '){
			words += word;
			endphrase.push_back(c);
			word = true;
		}else{
			endphrase.push_back('?');
			word = false;
		}
	}
	words += word;
	
	cout << char(curplayer+'A') << "\n";
	cout << players[curplayer] << "\n";
	cout << correct << "\n";
	cout << words << "\n";
	cout << endphrase << "\n";
}

int main(){
	// ios_base::sync_with_stdio(false);
	for(int acsl = 0; acsl < 2; ++acsl){
		nuke();
		read();
		solve();
		write();
		fin.get();
	}
	return 0;
}