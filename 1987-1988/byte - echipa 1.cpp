#include <iostream>
#include <cstdio>

using namespace std;

struct time{
    int hours, minutes;
    bool day;
    time(){
        this->day = false;
        this->hours = 12;
        this->minutes = 00;
    }

    time(bool d, int h, int m){
        this->day = d;
        this->hours = h;
        this->minutes = m;
    }

    void pass5minutes(){
        this->minutes += 5;
        if(this->minutes >= 60)
            this->minutes -= 60, this->hours++;
        if(this->hours >= 24)
            this->hours -= 24, this->day = true;
    }

    bool operator< (const time &other) const {
        if(this->day && !other.day) return false;
        else if(!this->day && other.day) return true;
        if(this->hours != other.hours){
            return this->hours < other.hours;
        }
        return this->minutes < other.minutes;
    }

    bool operator== (const time &other) const {
        if(this->day != other.day) return false;
        if(this->hours != other.hours) return false;
        return this->minutes == other.minutes;
    }

};

int directions[4];
pair<int, int> coord;
bool grid[13][13];

void reset(){
    for (int i = 0; i < 13; ++i) {
        for (int j = 0; j < 13; ++j) {
            grid[i][j] = false;
        }
    }
}

bool atSide(int d){
    const int dx[] = {-1, 0, 1, 0};
    const int dy[] = {0, 1, 0, -1};
    pair<int, int> newCoord = coord;
    newCoord.first += dx[d];
    newCoord.second += dy[d];
    if(newCoord.first == 0 || newCoord.first == 13)
        return true;
    return (newCoord.second == 0 || newCoord.second == 13);
}

int howManySlots(){
    int counter = 0;
    for (int i = 1; i < 13; ++i) {
        for (int j = 1; j < 13; ++j) {
            if(grid[i][j]) counter++;
        }
    }
    return counter;
}

void simulation(){
    reset();
    int dir[4] = {directions[0], directions[1], directions[2], directions[3]};
    const int dx[] = {-1, 0, 1, 0};
    const int dy[] = {0, 1, 0, -1};
    time dest(true, 12, 0);
    time init;
    int atSurface = 0;
    pair<int, int> when1pm, when6pm, whenFirstSurface;
    for(int d = 0; init < dest; init.pass5minutes()){
        if(time(false, 13, 0) == init)
            when1pm = coord;
        if(time(false, 18, 0) == init)
            when6pm = coord;
        if(atSide(d)){
            if(atSurface == 0)
                whenFirstSurface = coord;
            atSurface += 5;
        }
        else{
            coord.first += dx[d];
            coord.second += dy[d];
            grid[coord.first][coord.second] = true;
        }
        dir[d]-=5;
        if(dir[d] == 0) dir[d] = directions[d], d++;
        if(d >= 4) d -= 4;
    }
    cout<<'('<<when1pm.first<<", "<<when1pm.second<<')'<<"\n";
    cout<<'('<<when6pm.first<<", "<<when6pm.second<<')'<<"\n";
    cout<<'('<<whenFirstSurface.first<<", "<<whenFirstSurface.second<<')'<<"\n";
    cout<<atSurface<<'\n';
    cout<<howManySlots()<<'\n';
}

int main() {
    freopen("date.in", "r", stdin);
    for (int acsl = 0; acsl < 2; ++acsl) {
        scanf("%d, %d, %d, %d, %d, %d\n", &coord.first, &coord.second, &directions[0], &directions[1], &directions[2], &directions[3]);
        simulation();
    }
    return 0;
}
