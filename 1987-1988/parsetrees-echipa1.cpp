#include <iostream>
#include <fstream>
#include <string>
#include <stack>
#include <map>
#include <queue>

using namespace std;

ifstream fin("parsetrees.in");

struct Node{
	Node *lt, *rt;
	char v;
};

map<char, int> ope = {{'+', 1}, {'-', 1}, {'*', 2}};
string ex;
void read(){
	getline(fin, ex);
}

stack<Node*> operands;
stack<char> operators;
Node *root;

int ans1;

void nuke(){
	ans1 = 0;
}

void solve(){
	nuke();
	
	for(auto c : ex){
		if(c==' ')continue;
		if(c != '(' && c != ')')ans1++;
		
		if(c == '('){
			operators.push(c);
		}else if(c == ')'){
			while(operators.top() != '('){
				Node *node = new Node();
				node->rt = operands.top();operands.pop();
				node->lt = operands.top();operands.pop();
				node->v = operators.top();operators.pop();
				operands.push(node);
			}
			operators.pop();
		}else if(ope.count(c) > 0){
			while(!operators.empty() && ope[operators.top()] >= ope[c]){
				Node *node = new Node();
				node->rt = operands.top();operands.pop();
				node->lt = operands.top();operands.pop();
				node->v = operators.top();operators.pop();
				operands.push(node);
			}
			operators.push(c);
		}else{
			Node *node = new Node();
			node->v = c;
			operands.push(node);
		}
	}
	while(!operators.empty()){
		Node *node = new Node();
		node->rt = operands.top();operands.pop();
		node->lt = operands.top();operands.pop();
		node->v = operators.top();operators.pop();
		operands.push(node);
	}
	root = operands.top();
	operands.pop();
}

int height(Node *node = root){
	if(ope.count(node->v) > 0){
		return max(height(node->lt), height(node->rt))+1;
	}else{
		return 0;
	}
}

void preorder(Node *node = root){
	cout << node->v << " ";
	if(ope.count(node->v) > 0){
		preorder(node->lt);
		preorder(node->rt);
	}
}

void postorder(Node *node = root){
	if(ope.count(node->v) > 0){
		postorder(node->lt);
		postorder(node->rt);
	}
	cout << node->v << " ";
}

void bfs(){
	queue<Node*> qu;
	qu.push(root);
	while(!qu.empty()){
		Node *node = qu.front();qu.pop();
		cout << node->v << " ";
		if(ope.count(node->v) > 0){
			qu.push(node->lt);
			qu.push(node->rt);
		}
	}
}

void write(){
	cout << ans1 << "\n";
	cout << height() << "\n";
	preorder();cout << "\n";
	postorder();cout << "\n";
	bfs();cout << "\n";
}

int main(){
	// ios_base::sync_with_stdio(false);
	for(int acsl = 0; acsl < 2; ++acsl){
		read();
		solve();
		write();
	}
	return 0;
}