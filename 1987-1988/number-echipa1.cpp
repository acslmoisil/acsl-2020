#include <iostream>
#include <fstream>

using namespace std;

ifstream f("number.in");

long long st,d,a[5010][5010];
char vir;

void make_grid(int limit)
{
    a[1][1]=st;
    for (int i = 2; i <= limit; ++i) {
        a[i][1]=a[i-1][1]+d;
    }
    for (int i = 1; i <= limit; ++i) {
        for (int j = 2; j <= limit; ++j)
            a[i][j]=a[i][j-1]+d;
        d+=2;
    }
}

void afis()
{
    for (int i = 1; i <= 4; ++i) {
        for (int j = 1; j <= 6; ++j) {
            cout<<a[i][j]<<' ';
        }
        cout<<'\n';
    }
}

void output_3(int limit)
{
    long long maxi=LLONG_MIN;
    for (int i = 1; i <= limit; ++i) {
        if(a[i][1]>100)
        {
            cout<<maxi<<'\n';
            return;
        }
        for (int j = 1; j <= limit&&a[i][j]<100; ++j)
            maxi=max(maxi,a[i][j]);
    }
}

void output_4_afis(int fr[105])
{
    int maxi=-1;
    bool ok=0;
    for (int i = 0; i < 100; ++i)
        maxi=max(maxi,fr[i]);
    for (int i = 0; i < 100; ++i) {
        if(fr[i]==maxi)
        {
            if(!ok)
                ok=true;
            else
                cout<<", ";
            cout<<i;
        }
    }
    cout<<'\n';
}

void output_5_afis(int fr[105])
{
    int s=0;
    for (int i = 0; i < 100; ++i)
        if(fr[i]==0)
            s+=i;
    cout<<s<<'\n';
}

void output_4_and_5(int limit)
{
    int fr[105]={0};
    for (int i = 1; i <= limit; ++i) {
        if(a[i][1]>100)
        {
            output_4_afis(fr);
            output_5_afis(fr);
            return;
        }
        for (int j = 1; j <= limit&&a[i][j]<100; ++j)
            fr[a[i][j]]++;
    }
}

int main() {
    for (int acsl = 0; acsl < 2; ++acsl) {
        f>>st>>vir>>d;
        if(acsl==0)
            make_grid(55);
        else
            make_grid(5005);
        cout<<a[1][9]<<'\n'<<a[14][16]<<'\n';
        if(acsl==0)
            output_3(50);
        else
            output_3(5000);
        if(acsl==0)
            output_4_and_5(50);
        else
            output_4_and_5(5000);
    }
    return 0;
}
