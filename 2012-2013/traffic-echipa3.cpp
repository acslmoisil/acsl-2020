#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <queue>
#include <iomanip>

using namespace std;

const int INF = 0x3f3f3f3f;

struct Edge{
	int a, v;
};

struct Nod{
	int a, v;
	bool operator<(const Nod &rhs)const{
		if(v != rhs.v){
			return v > rhs.v;
		}else{
			return a > rhs.a;
		}
	}
};

ifstream fin("traffic.in");

vector<Edge> gra[41];

int cv(int x, int y){
	return x-1 + (y-1)*6;
}

int sign(int a){
	if(a > 0){
		return 1;
	}else if(a == 0){
		return 0;
	}else{
		return -1;
	}
}

void diagonal(int x1, int y1, int x2, int y2){
	int xinc = sign(x2-x1);
	int yinc = sign(y2-y1);
	int t = abs(x1-x2);
	for(int i = 0; i < t; ++i){
		gra[cv(x1+i*xinc, y1+i*yinc)].push_back({cv(x1+(i+1)*xinc, y1+(i+1)*yinc), 14});
		gra[cv(x1+(i+1)*xinc, y1+(i+1)*yinc)].push_back({cv(x1+i*xinc, y1+i*yinc), 14});
	}
}

void makegraph(){
	for(int y = 1; y <= 6; ++y){
		for(int x = 1; x <= 5; ++x){
			gra[cv(x + (y+1)%2, y)].push_back({cv(x + y%2, y), 10});
		}
	}
	for(int y = 1; y <= 5; ++y){
		for(int x = 1; x <= 6; ++x){
			gra[cv(x, y + (x+1)%2)].push_back({cv(x, y + x%2), 10});
		}
	}
}

int st, en;

void read(){
	char c;
	int x1, y1, x2, y2;
	fin >> x1 >> c >> y1 >> c >> x2 >> c >> y2;
	st = cv(x1, y1);
	en = cv(x2, y2);
}

int dist[41];
priority_queue<Nod> qu;

void solve(){
	for(int i = 0; i < 41; ++i){
		dist[i] = INF;
	}
	
	dist[st] = 0;
	qu.push({st, 0});
	
	while(!qu.empty()){
		Nod x = qu.top();qu.pop();
		if(x.v <= dist[x.a]){
			int a = x.a;
			for(auto w : gra[x.a]){
				int b = w.a;
				int v = dist[a]+w.v;
				if(v < dist[b]){
					dist[b] = v;
					qu.push({b, dist[b]});
				}
			}
		}
	}
}

void write(){
	cout << dist[en]/10;
	if(dist[en]%10 != 0){
		cout << "." << dist[en]%10;
	}
	cout << "\n";
}

int main(){
	// ios_base::sync_with_stdio(false);
	makegraph();
	for(int acsl = 0; acsl < 10; ++acsl){
		if(acsl == 5){
			diagonal(1, 6, 5, 2);
			diagonal(1, 2, 3, 4);
			diagonal(2, 2, 5, 5);
		}
		read();
		solve();
		write();
	}
	return 0;
}