#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

ifstream f("date.in");

double a[10][10];
bool vizitate[5];

double intra(int curent, int finish){
    if(curent == finish)
        return 1;
    for (int i = 0; i < 5; ++i) {
        if(a[curent][i] != 0 && !vizitate[i] && i != curent){
            vizitate[i] = 1;
            double res = intra(i, finish);
            vizitate[i] = 0;
            if(res != 0)
                return a[curent][i] * res;
        }
    }
    return 0;
}

double ajunge(int start, int finish){
    for (int i = 0; i < 5; ++i) {
        vizitate[i] = 0;
    }
    vizitate[start] = 1;
    return intra(start, finish);
}


void genereaza(){
    bool ok = false;
    do{
        for (int i = 0; i < 5; ++i) {
            for (int j = 0; j < 5; ++j) {
                if(a[i][j] == 0){
                    a[i][j] = ajunge(i, j);
                    if(a[i][j] == 0)
                        ok = true;
                }
            }
        }
    }while(ok);
}

int main() {
    for (int i = 0; i < 5; ++i) {
        a[i][i] = 1;
    }
    for (int acsl = 0; acsl < 4; ++acsl) {
        char pornire, final, comma;
        double valoare;
        f>>pornire>>final>>comma>>valoare;
        int coord1 = pornire - 'A', coord2 = final - 'A';
        a[coord1][coord2] = valoare;
        a[coord2][coord1] = 1/valoare;
    }

    genereaza();
//    for (int i = 0; i < 5; ++i) {
//        for (int j = 0; j < 5; ++j) {
//            cout<<a[i][j]<<' ';
//        }
//        cout<<endl;
//    }
    for (int acsl = 0; acsl < 10; ++acsl) {
        char pornire, final;
        f>>pornire>>final;
        int coord1 = pornire - 'A', coord2 = final - 'A';
        cout<<setprecision(2)<<fixed<<a[coord1][coord2]<<"\n";
    }
    return 0;
}
