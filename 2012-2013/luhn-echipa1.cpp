#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

using namespace std;

ifstream f("date.in");
vector<int> cifre;
long long nr;
int cifraControl;

void transform(long long x){
    while(x){
        int c = x % 10;
        x/=10;
        cifre.push_back(c);
    }
    reverse(cifre.begin(), cifre.end());
}

int control(){
    int cifraControl = 0;
    vector<int> cifre2;
    for (int i = 0; i < cifre.size(); ++i) {
        cifre2.push_back(cifre[i]);
    }
    for (int i = cifre2.size() - 2; i >= 0; i -= 2) {
        cifre2[i] *= 2;
        if(cifre2[i] > 9){
            int sum = 0;
            while(cifre2[i]){
                sum += cifre2[i] % 10;
                cifre2[i] /= 10;
            }
            cifre2[i] = sum;
        }
    }
    for (int i = 0; i < cifre2.size() - 1; ++i) {
        cifraControl += cifre2[i];
    }
    cifraControl *= 9;
    return cifraControl % 10;
}

void singleDigitError(){
    for (int i = 1; i <= 9; ++i) {
        for (int j = 0; j < cifre.size() - 1; ++j) {
            int aux = cifre[j];
            cifre[j] += i + 10;
            cifre[j] = cifre[j] % 10;
            if(control() == cifre[cifre.size() - 1])
                return;
            cifre[j] = aux;
            cifre[j] -= i;
            cifre[j] += 10;
            cifre[j] %= 10;
            if(control() == cifre[cifre.size() - 1])
                return;
            cifre[j] = aux;
        }
    }
}

void transpositionError(){
    for (int i = 0; i < cifre.size() - 2; ++i) {
        swap(cifre[i], cifre[i + 1]);
        if(control() == cifre[cifre.size() - 1])
            return;
        swap(cifre[i], cifre[i + 1]);
    }
}

void twinDigitError(){
    for (int i = 1; i <= 9; ++i) {
        for (int j = 0; j < cifre.size() - 2; ++j) {
            if(cifre[j] == cifre[j + 1]){
                int aux = cifre[j];
                cifre[j] += i + 10;
                cifre[j] = cifre[j] % 10;
                cifre[j + 1] = cifre[j];
                if(control() == cifre[cifre.size() - 1])
                    return;
                cifre[j] = aux;
                cifre[j + 1] = aux;
                cifre[j] -= i;
                cifre[j] += 10;
                cifre[j] %= 10;
                cifre[j + 1] = cifre[j];
                if(control() == cifre[cifre.size() - 1])
                    return;
                cifre[j] = aux;
                cifre[j + 1] = aux;
            }

        }
    }
}

int main() {
    for (int acsl = 0; acsl < 10; ++acsl) {
        cifre.clear();
        f>>nr;
        transform(nr);
        cifraControl = control();
        if(acsl < 4){
            if(cifraControl == cifre[cifre.size() - 1])
                cout<<"VALID\n";
            else cout<<cifraControl<<"\n";
        }
        if(acsl == 4 || acsl == 5){
            singleDigitError();
            for (int i = 0; i < cifre.size(); ++i) {
                cout<<cifre[i];
            }
            cout<<"\n";
        }
        if(acsl == 6 || acsl == 7){
            transpositionError();
            for (int i = 0; i < cifre.size(); ++i) {
                cout<<cifre[i];
            }
            cout<<"\n";
        }
        if(acsl == 8 || acsl == 9){
            twinDigitError();
            for (int i = 0; i < cifre.size(); ++i) {
                cout<<cifre[i];
            }
            cout<<"\n";
        }
    }
    return 0;
}
