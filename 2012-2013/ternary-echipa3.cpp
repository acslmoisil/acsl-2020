#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <bitset>

using namespace std;

ifstream fin("ternary.in");

int county(int a){
	int r = 0;
	while(a > 0){
		r += (a&1);
		a >>= 1;
	}
	return r;
}

struct Bill{
	char type;
	int ins = 0, inv = 0;
	bool calculated = false;
	bool answer;
	vector<Bill*> kids;
	Bill(){}
	Bill(char type) : type(type){}
	void susta(){
		ins = county(inv);
		if(type == 'A'){
			answer = (ins==0);
		}else if(type == 'B'){
			answer = (ins<=1);
		}else if(type == 'C'){
			answer = (ins==3);
		}else if(type == 'D'){
			answer = (ins<=2);
		}
	}
	bool solve(){
		if(!calculated){
			calculated = true;
			for(auto a : kids){
				inv <<= 1;
				inv |= a->solve();
			}
			susta();
		}
		return answer;
	}
};

map<char, Bill> gates[5];

int n;
Bill *root;

void read(){
	for(int i = 0; i < 5; ++i){
		gates[i].clear();
	}
	
	char c;
	fin >> n >> c;
	string s;
	for(int i = 0; i < n; ++i){
		Bill gate;
		fin >> gate.type >> gate.inv;
		gates[0][gate.type] = gate;
		
		fin >> c >> s;
		s.pop_back();
		for(auto a : s){
			if(gates[1].count(a) == 0){
				gates[1][a] = Bill(a);
			}
			Bill &ref = gates[1][a];
			ref.kids.push_back(&gates[0][gate.type]);
		}
	}
	
	fin >> c;
	if(gates[1].size() == 1 && gates[1].count(c)){
		root = &gates[1][c];
	}else{
		gates[2][c] = Bill(c);
		root = &gates[2][c];
		for(auto &a : gates[1]){
			root->kids.push_back(&a.second);
		}
	}
}

int main(){
	// ios_base::sync_with_stdio(false);
	for(int acsl = 0; acsl < 5; ++acsl){
		read();
		root->solve();
		cout << bitset<3>(root->inv) << "\n";
		cout << root->answer << "\n";
	}
	return 0;
}