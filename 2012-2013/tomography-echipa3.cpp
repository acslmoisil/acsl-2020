#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

ifstream fin("tomography.in");

vector<int> row, col;
int n, total;
bool ans[14][14];

void write(){
	for(int y = 0; y < n; ++y){
		for(int x = 0; x < n; ++x){
			cout << ans[x][y];
		}
		cout << "\n";
	}
	cout << "\n";
}

void back(int x=0, int y=0){
	if(row[y] > 0 && col[x] > 0){
		ans[x][y] = true;
		row[y]--;
		col[x]--;
		total--;
		if(x+1 < n){
			back(x+1, y);
		}else if(y+1 < n){
			back(0, y+1);
		}else if(total == 0){
			write();
		}
		ans[x][y] = false;
		row[y]++;
		col[x]++;
		total++;
	}
	
	if(x+1 < n){
		back(x+1, y);
	}else if(y+1 < n){
		back(0, y+1);
	}else if(total == 0){
		write();
	}
}

void read(){
	row.clear();col.clear();
	total = 0;
	char c;
	for(fin >> c; c != ','; fin >> c){
		row.push_back(c-'0');
		total += row.back();
	}
	n = row.size();
	for(int i = 0; i < n; ++i){
		fin >> c;
		col.push_back(c-'0');
	}
}

int main(){
	// ios_base::sync_with_stdio(false);
	for(int acsl = 0; acsl < 10; ++acsl){
		read();
		back();
	}
	return 0;
}