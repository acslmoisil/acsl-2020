#include <iostream>
#include <cstdio>
#include <vector>
#include <queue>
#include <cstring>
using namespace std;
struct nod{
    int y;
    double cost;
    bool operator<(const nod &other) const{
        return cost>other.cost;
    }
};
vector<nod>graph[50];

int numarNod(int i, int j){
    return (i-1)*6+j;
}

void generare()
{
    for(int i=1; i<=6; i++){
        for(int j=1; j<=6; j++){
            int nr = numarNod(i, j);
            if(i%2 == 1){
                if(j<6){
                    int nrvecindreapta = numarNod(i, j+1);
                    graph[nr].push_back({nrvecindreapta, 1});
                }
            }
            else {
                if(j>1){
                    int nrvecinst = numarNod(i, j-1);
                    graph[nr].push_back({nrvecinst, 1});
                }
            }

            if(j%2 == 1){
                if(i<6){
                    int nrvecinsus = numarNod(i+1, j);
                    graph[nr].push_back({nrvecinsus, 1});
                }
            }
            else {
                if(i>1){
                    int nrvecinjos = numarNod(i-1, j);
                    graph[nr].push_back({nrvecinjos, 1});
                }
            }
        }
    }
}

double bellmann(int st, int fin){
    int viz[50]{0};
    double dmin[50];
    for(int i=0; i<=40; i++)
        dmin[i] = 0x3f3f3f3f;
    dmin[st]=0;
    queue<int>Q;
    Q.push(st);
    while(!Q.empty()){
        int el = Q.front();
        Q.pop();
        viz[el]=0;
        for(auto &v:graph[el])
            if(dmin[v.y]>dmin[el]+v.cost){
                dmin[v.y]=dmin[el]+v.cost;
                if(viz[v.y] == 0)
                    Q.push(v.y);
                viz[v.y]=1;
            }
    }
    return dmin[fin];
}

void generare_diagonale()
{
    for(int i=6; i>2; i--){
       int nrvec = numarNod(i-1, 7-i+1);
       int nr = numarNod(i, 7-i);
       graph[nr].push_back({nrvec, 1.4});
       graph[nrvec].push_back({nr, 1.4});
    }
    for(int i=5; i>2; i--){
        int nrvec = numarNod(i-1, i-1);
        int nr = numarNod(i, i);
        graph[nr].push_back({nrvec, 1.4});
        graph[nrvec].push_back({nr, 1.4});
    }
    for(int i=4; i>2; i--){
        int nrvec = numarNod(i-1, i-2);
        int nr = numarNod(i, i-1);
        graph[nr].push_back({nrvec, 1.4});
        graph[nrvec].push_back({nr, 1.4});
    }



}
int main()
{
    freopen("traffic.in", "r", stdin);
    generare();
    for(int acsl=1; acsl<=5; acsl++)
    {
        int pi, pj, fi, fj;
        scanf("%d, %d, %d, %d\n", &pj, &pi, &fj, &fi);
        int p=numarNod(pi, pj);
        int f=numarNod(fi, fj);
        printf("%.0f\n", bellmann(p, f));
    }
    generare_diagonale();
    for(int acsl=1; acsl<=5; acsl++)
    {
        int pi, pj, fi, fj;
        scanf("%d, %d, %d, %d\n", &pj, &pi, &fj, &fi);
        int p=numarNod(pi, pj);
        int f=numarNod(fi, fj);
        printf("%.1f\n", bellmann(p, f));
    }
    return 0;
}
