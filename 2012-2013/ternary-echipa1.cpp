#include <iostream>
#include <cstdio>
#include <cstring>
using namespace std;
int n;
char rez;
struct nod{
    char l;
    int i1, i2, i3;
};
int a[5][4]; // a[0] - 'A' de pe a doua coloana si pe coloanele lui sa avem copii
nod convert(int x){
    if(x==0)
        return {'A', 0, 0, 0};
    if(x==1)
        return {'A', 0, 0, 1};
    if(x==2)
        return {'A', 0, 1, 0};
    if(x==3)
        return {'A', 0, 1, 1};
    if(x==4)
        return {'A', 1, 0, 0};
    if(x==5)
        return {'A', 1, 0, 1};
    if(x==6)
        return {'A', 1, 1, 0};
    if(x==7)
        return {'A', 1, 1, 1};
}
void zerozare()
{
    for(int i=0; i<4; i++)
        for(int j=0; j<4; j++)
            a[i][j]=0;
}
int calcul(int a, int b, int c, char l)
{
    if(l=='A')
    {
        if(a==0 && b==0 && c==0)
            return 1;
        return 0;
    }
    if(l=='B')
    {
        if(a+b+c<=1)
            return 1;
        return 0;
    }
    if(l=='C')
    {
        if(a==1 && b==1 && c==1)
            return 1;
        return 0;
    }
    if(l=='D')
    {
        if(a+b+c<=2)
            return 1;
        return 0;
    }
}
int main()
{
    freopen("ternary.in", "r", stdin);
    for(int acsl=0; acsl<5; acsl++)
    {
        zerozare();
        scanf("%d,", &n);
        char lit, s2[5];
        int m;
        int fr[4]{0};
        int ok=0;
        for(int i=0; i<n; i++)
        {
            scanf(" %c%d, %s", &lit, &m, s2);
            nod x = convert(m);
            {
                int val = calcul(x.i1, x.i2, x.i3, lit);
                int l = strlen(s2);
                for(int j=0; j<l && s2[j]!=','; j++){
                    a[s2[j]-'A'][++a[s2[j]-'A'][0]] = val;
                    if(fr[s2[j]-'A'] == 0)
                        ok++;
                    fr[s2[j]-'A']++;

                }
            }
        }
        int re=0;
        int sir[5]{0};
        int p=0;
        scanf(" %c\n", &rez);
        for(int i=0; i<4; i++)
            if(a[i][0]!=0){
                if(ok != 1)
                {
                    int val=calcul(a[i][1], a[i][2], a[i][3], i+'A');
                    printf("%d", val);
                    sir[p++]=val;
                }

                else {
                    for(int j=1; j<=3; j++)
                        printf("%d",a[i][j]);
                    re = calcul(a[i][1], a[i][2], a[i][3], rez);
                }
            }
        printf("\n");
        if(ok == 1)
            printf("%d\n",re);
        else
            printf("%d\n", calcul(sir[0], sir[1], sir[2], rez));
    }
    return 0;
}
