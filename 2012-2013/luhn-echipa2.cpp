#include <iostream>
#include <fstream>
#include <cstring>

using namespace std;

ifstream f("luhn_algorithm.in");

char number[20];
int check_sum;

int verif()
{
    char aux[20];
    strcpy(aux,number);
    int nrl=strlen(aux);
    for (int i = nrl-1; i >= 0; i=i-2) {
        int c=aux[i]-'0';
        c=2*c;
        if(c>9)
            c=c%10+c/10;
        aux[i]=c+'0';
    }
    int sum=0;
    for (int i = 0; i < nrl; ++i)
        sum=sum+aux[i]-'0';
    sum=(sum*9)%10;
    return sum;
}

void input_lines_1_4()
{
    for (int acsl = 1; acsl <= 4; ++acsl) {
        f>>number;
        check_sum=number[strlen(number)-1]-'0';
        number[strlen(number)-1]='\000';
        int sum=verif();
        if(sum==check_sum)
            cout<<"VALID"<<'\n';
        else
            cout<<sum<<'\n';
    }
}

void input_lines_5_6()
{
    for (int acsl = 5; acsl <= 6; ++acsl) {
        f>>number;
        check_sum=number[strlen(number)-1]-'0';
        number[strlen(number)-1]='\000';
        int nrl=strlen(number);
        for (int i = 0; i < nrl; ++i) {
            if(number[i]=='0')
                number[i]='9';
            else
                number[i]=(char)(number[i]-1);
            if(verif()==check_sum)
            {
                cout<<number<<check_sum<<'\n';
                break;
            }
            if(number[i]=='8')
                number[i]='0';
            else
                number[i]=(char)(number[i]+2);
            if(verif()==check_sum)
            {
                cout<<number<<check_sum<<'\n';
                break;
            }
            if(number[i]=='0')
                number[i]='9';
            else
                number[i]=number[i]-1;
        }
    }
}

void input_lines_7_8()
{
    for (int acsl = 7; acsl <= 8; ++acsl) {
        f>>number;
        check_sum=number[strlen(number)-1]-'0';
        number[strlen(number)-1]='\000';
        int nrl=strlen(number);
        for (int i = 0; i < nrl-1; ++i) {
            char aux=number[i];
            number[i]=number[i+1];
            number[i+1]=aux;
            if(verif()==check_sum)
            {
                cout<<number<<check_sum<<'\n';
                break;
            }
            aux=number[i];
            number[i]=number[i+1];
            number[i+1]=aux;
        }
    }
}

void input_lines_9_10()
{
    for (int acsl = 9; acsl <= 10; ++acsl) {
        f>>number;
        check_sum=number[strlen(number)-1]-'0';
        number[strlen(number)-1]='\000';
        int nrl=strlen(number);
        for (int i = 0; i < nrl-1; ++i) {
            if(number[i]==number[i+1])
            {
                if(number[i]=='0')
                    number[i]=number[i+1]='9';
                else
                    number[i]=number[i+1]=(char)(number[i]-1);
                if(verif()==check_sum)
                {
                    cout<<number<<check_sum<<'\n';
                    break;
                }
                if(number[i]=='8')
                    number[i]=number[i+1]='0';
                else
                    number[i]=number[i+1]=(char)(number[i]+2);
                if(verif()==check_sum)
                {
                    cout<<number<<check_sum<<'\n';
                    break;
                }
                if(number[i]=='0')
                    number[i]=number[i+1]='9';
                else
                    number[i]=number[i+1]=number[i]-1;
            }
        }
    }
}

int main() {
    input_lines_1_4();
    input_lines_5_6();
    input_lines_7_8();
    input_lines_9_10();
    return 0;
}
