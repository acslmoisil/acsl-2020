#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

struct Node{
	string st;
	int fr;
};

Node unite(Node lhs, Node rhs){
	return {lhs.st+rhs.st, lhs.fr+rhs.fr};
}

bool cmp(const Node &lhs, const Node &rhs){
	if(lhs.fr != rhs.fr){
		return lhs.fr < rhs.fr;
	}else{
		return lhs.st < rhs.st;
	}
}

ifstream fin("compressed.in");

int freq[30];
vector<Node> nodes;
char let;
void read(){
	for(int i = 0; i < 30; ++i){
		freq[i] = 0;
	}
	
	string s;
	fin >> s >> let;
	for(auto c : s){
		freq[c-'A']++;
	}
}

void solve(){
	string ans = "";
	nodes.clear();
	for(int i = 0; i < 30; ++i){
		if(freq[i] > 0){
			nodes.push_back({string(1, i+'A'), freq[i]});
		}
	}
	while(nodes.size() > 1){
		sort(nodes.begin(), nodes.end(), cmp);
		if(nodes[0].st.find(let) != string::npos){
			ans += '0';
		}else if(nodes[1].st.find(let) != string::npos){
			ans += '1';
		}
		nodes.push_back(unite(nodes[0], nodes[1]));
		nodes.erase(nodes.begin());
		nodes.erase(nodes.begin());
	}
	reverse(ans.begin(), ans.end());
	cout << ans << "\n";
}

int main(){
	// ios_base::sync_with_stdio(false);
	for(int acsl = 0; acsl < 10; ++acsl){
		read();
		solve();
	}
	return 0;
}