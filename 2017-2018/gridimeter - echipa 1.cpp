#include <iostream>
#include <fstream>
#include <map>
#include <string>
#include <vector>

using namespace std;

ifstream f("date.in");

map<char, string> digits;

int matrix[1000][1000];
bool dot[1000][1000];

int r, c;
bool four;
pair<int, int> start;
bool done;

const int dx[] = {0, 1, 0, -1};
const int dy[] = {-1, 0, 1, 0};

void init(){
    digits['0'] = "0000";
    digits['1'] = "0001";
    digits['2'] = "0010";
    digits['3'] = "0011";
    digits['4'] = "0100";
    digits['5'] = "0101";
    digits['6'] = "0110";
    digits['7'] = "0111";
    digits['8'] = "1000";
    digits['9'] = "1001";
    digits['A'] = "1010";
    digits['B'] = "1011";
    digits['C'] = "1100";
    digits['D'] = "1101";
    digits['E'] = "1110";
    digits['F'] = "1111";
}

bool check(){
    for (int i = 1; i <= r; ++i) {
        for (int j = 1; j <= c; ++j) {
            if(matrix[i][j] > 0)
                return false;
        }
    }
    return  true;
}

void reset(){
    for (int i = 0; i < 999; ++i) {
        for (int j = 0; j < 999; ++j) {
            matrix[i][j] = 0;
            dot[i][j] = false;
        }
    }
    for (int k = 0; k < 999; ++k) {
        matrix[k][0] = matrix[0][k] = 1000000;
    }
    four = false;
    done = false;
    start = make_pair(-1, -1);
}

void readLine(int l){
    string line;
    f>>line;
    long long nr = 0;
    for (int i = 0; i < line.length(); ++i) {
        string bits = digits[line[i]];
        for (int j = 0; j < bits.length(); ++j) {
            nr = (nr << 1);
            if(bits[j] == '1') nr++;
        }
    }
    for (int i = c; i > 0 && nr; --i, nr /= 10) {
        matrix[l][i] = nr % 10;
        if(matrix[l][i] == 4)
            four = true;
    }
}

bool inInterval(int x, int y, int z){
    return (z >= x && z <= y) || (z <= x && z >= y);
}

vector<pair<int, int>> findStart(){
    vector<pair<int, int>> v;
    for (int i = 1; i < r; ++i) {
        for (int j = 1; j < c; ++j) {
            if(matrix[i][j] == 1 && matrix[i][j + 1] == 1 && matrix[i + 1][j] == 1 && matrix[i + 1][j + 1] == 1){
                v.push_back(make_pair(i, j));
                return v;
            }
        }
    }
    for (int i = 1; i <= r; ++i) {
        for (int j = 1; j <= c; ++j) {
            if(matrix[i][j] == 3){
                v.push_back(make_pair(i, j));
                return v;
            }
        }
    }
    for (int i = 1; i < r; ++i) {
        for (int j = 1; j < c; ++j) {
            if(inInterval(1, 2, matrix[i][j]) && matrix[i + 1][j] == 0 && inInterval(1, 2, matrix[i + 1][j + 1]))
                v.push_back(make_pair(i, j));
        }
    }
    for (int i = 1; i < r; ++i) {
        for (int j = 1; j < c; ++j) {
            if(inInterval(1, 2, matrix[i][j + 1]) && matrix[i + 1][j + 1] == 0 && inInterval(1, 2, matrix[i + 1][j]))
                v.push_back(make_pair(i, j));
        }
    }
    for (int i = 1; i < r; ++i) {
        for (int j = 1; j < c; ++j) {
            if(inInterval(1, 2, matrix[i][j + 1]) && matrix[i][j] == 0 && inInterval(1, 2, matrix[i + 1][j]))
                v.push_back(make_pair(i, j));
        }
    }
    for (int i = 1; i < r; ++i) {
        for (int j = 1; j < c; ++j) {
            if(inInterval(1, 2, matrix[i][j]) && matrix[i][j + 1] == 0 && inInterval(1, 2, matrix[i + 1][j + 1]))
                v.push_back(make_pair(i, j));
        }
    }
    for (int i = 1; i <= r; ++i) {
        if(matrix[i][1] == 1){
            v.push_back(make_pair(i, 0));
        }
        if(matrix[i][c] == 1){
            v.push_back(make_pair(i, c));
        }
    }
    for (int i = 1; i <= c; ++i) {
        if(matrix[1][i] == 1){
            v.push_back(make_pair(0, i));
        }
        if(matrix[r][i] == 1){
            v.push_back(make_pair(c, i));
        }
    }
    return v;
}

bool checkWay(pair<int, int> p1, pair<int, int> p2){
    if(p1.first == p2.first){
        if(p2.second > p1.second)
            swap(p1, p2);
        return matrix[p1.first][p1.second] > 0 && (matrix[p1.first + 1][p1.second] > 0 || p1.first == r);
    }else if(p1.second == p2.second){
        if(p2.first > p1.first)
            swap(p1, p2);
        return matrix[p1.first][p1.second] > 0 && (matrix[p1.first][p1.second + 1] > 0 || p1.second == c);
    }
    return false;
}

void subtract(pair<int, int> p1, pair<int, int> p2){
    if(p1.first == p2.first){
        if(p2.second > p1.second)
            swap(p1, p2);
        matrix[p1.first][p1.second]--;
        if(p1.first != r) matrix[p1.first + 1][p1.second]--;
    }else if(p1.second == p2.second){
        if(p2.first > p1.first)
            swap(p1, p2);
        matrix[p1.first][p1.second]--;
        if(p1.second != c) matrix[p1.first][p1.second + 1]--;
    }
}

void add(pair<int, int> p1, pair<int, int> p2){
    if(p1.first == p2.first){
        if(p2.second > p1.second)
            swap(p1, p2);
        matrix[p1.first][p1.second]++;
        if(p1.first != r) matrix[p1.first + 1][p1.second]++;
    }else if(p1.second == p2.second){
        if(p2.first > p1.first)
            swap(p1, p2);
        matrix[p1.first][p1.second]++;
        if(p1.second != c) matrix[p1.first][p1.second + 1]++;
    }
}

void afis(){
    for (int i = -1; i < r; ++i) {
        for (int j = -1; j < c; ++j) {
            cout<<dot[i + 1][j + 1]<<' ';
        }
        cout<<endl;
    }
    cout<<endl;
}

void afis2(){
    for (int i = -1; i < r; ++i) {
        for (int j = -1; j < c; ++j) {
            cout<<matrix[i + 1][j + 1]<<' ';
        }
        cout<<endl;
    }
    cout<<endl;
}

void goThrough(pair<int, int> porn, int depth = 1){
    if(porn.first == start.first && porn.second == start.second && depth > 1){
        if(!check())
            return;
        done = true;
        cout<<depth - 1;
        return;
    }
    else if(dot[porn.first][porn.second] && porn != start) return;
    dot[porn.first][porn.second] = true;
//    afis();
    for (int i = 0; i < 4; ++i) {
        pair<int, int> newDot = make_pair(porn.first + dx[i], porn.second + dy[i]);
        if(checkWay(porn, newDot)){
            subtract(porn, newDot);
            goThrough(newDot, depth + 1);
            if(done) return;
            add(porn, newDot);
        }
    }
    dot[porn.first][porn.second] = false;
}

int main() {
    init();
    for (int acsl = 0; acsl < 10; ++acsl) {
        reset();
        f>>r>>c;
        for (int i = 1; i <= r; ++i) {
            readLine(i);
        }
        if(four) cout<<4;
        else{
//            afis2();
            vector<pair<int, int>> starts = findStart();
            for (int i = 0; i < starts.size(); ++i) {
                start = starts[i];
                goThrough(start);
                if(done) break;
            }
            starts.clear();
            if(start.first == -1 && start.second == -1)
                cout<<4;
        }
        cout<<"\n";
    }
    return 0;
}
