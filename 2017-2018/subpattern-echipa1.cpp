#include <iostream>
#include <fstream>
#include <cstring>
using namespace std;
bool a[41][41];
int n,m;
int baze(char x)
{
    if(x>='0' && x<='9')
        return x-'0';
    else
        return x-'A'+10;
}

bool check(int cn, int cm){
    for(int i = 0; i < n; ++i){
        for(int j = 0; j < m; ++j){
            if(a[i][j] != a[i%cn][j%cm]){
                return false;
            }
        }
    }
    return true;
}

void nuke(){
    for(int i = 0; i < n; ++i){
        for(int j = 0; j < m; ++j){
            a[i][j] = false;
        }
    }
}

ifstream f("subpattern.in");
void read(){
    char s[4];
    f>>n>>m;
    nuke();
    for(int i=0;i<n;i++)
    {
        f>>s;
        int len=strlen(s);
        for(int j=0;j<len;j++)
        {
            int x=baze(s[j]);
            for(int k=3;k>=0;k--)
            {
                a[i][j*4+k]=x&1;
                x>>=1;
            }
        }
    }
}

void solve(){
    int ansn = n, ansm = m;
    for(int i = 1; i <= n; ++i){
        for(int j = 1; j <= m; ++j){
            if(n % i == 0 && m % j == 0){
                if(i*j < ansn*ansm && check(i, j)){
                    ansn = i;
                    ansm = j;
                }
            }
        }
    }
    
    cout << ansn << " " << ansm << "\n";
}

int main()
{
    for(int acsl = 0; acsl < 10; ++acsl){
        read();
        solve();
    }
    return 0;
}