#include <iostream>
#include <fstream>

using namespace std;

ifstream f("date.in");

const int dirImpI[] = {0, 1, 1, 1, 0, -1};
const int dirImpJ[] = {-1, -1, 0, 1, 1, 0};

const int dirParI[] = {0, 1, 0, -1, -1, -1};
const int dirParJ[] = {-1, 0, 1, 1, 0, -1};

int teren[1000][30];
pair<int, int> start, finish;
int k, cate;

void bordare(){
    for (int i = 0; i < 1000; ++i) {
        teren[i][0] = 1;
        if(i <= 29)
            teren[0][i] = 1;
    }
}

void reset(){
    cate = 0;
    start = make_pair(0, 0);
    finish = make_pair(0, 0);
    k = 0;
}

void afisare(){
    int x = 10;
    for (int i = 0; i < x; ++i) {
        for (int j = 0; j < 28; ++j) {
            cout<<teren[i][j]<<' ';
        }
        cout<<endl;
    }
    cout<<endl;
}

void backt(int i = start.second, int j = start.first, int depth = 0){
    if(depth >= k && (i != finish.second || j != finish.first))
        return;
    if(depth == k && i == finish.second && j == finish.first){
        cate++;
        teren[i][j] = 3;
//        cout<<i<<' '<<j<<endl;
//        afisare();
        teren[i][j] = 0;
        return;
    }
    for (int l = 0; l < 6; ++l) {
        int nouI, nouJ;
        if(j % 2 != 0){
            nouI = i + dirImpI[l], nouJ = j + dirImpJ[l];
        }else{
            nouI = i + dirParI[l], nouJ = j + dirParJ[l];
        }
        if(teren[nouI][nouJ] != 0) continue;
        teren[nouI][nouJ] = 2;
        backt(nouI, nouJ, depth + 1);
        teren[nouI][nouJ] = 0;
    }
}

int main() {
    bordare();
    for (int acsl = 0; acsl < 10; ++acsl) {
        reset();
        char sL, fL;
        f>>sL>>start.second>>fL>>finish.second>>k;
        start.first = sL - 'A' + 1;
        finish.first = fL - 'A' + 1;
        teren[start.second][start.first] = 1;
        backt();
        teren[start.second][start.first] = 0;
        cout<<cate<<"\n";
    }
    return 0;
}
