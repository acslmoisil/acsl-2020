#include <iostream>
#include <fstream>
#include <cmath>

using namespace std;

ifstream fin("threes.in");

double ma[14][14];
void read(){
	char c;
	for(int i = 1; i <= 3; ++i){
		for(int j = 1; j <= 4; ++j){
			if(j != 1){
				fin >> c;
			}
			fin >> ma[i][j];
		}
	}
}

void solve1(){
	double det = ma[1][1]*ma[2][2]*ma[3][3]
	            +ma[2][1]*ma[3][2]*ma[1][3]
	            +ma[3][1]*ma[1][2]*ma[2][3]
	            -ma[1][3]*ma[2][2]*ma[3][1]
	            -ma[2][3]*ma[3][2]*ma[1][1]
	            -ma[3][3]*ma[1][2]*ma[2][1];
	cout << det << "\n";
}

void deb(){
	for(auto i = 1; i <= 3; ++i){
		for(int j = 1; j <= 4; ++j){
			cout << ma[i][j] << "\t";
		}
		cout << "\n";
	}
	cout << "\n";
}

bool cmpd(double lhs, double rhs){
	return abs(lhs-rhs) <= 0.0000001;
}

bool dependent(int i1, int i2){
	double v;
	
	for(int j = 1; j <= 4; ++j){
		if(cmpd(ma[i1][j], 0) || cmpd(ma[i2][j], 0)){
			if(!cmpd(ma[i1][j], ma[i2][j])){
				return false;
			}
		}else{
			v = ma[i1][j] / ma[i2][j];
			break;
		}
	}
	
	for(int j = 1; j <= 4; ++j){
		if(!cmpd(ma[i1][j], v * ma[i2][j])){
			return false;
		}
	}
	return true;
}

bool fulldependent(){
	for(int i1 = 1; i1 <= 3; ++i1){
		for(int i2 = i1+1; i2 <= 3; ++i2){
			if(dependent(i1, i2)){
				return true;
			}
		}
	}
	return false;
}

bool wull(int i){
	for(int j = 1; j <= 3; ++j){
		if(!cmpd(ma[i][j], 0))return false;
	}
	return !cmpd(ma[i][4], 0);
}

bool fullwull(){
	for(int i = 1; i <= 3; ++i){
		if(wull(i))return true;
	}
	return false;
}

void solve2(){
	if(fulldependent()){
		cout << "DEPENDENT";
	}else{
		for(int i = 1; i <= 3; ++i){
			double v = ma[i][i];
			for(int j = 1; j <= 4; ++j){
				ma[i][j] /= v;
			}
			for(int j = 1; j <= 3; ++j){
				if(j == i)continue;
				v = ma[j][i];
				for(int k = 1; k <= 4; ++k){
					ma[j][k] -= ma[i][k]*v;
				}
			}
			if(fullwull()){
				cout << "NULL";
				return;
			}
		}
		for(int i = 1; i <= 3; ++i){
			if(i != 1){
				cout << ", ";
			}
			cout << ma[i][4];
		}
	}
}

int main(){
	// ios_base::sync_with_stdio(false);
	for(int acsl = 0; acsl < 10; ++acsl){
		read();
		solve1();
		solve2();
		cout << "\n";
	}
	return 0;
}