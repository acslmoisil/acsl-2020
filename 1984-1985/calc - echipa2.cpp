#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <regex>
#include <queue>
#include <algorithm>
using namespace std;
ifstream f("excel2electricboogaloo.in");
string theMatrix[1000][1000];
string oldVals[1000][1000];
int n, m;
int ppLengthCheck = 0;
string valueOfCell(int line, int column, int initL = -1, int initC = -1);
void littleBoy(){
    n = 4;
    m = 4;
    theMatrix[1][1] = "";
    theMatrix[1][2] = "QUIZ1";
    theMatrix[1][3] = "TEST";
    theMatrix[1][4] = "HW PTS";
    theMatrix[2][1] = "JOHN";
    theMatrix[2][2] = "43";
    theMatrix[2][3] = "73";
    theMatrix[2][4] = "123";
    theMatrix[3][1] = "MARY";
    theMatrix[3][2] = "32";
    theMatrix[3][3] = "92";
    theMatrix[3][4] = "157";
    theMatrix[4][1] = "CHRIS";
    theMatrix[4][2] = "40";
    theMatrix[4][3] = "87";
    theMatrix[4][4] = "63";
}
int precPos(char oper, char op[]){
    int l = strlen(op);
    for (int i = 0; i < l; ++i) {
        if(oper == op[i])
            return i;
    }
}
double evalUserFuncc(string expression, int oldL = 0, int oldC = 0){
    queue<double> valori;
    queue<char> operatowo;
    char op[] = "+-/*";
    int prec[] = {0, 0, 0, 0};
    int line = 0, col = 0;
    bool cell = false;
    double val = 0;
    for (int i = 0; i < expression.length(); ++i) {
        if(isupper(expression[i])){
            col = (int)(expression[i] - 'A' + 1);
            cell = true;
        }else if(strchr(op, expression[i]) != nullptr && cell){
            cell = false;
            if(valueOfCell(line, col, oldL, oldC).length() == 0 || islower(valueOfCell(line, col, oldL, oldC)[0]) || isupper(valueOfCell(line, col, oldL, oldC)[0])){
                val = 0;
            }else val = (double)stof(valueOfCell(line, col, oldL, oldC));
            valori.push(val);
            operatowo.push(expression[i]);
            val = 0;
            line = 0;
            col = 0;
        }else if(strchr(op, expression[i]) != nullptr){
            valori.push(val);
            operatowo.push(expression[i]);
            val = 0;
            line = 0;
            col = 0;
        }else if(cell){
            line *= 10;
            line += (int)(expression[i] - '0');
        }else val = val * 10 + (int)(expression[i] - '0');
    }
    if(cell){
        if(valueOfCell(line, col, oldL, oldC).length() == 0 || islower(valueOfCell(line, col, oldL, oldC)[0]) || isupper(valueOfCell(line, col, oldL, oldC)[0])){
            val = 0;
        }else val = (double)stof(valueOfCell(line, col, oldL, oldC));
        valori.push(val);
    }else{
        valori.push(val);
    }
    double returner = valori.front();
    valori.pop();
    while(!valori.empty() && !operatowo.empty()){
        switch(operatowo.front()){
            case '+': returner += valori.front();
                        valori.pop();
                        operatowo.pop();
                        break;
            case '-': returner -= valori.front();
                valori.pop();
                operatowo.pop();
                break;
            case '*': returner *= valori.front();
                valori.pop();
                operatowo.pop();
                break;
            case '/': returner /= valori.front();
                valori.pop();
                operatowo.pop();
                break;

        }
    }
    return returner;
}
double calcRangeSum(int col1, int lin1, int col2, int lin2){
    double sum = 0;
    if(col1 == col2){
        for(int i = lin1; i <= lin2; ++i){
            if(isdigit(theMatrix[i][col1][0])) sum += stof(valueOfCell(i, col1));
        }
        for(int i = lin2; i <= lin1; ++i){
            if(isdigit(theMatrix[i][col1][0])) sum += stof(valueOfCell(i, col1));
        }
    }else if(lin1 == lin2){
        for (int i = col1; i <= col2; ++i) {
            if(isdigit(theMatrix[lin1][i][0])) sum += stof(valueOfCell(lin1, i));
        }
        for (int i = col2; i <= col1; ++i) {
            if(isdigit(theMatrix[lin1][i][0])) sum += stof(valueOfCell(lin1, i));
        }
    }
    return sum;
}
double calcRangeAve(int col1, int lin1, int col2, int lin2){
    double sum = 0;
    if(col1 == col2){
        for(int i = lin1; i <= lin2; ++i){
            if(isdigit(theMatrix[i][col1][0])) sum += stof(valueOfCell(i, col1));
        }
        for(int i = lin2; i <= lin1; ++i){
            if(isdigit(theMatrix[i][col1][0])) sum += stof(valueOfCell(i, col1));
        }
        sum /= (abs(lin1 - lin2) + 1);
    }else if(lin1 == lin2){
        for (int i = col1; i <= col2; ++i) {
            if(isdigit(theMatrix[lin1][i][0])) sum += stof(valueOfCell(lin1, i));
        }
        for (int i = col2; i <= col1; ++i) {
            if(isdigit(theMatrix[lin1][i][0])) sum += stof(valueOfCell(lin1, i));
        }
        sum /= (abs(col1 - col2) + 1);
    }
    return sum;
}
double calcRangeMin(int col1, int lin1, int col2, int lin2){
    double sum = 100000000;
    if(col1 == col2){
        for(int i = lin1; i <= lin2; ++i){
            if(isdigit(theMatrix[i][col1][0])) min(sum, (double)stof(valueOfCell(i, col1)));
        }
        for(int i = lin2; i <= lin1; ++i){
            if(isdigit(theMatrix[i][col1][0])) min(sum, (double)stof(valueOfCell(i, col1)));
        }
        sum /= (abs(lin1 - lin2) + 1);
    }else if(lin1 == lin2){
        for (int i = col1; i <= col2; ++i) {
            if(isdigit(theMatrix[lin1][i][0])) min(sum, (double)stof(valueOfCell(lin1, i)));
        }
        for (int i = col2; i <= col1; ++i) {
            if(isdigit(theMatrix[lin1][i][0])) min(sum, (double)stof(valueOfCell(lin1, i)));
        }
        sum /= (abs(col1 - col2) + 1);
    }
    return sum;
}
double calcRangeMax(int col1, int lin1, int col2, int lin2){
    double sum = -100000000;
    if(col1 == col2){
        for(int i = lin1; i <= lin2; ++i){
            if(isdigit(theMatrix[i][col1][0])) max(sum, (double)stof(valueOfCell(i, col1)));
        }
        for(int i = lin2; i <= lin1; ++i){
            if(isdigit(theMatrix[i][col1][0])) max(sum, (double)stof(valueOfCell(i, col1)));
        }
        sum /= (abs(lin1 - lin2) + 1);
    }else if(lin1 == lin2){
        for (int i = col1; i <= col2; ++i) {
            if(isdigit(theMatrix[lin1][i][0])) max(sum, (double)stof(valueOfCell(lin1, i)));
        }
        for (int i = col2; i <= col1; ++i) {
            if(isdigit(theMatrix[lin1][i][0])) max(sum, (double)stof(valueOfCell(lin1, i)));
        }
        sum /= (abs(col1 - col2) + 1);
    }
    return sum;
}
void rangePrint(int col1, int lin1, int col2, int lin2){
    if(col1 == col2){
        for(int i = lin1; i <= lin2; ++i){
            cout<<valueOfCell(i, col1)<<' ';
        }
        for(int i = lin2; i <= lin1; ++i){
            cout<<valueOfCell(i, col1)<<' ';
        }
    }else if(lin1 == lin2){
        for (int i = col1; i <= col2; ++i) {
            cout<<valueOfCell(lin1, i)<<' ';
        }
        for (int i = col2; i <= col1; ++i) {
            cout<<valueOfCell(lin1, i)<<' ';
        }
    }
    cout<<"\n";
}
void lineInsert(int l){
    for(int i = n + 1; i > l; i--){
        for (int j = 1; j <= m; ++j) {
            theMatrix[i][j] = theMatrix[i - 1][j];
        }
    }
    for (int j = 0; j <= m; ++j) {
        theMatrix[l][j] = "";
    }
    n++;
}
void columnInsert(int c){
    for(int i = m + 1; i > c; i--){
        for (int j = 1; j <= n; ++j) {
            theMatrix[j][i] = theMatrix[j][i - 1];
        }
    }
    for (int j = 0; j <= n; ++j) {
        theMatrix[j][c] = "";
    }
    m++;
}
void lineDelete(int l){
    for(int i = l; i < n; i++){
        for (int j = 0; j <= m; ++j) {
            theMatrix[i][j] = theMatrix[i + 1][j];
        }
    }
    for (int j = 0; j <= m; ++j) {
        theMatrix[n][j] = "";
    }
    n--;
}
void columnDelete(int c){
    for(int i = c; i < m; i++){
        for (int j = 0; j <= n; ++j) {
            theMatrix[j][i] = theMatrix[j][i + 1];
        }
    }
    for (int j = 0; j <= n; ++j) {
        theMatrix[j][m] = "";
    }
    m--;
}
string valueOfCell(int line, int column, int initL, int initC){
    string output = "";
    if(initL == line && initC == column){
        return oldVals[line][column];
    }
    if(theMatrix[line][column][0] == '#'){
        output = to_string(evalUserFuncc(theMatrix[line][column].substr(1), line, column));
    }else if(theMatrix[line][column][0] == '@'){
        int findOpenBracket = theMatrix[line][column].find("(");
        int findDots = theMatrix[line][column].find("..");
        int findClosedBracket = theMatrix[line][column].find(")");
        int col1 = (int)(theMatrix[line][column][findOpenBracket + 1] - 'A' + 1);
        int lin1 = stoi(theMatrix[line][column].substr(findOpenBracket + 2, findDots - findOpenBracket - 2));
        int col2 = (int)(theMatrix[line][column][findDots + 2] - 'A' + 1);
        int lin2 = stoi(theMatrix[line][column].substr(findDots + 3, findClosedBracket - findDots - 3));
        if(theMatrix[line][column].substr(1, 3) == "SUM") output = to_string(calcRangeSum(col1, lin1, col2, lin2));
        if(theMatrix[line][column].substr(1, 3) == "AVE") output = to_string(calcRangeAve(col1, lin1, col2, lin2));
        if(theMatrix[line][column].substr(1, 3) == "MAX") output = to_string(calcRangeMax(col1, lin1, col2, lin2));
        if(theMatrix[line][column].substr(1, 3) == "MIN") output = to_string(calcRangeMin(col1, lin1, col2, lin2));
    }else output = theMatrix[line][column];
    return output;
}

void pearlHarbour(const string& kernelSandersKentuckyFriedChicken){
    regex assignmentNumber(R"([QWERTYUIOPASDFGHJKLZXCVBNM]\d+:\d+)");
    regex assignmentString(R"([QWERTYUIOPASDFGHJKLZXCVBNM]\d+:[QWERTYUIOPASDFGHJKLZXCVBNM].*)");
    regex assignmentUserFunc(R"([QWERTYUIOPASDFGHJKLZXCVBNM]\d+:#.*)");
    regex assignmentSysFuncSum(R"([QWERTYUIOPASDFGHJKLZXCVBNM]\d+:@SUM\([QWERTYUIOPASDFGHJKLZXCVBNM]\d+\.\.[QWERTYUIOPASDFGHJKLZXCVBNM]\d+\))");
    regex assignmentSysFuncAve(R"([QWERTYUIOPASDFGHJKLZXCVBNM]\d+:@AVE\([QWERTYUIOPASDFGHJKLZXCVBNM]\d+\.\.[QWERTYUIOPASDFGHJKLZXCVBNM]\d+\))");
    regex assignmentSysFuncMin(R"([QWERTYUIOPASDFGHJKLZXCVBNM]\d+:@MIN\([QWERTYUIOPASDFGHJKLZXCVBNM]\d+\.\.[QWERTYUIOPASDFGHJKLZXCVBNM]\d+\))");
    regex assignmentSysFuncMax(R"([QWERTYUIOPASDFGHJKLZXCVBNM]\d+:@MAX\([QWERTYUIOPASDFGHJKLZXCVBNM]\d+\.\.[QWERTYUIOPASDFGHJKLZXCVBNM]\d+\))");
    regex printCell(R"(/P:[QWERTYUIOPASDFGHJKLZXCVBNM]\d+)");
    regex printLine(R"(/P:\d+)");
    regex printColumn(R"(/P:[QWERTYUIOPASDFGHJKLZXCVBNM])");
    regex printRange(R"(/P:[QWERTYUIOPASDFGHJKLZXCVBNM]\d+\.\.[QWERTYUIOPASDFGHJKLZXCVBNM]\d+)");
    regex insertLine(R"(/I:\d+)");
    regex insertColumn(R"(/I:[QWERTYUIOPASDFGHJKLZXCVBNM])");
    regex deleteLine(R"(/D:\d+)");
    regex deleteColumn(R"(/D:[QWERTYUIOPASDFGHJKLZXCVBNM])");
    if(regex_match(kernelSandersKentuckyFriedChicken, assignmentNumber)){
        int posColumn = kernelSandersKentuckyFriedChicken.find(":");
        int column = (int)(kernelSandersKentuckyFriedChicken[0] - 'A' + 1);
        int line = stoi(kernelSandersKentuckyFriedChicken.substr(1, posColumn - 1));
        double val = (double)stof(kernelSandersKentuckyFriedChicken.substr(posColumn + 1));
        theMatrix[line][column] = to_string(val);
        if(line > n) n = line;
        if(column > m) m = column;
    }else if(regex_match(kernelSandersKentuckyFriedChicken, assignmentString)){
        int posColumn = kernelSandersKentuckyFriedChicken.find(":");
        int column = (int)(kernelSandersKentuckyFriedChicken[0] - 'A' + 1);
        int line = stoi(kernelSandersKentuckyFriedChicken.substr(1, posColumn - 1));
        string val = kernelSandersKentuckyFriedChicken.substr(posColumn + 1);
        theMatrix[line][column] = val;
        if(line > n) n = line;
        if(column > m) m = column;
    }else if(regex_match(kernelSandersKentuckyFriedChicken, assignmentUserFunc)){
        int posColumn = kernelSandersKentuckyFriedChicken.find(":");
        int column = (int)(kernelSandersKentuckyFriedChicken[0] - 'A' + 1);
        int line = stoi(kernelSandersKentuckyFriedChicken.substr(1, posColumn - 1));
        oldVals[line][column] = theMatrix[line][column];
        theMatrix[line][column] = kernelSandersKentuckyFriedChicken.substr(posColumn + 1);
        if(line > n) n = line;
        if(column > m) m = column;
    }else if(regex_match(kernelSandersKentuckyFriedChicken, assignmentSysFuncSum)){
        int posColumn = kernelSandersKentuckyFriedChicken.find(":");
        int column = (int)(kernelSandersKentuckyFriedChicken[0] - 'A' + 1);
        int line = stoi(kernelSandersKentuckyFriedChicken.substr(1, posColumn - 1));
        theMatrix[line][column] = kernelSandersKentuckyFriedChicken.substr(posColumn + 1);
        if(line > n) n = line;
        if(column > m) m = column;
    }else if(regex_match(kernelSandersKentuckyFriedChicken, assignmentSysFuncAve)){
        int posColumn = kernelSandersKentuckyFriedChicken.find(":");
        int column = (int)(kernelSandersKentuckyFriedChicken[0] - 'A' + 1);
        int line = stoi(kernelSandersKentuckyFriedChicken.substr(1, posColumn - 1));
        theMatrix[line][column] = kernelSandersKentuckyFriedChicken.substr(posColumn + 1);
        if(line > n) n = line;
        if(column > m) m = column;
    }else if(regex_match(kernelSandersKentuckyFriedChicken, assignmentSysFuncMin)){
        int posColumn = kernelSandersKentuckyFriedChicken.find(":");
        int column = (int)(kernelSandersKentuckyFriedChicken[0] - 'A' + 1);
        int line = stoi(kernelSandersKentuckyFriedChicken.substr(1, posColumn - 1));
        theMatrix[line][column] = kernelSandersKentuckyFriedChicken.substr(posColumn + 1);
        if(line > n) n = line;
        if(column > m) m = column;
    }else if(regex_match(kernelSandersKentuckyFriedChicken, assignmentSysFuncMax)){
        int posColumn = kernelSandersKentuckyFriedChicken.find(":");
        int column = (int)(kernelSandersKentuckyFriedChicken[0] - 'A' + 1);
        int line = stoi(kernelSandersKentuckyFriedChicken.substr(1, posColumn - 1));
        theMatrix[line][column] = kernelSandersKentuckyFriedChicken.substr(posColumn + 1);
        if(line > n) n = line;
        if(column > m) m = column;
    }else if(regex_match(kernelSandersKentuckyFriedChicken, printCell)){
        int column = (int)(kernelSandersKentuckyFriedChicken[3] - 'A' + 1);
        int line = stoi(kernelSandersKentuckyFriedChicken.substr(4));
        ppLengthCheck++;
        cout<<valueOfCell(line, column)<<'\n';
        if(line > n) n = line;
        if(column > m) m = column;
    }else if(regex_match(kernelSandersKentuckyFriedChicken, printColumn)){
        int column = (int)(kernelSandersKentuckyFriedChicken[3] - 'A' + 1);
        for (int i = 1; i <= n; ++i) {
            cout<<valueOfCell(i, column)<<' ';
        }
        cout<<"\n";
        ppLengthCheck++;
        if(column > m) m = column;
    }else if(regex_match(kernelSandersKentuckyFriedChicken, printLine)){
        int line = stoi(kernelSandersKentuckyFriedChicken.substr(3));
        for (int i = 1; i <= n; ++i) {
            cout<<valueOfCell(line, i)<<' ';
        }
        cout<<"\n";
        ppLengthCheck++;
        if(line > n) n = line;
    }else if(regex_match(kernelSandersKentuckyFriedChicken, printRange)){
        int findDots = kernelSandersKentuckyFriedChicken.find("..");
        int col1 = (int)(kernelSandersKentuckyFriedChicken[3] - 'A' + 1);
        int lin1 = stoi(kernelSandersKentuckyFriedChicken.substr(4, findDots - 4));
        int col2 = (int)(kernelSandersKentuckyFriedChicken[findDots + 2] - 'A' + 1);
        int lin2 = stoi(kernelSandersKentuckyFriedChicken.substr(findDots + 3));
        rangePrint(col1, lin1, col2, lin2);
        ppLengthCheck++;
    }else if(regex_match(kernelSandersKentuckyFriedChicken, insertColumn)){
        int column = (int)(kernelSandersKentuckyFriedChicken[3] - 'A' + 1);
        columnInsert(column);
    }else if(regex_match(kernelSandersKentuckyFriedChicken, insertLine)){
        int line = stoi(kernelSandersKentuckyFriedChicken.substr(3));
        lineInsert(line);
    }else if(regex_match(kernelSandersKentuckyFriedChicken, deleteColumn)){
        int column = (int)(kernelSandersKentuckyFriedChicken[3] - 'A' + 1);
        columnDelete(column);
    }else if(regex_match(kernelSandersKentuckyFriedChicken, deleteLine)){
        int line = stoi(kernelSandersKentuckyFriedChicken.substr(3));
        lineDelete(line);
    }
}
int main() {
    littleBoy();
    ppLengthCheck = 0;
    while(ppLengthCheck < 20){
        string kernelSanderssRecipy;
        getline(f, kernelSanderssRecipy);
        pearlHarbour(kernelSanderssRecipy);
    }
    return 0;
}
