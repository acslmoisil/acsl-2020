#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stack>
#include <map>

using namespace std;

ifstream fin("acslcalc.in");

int prints = 0;

int chartoint(char c){
    return (c-'0');
}

int stringtoint(const string &s){
    int r = 0;
    for(auto c : s){
        r *= 10;
        r += chartoint(c);
    }
    return r;
}

struct Cell{
    //0 - empty
    //1 - numeric constant
    //2 - string constant
    //3 - user-defined function
    //4 - system-defined function
    int type;
    string value;
    double result;
    bool evaluated = false;
    Cell() : type(0){}
    void prevaluate(){
    }
    double evaluate();
    void print(){
        if(type == 1 || type == 2){
            cout << value;
        }else if(type == 3 || type == 4){
            cout << result;
        }
        cout << " ";
    }
};

struct Position{
    int x, y;
    Position(){}
    Position(string s){
        x = (s[0]-'A');
        y = (s[1]-'1');
    }
};

bool isletter(char c){
    return ((c>='A' && c<='Z') || (c>='a' && c<='z'));
}

bool isdigit(char c){
    return (c>='0' && c<='9');
}

int sign(int a){
    if(a > 0){
        return 1;
    }else if(a == 0){
        return 0;
    }else{
        return -1;
    }
}

struct Range{
    vector<Position> positions;
    Range(){}
    Range(string s){
        if(s.size() == 1){
            if(isletter(s[0])){
                for(int i = 1; i <= 9; ++i){
                    positions.push_back(Position(s + char(i+'0')));
                }
            }else if(isdigit(s[0])){
                for(int i = 0; i < 26; ++i){
                    positions.push_back(Position(char(i+'A') + s));
                }
            }
        }else if(s.size() == 2){
            positions.push_back(Position(s));
        }else{
            string s1 = s.substr(0, 2);
            string s2 = s.substr(s.size()-2);
            int inc = sign(s2[0]-s1[0]) + sign(s2[1]-s1[1]);

            positions.push_back(Position(s1));
            while(s1 != s2){
                if(s1[0] != s2[0]){
                    s1[0] += inc;
                }else{
                    s1[1] += inc;
                }
                positions.push_back(Position(s1));
            }
        }
    }
};

struct Sheet{
    Cell cells[41][41];
    void update(){
        for(int x = 0; x < 26; ++x){
            for(int y = 0; y < 9; ++y){
                cells[x][y].evaluated = false;
            }
        }
        for(int x = 0; x < 26; ++x){
            for(int y = 0; y < 9; ++y){
                cells[x][y].evaluate();
            }
        }
    }
    Cell &cell(Position position){
        return cells[position.x][position.y];
    }
    void setcell(Position position, string value){
        Cell &cell = this->cell(position);
        if(value[0] == '#'){
            cell.type = 3;
            value.erase(value.begin());
        }else if(value[0] == '@'){
            cell.type = 4;
            value.erase(value.begin());
        }else if(isletter(value[0])){
            cell.type = 2;
        }else{
            cell.type = 1;
        }
        cell.value = value;
    }
    Sheet(){
        setcell(Position("A2"), "JOHN");
        setcell(Position("A3"), "MARY");
        setcell(Position("A4"), "CHRIS");

        setcell(Position("B1"), "QUIZ1");
        setcell(Position("B2"), "43");
        setcell(Position("B3"), "37");
        setcell(Position("B4"), "40");

        setcell(Position("C1"), "TEST");
        setcell(Position("C2"), "73");
        setcell(Position("C3"), "92");
        setcell(Position("C4"), "87");

        setcell(Position("D1"), "HW PTS");
        setcell(Position("D2"), "123");
        setcell(Position("D3"), "157");
        setcell(Position("D4"), "63");
    }
    void idelete(string s){
        if(isletter(s[0])){
            Position position(s+'1');
            for(int x = 26-1; x >= position.x; --x){
                for(int y = 0; y < 9; ++y){
                    int nx = x+1;
                    if(nx >= 0 && nx < 26){
                        cells[x][y] = cells[nx][y];
                    }else{
                        cells[x][y] = Cell();
                    }
                }
            }
        }else if(isdigit(s[0])){
            Position position('A'+s);
            for(int y = 9-1; y >= position.y; --y){
                for(int x = 0; x < 26; ++x){
                    int ny = y+1;
                    if(ny >= 0 && ny < 9){
                        cells[x][y] = cells[x][ny];
                    }else{
                        cells[x][y] = Cell();
                    }
                }
            }
        }
    }
    void iinsert(string s){
        if(isletter(s[0])){
            Position position(s+'1');
            for(int x = 26-1; x > position.x; --x){
                for(int y = 0; y < 9; ++y){
                    int nx = x-1;
                    if(nx >= 0 && nx < 26){
                        cells[x][y] = cells[nx][y];
                    }else{
                        cells[x][y] = Cell();
                    }
                }
            }
            for(int y = 0; y < 9; ++y){
                cells[position.x][y] = Cell();
            }
        }else if(isdigit(s[0])){
            Position position('A'+s);
            for(int y = 9-1; y > position.y; --y){
                for(int x = 0; x < 26; ++x){
                    int ny = y-1;
                    if(ny >= 0 && ny < 9){
                        cells[x][y] = cells[x][ny];
                    }else{
                        cells[x][y] = Cell();
                    }
                }
            }
            for(int x = 0; x < 26; ++x){
                cells[x][position.y] = Cell();
            }
        }
    }
};
Sheet sheet;

namespace op{
    map<char, int> priority{
            {'+', 1}, {'-', 1}, {'*', 2}, {'/', 2}
    };
    double evaluate(char c, double lhs, double rhs){
        if(c == '+'){
            return lhs+rhs;
        }else if(c == '-'){
            return lhs-rhs;
        }else if(c == '*'){
            return lhs*rhs;
        }else if(c == '/'){
            return lhs/rhs;
        }
        return 0;
    }
    double tooperand(string s){
        if(isletter(s[0])){
            return sheet.cell(Position(s)).evaluate();
        }else{
            return stringtoint(s);
        }
    }
}

double Cell::evaluate(){
    if(!evaluated){
        evaluated = true;
        if(type == 0){
            result = 0;
        }else if(type == 1){
            result = stringtoint(value);
        }else if(type == 2){
            result = 0;
        }else if(type == 3){
            result = 0;
            string s = "";
            char ope = '+';
            for(auto c : value){
                if(op::priority.count(c) > 0){
                    result = op::evaluate(ope, result, op::tooperand(s));
                    s.clear();
                    ope = c;
                }else{
                    s += c;
                }
            }
            result = op::evaluate(ope, result, op::tooperand(s));
        }else if(type == 4){
            string function = value.substr(0, 3);
            Range range(value.substr(4, value.size()-4-1));

            if(function == "SUM" || function == "AVE"){
                result = 0;
            }else if(function == "MIN" || function == "MAX"){
                result = sheet.cell(range.positions[0]).evaluate();
            }

            for(auto position : range.positions){
                if(function == "SUM" || function == "AVE"){
                    result += sheet.cell(position).evaluate();
                }else if(function == "MIN"){
                    result = min(result, sheet.cell(position).evaluate());
                }else if(function == "MAX"){
                    result = max(result, sheet.cell(position).evaluate());
                }
            }

            if(function == "AVE"){
                result /= double(range.positions.size());
            }
        }
    }
    return result;
}

void read(){
    string s;
    getline(fin, s);
    if(s[0] == '/'){
        string srange = s.substr(3);
        if(s[1] == 'P'){
            prints++;
            sheet.update();
            Range range(srange);
            for(auto position : range.positions){
                sheet.cell(position).print();
            }
            cout << "\n";
        }else if(s[1] == 'I'){
            sheet.iinsert(srange);
        }else if(s[1] == 'D'){
            sheet.idelete(srange);
        }
    }else{
        int colon = s.find(':');
        Position position(s.substr(0, colon));
        string value = s.substr(colon+1);
        sheet.setcell(position, value);
    }
}

int main(){
    // ios_base::sync_with_stdio(false);
    while(prints < 20){
        read();
    }
    return 0;
}