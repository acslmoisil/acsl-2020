#include <iostream>
#include <fstream>
#include <stack>
#include <cstring>

using namespace std;

ifstream f("post2pre.in");

stack<string> st;
char line[50];

void clear()
{
    while(!st.empty())
        st.pop();
}

bool cond(char x)
{
    return x=='+'||x=='-'||x=='*'||x=='/'||x=='^';
}

int main() {
    for (int acsl = 0; acsl < 10; ++acsl) {
        clear();
        f.getline(line,50);
        int nrl=strlen(line);
        for (int i = 0; i < nrl; ++i) {
            if(cond(line[i]))
            {
                string t1,t2,rez;
                t2=st.top();
                st.pop();
                t1=st.top();
                st.pop();
                rez=line[i];
                rez+=' ';
                rez+=t1;
                rez+=t2;
                st.push(rez);
            } else if(line[i]!=' ')
            {
                string t;
                t+=line[i];
                t+=' ';
                st.push(t);
            }
        }
        cout<<st.top()<<'\n';
    }
    return 0;
}
