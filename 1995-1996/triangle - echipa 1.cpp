#include <iostream>
#include <fstream>
#include <algorithm>
#include <string>

using namespace std;

struct dreapta{
    double a, b, c;
    dreapta(double a1, double b1, double c1){
        this->a = a1;
        this->b = b1;
        this->c = c1;
    }
    dreapta(){
        this->a = 0;
        this->b = 0;
        this->c = 0;
    }
};

struct punct{
    double x, y;
    punct(double x1, double y1){
        this->x = x1;
        this->y = y1;
    }
};
dreapta drep[8];
punct intersectie(dreapta d1, dreapta d2){
    if(d2.a * d1.b - d1.a * d2.b != 0){
        double y = (-d2.a * d1.c + d1.a * d2.c)/(d2.a * d1.b - d1.a * d2.b);
        double x = 0;
        if(d1.a != 0)
            x = (-d1.c - d1.b * y) / d1.a;
        else if(d2.a != 0) x = (-d2.c - d2.b * y)/ d2.a;
        return {x, y};
    }
    if(d1.a * d2.b - d2.a * d1.b != 0){
        double x = (-d1.c * d2.b + d2.c * d1.b)/(d1.a * d2.b - d2.a * d1.b);
        double y = 0;
        if(d1.b != 0)
            y =  (-d1.c - d1.a * x)/d1.b;
        else if(d2.b != 0) y =  (-d2.c - d2.a * x)/d2.b;
        return {x, y};
    }

    return {0, 0};
}

double area(punct a, punct b, punct c){
    return a.x * b.y + b.x * c.y + c.x * a.y - c.x * b.y - b.x * a.y - a.x * c.y;
}

void compute(string cele3){
    dreapta drepte[3];
    drepte[0] = drep[cele3[0] - 'a'];
    drepte[1] = drep[cele3[1] - 'a'];
    drepte[2] = drep[cele3[2] - 'a'];
    punct p1 = intersectie(drepte[0], drepte[1]), p2 = intersectie(drepte[1], drepte[2]), p3 = intersectie(drepte[0], drepte[2]);
    cout<<abs(area(p1, p2, p3)) / 2<<'\n';
}

int main() {
    ifstream f("date.in");
    for (int i = 0; i < 7; ++i) {
        int a, b, c;
        char comma;
        f>>a>>comma>>b>>comma>>c;
        drep[i] = dreapta(a, b, c);
    }
    f.get();
    for (int acsl = 0; acsl < 9; ++acsl) {
        string cele3;
        getline(f, cele3, ',');
        if(acsl != 0) cele3.erase(0, 1);
        compute(cele3);
    }
    string cele3;
    getline(f, cele3);
    cele3.erase(0, 1);
    compute(cele3);
    return 0;
}
