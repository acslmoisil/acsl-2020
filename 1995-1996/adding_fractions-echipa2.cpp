#include <iostream>
#include <fstream>

using namespace std;

struct frac{
    int a,b;
};

ifstream f("adding_fractions.in");

frac vec[10];
int n;
char vir;

int cmmdc(int a, int b)
{
    int r;
    while(b)
    {
        r=a%b;
        a=b;
        b=r;
    }
    return a;
}

int cmmmc(int a,int b)
{
    return a*b/cmmdc(a,b);
}

void solve()
{
    for (int i = 1; i < n; ++i) {
        int cm=cmmmc(vec[i-1].b,vec[i].b);
        vec[i].a=cm/vec[i-1].b*vec[i-1].a+cm/vec[i].b*vec[i].a;
        vec[i].b=cm;
    }
    int cm=cmmdc(vec[n-1].a,vec[n-1].b);
    vec[n-1].a/=cm;
    vec[n-1].b/=cm;
}

int main() {
    for (int acsl = 0; acsl < 10; ++acsl) {
        f>>n;
        for (int i = 0; i < n; ++i)
            f>>vir>>vec[i].a>>vir>>vec[i].b;
        solve();
        cout<<vec[n-1].a<<'/'<<vec[n-1].b<<'\n';
    }
    return 0;
}
