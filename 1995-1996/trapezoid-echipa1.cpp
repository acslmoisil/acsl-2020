#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <cmath>
#include <iomanip>

using namespace std;

ifstream fin("trapezoid.in");

typedef long double ldb;

struct Poly{
	vector<ldb> coef;
	ldb calc(ldb x){
		ldb p = 1, r = 0;
		for(auto c : coef){
			r += p*c;
			p *= x;
		}
		return r;
	}
};

Poly poly;
ldb lt, rt;
int res;
void read(){
	char c;
	int n;
	fin >> n;
	poly = Poly();
	for(int i = 0; i < n+1; ++i){
		ldb a;fin >> c >> a;
		poly.coef.push_back(a);
	}
	reverse(poly.coef.begin(), poly.coef.end());
	fin >> c >> lt >> c >> rt >> c >> res;
}

ldb hei;
ldb trap(ldb y1, ldb y2){
	return abs((y1+y2)/2 * hei);
}

ldb tri(ldb y1, ldb y2){
	ldb x = -(0*y2 - hei*y1) / (y1 - y2);
	return abs(x*y1/2) + abs((hei-x)*y2/2);
}

void solve(){
	ldb area = 0;
	
	hei = (rt-lt)/ldb(res);
	ldb x = lt;
	ldb y2 = poly.calc(x), y1;
	for(int i = 1; i <= res; ++i){
		x += hei;
		
		y1 = poly.calc(x);
		if((y1>0 && y2>0) || (y1<0 && y2<0)){
			area += trap(y1, y2);
		}else{
			area += tri(y1, y2);
		}
		
		y2 = y1;
	}
	
	// cout << fixed << setprecision(6);
	cout << area << "\n";
}

int main(){
	// ios_base::sync_with_stdio(false);
	for(int acsl = 0; acsl < 10; ++acsl){
		read();
		solve();
	}
	return 0;
}