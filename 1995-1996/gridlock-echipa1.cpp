#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

ifstream fin("gridlock.in");

struct vec2{
	int x, y;
	vec2 add(const vec2 &rhs){
		return {x+rhs.x, y+rhs.y};
	}
	bool eqq(const vec2 &rhs){
		return x==rhs.x && y==rhs.y;
	}
};

const int N = 41;
int s = 8;

int ma[N][N], vi[N][N];

void susta(){
	ifstream num("susta.txt");
	for(int y = s; y >= 1; --y){
		for(int x = 1; x <= s; ++x){
			num >> ma[x][y];
		}
	}
}

int n;
vector<vec2> pts;

void nuke(){
	pts.clear();
	for(int y = 0; y < N; ++y){
		for(int x = 0; x < N; ++x){
			vi[x][y] = 0;
		}
	}
}

void read(){
	char c;
	fin >> n;
	for(int i = 0; i < n; ++i){
		vec2 v;
		fin >> c >> v.y >> c >> v.x >> c;
		pts.push_back(v);
	}
}

int sign(int a){
	if(a > 0){
		return 1;
	}else if(a == 0){
		return 0;
	}else{
		return -1;
	}
}

void line(vec2 p1, vec2 p2){
	vec2 inc = {sign(p2.x-p1.x), sign(p2.y-p1.y)};
	for(vec2 i = p1; !i.eqq(p2); i = i.add(inc)){
		vi[i.x][i.y] = 1;
	}
	vi[p2.x][p2.y] = 1;
}

void dab(){
	for(int y = 1; y <= s; ++y){
		for(int x = 1; x <= s; ++x){
			cout << vi[x][y];
		}
		cout << "\n";
	}
	cout << "\n";
}

bool inb(vec2 p){
	return p.x >= 1 && p.x <= s && p.y >= 1 && p.y <= s;
}

vector<vec2> d = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};
void fill(vec2 p){
	vi[p.x][p.y] = 2;
	for(auto dv : d){
		vec2 np = p.add(dv);
		if(inb(np) && vi[np.x][np.y] == 0){
			fill(np);
		}
	}
}

void solve(){
	for(int i = 0; i < n; ++i){
		line(pts[i], pts[(i+1)%n]);
	}
	for(int i = 1; i <= s; ++i){
		if(vi[1][i] == 0){
			fill({1, i});
		}
		if(vi[s][i] == 0){
			fill({s, i});
		}
		if(vi[i][1] == 0){
			fill({i, 1});
		}
		if(vi[i][s] == 0){
			fill({i, s});
		}
	}
}

void write(){
	int ans = 0;
	for(int y = 1; y <= s; ++y){
		for(int x = 1; x <= s; ++x){
			if(vi[x][y] <= 1){
				ans += ma[x][y];
			}
		}
	}
	cout << ans << "\n";
}

int main(){
	// ios_base::sync_with_stdio(false);
	susta();
	for(int acsl = 0; acsl < 10; ++acsl){
		nuke();
		read();
		solve();
		// dab();
		write();
	}
	return 0;
}