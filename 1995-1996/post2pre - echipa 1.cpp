#include <iostream>
#include <fstream>
#include <algorithm>
#include <stack>

using namespace std;

ifstream f("post2pre.in");

string operators = "+-*/^";

struct node{
    char thing;
    node *left, *right;
    node(){
        this->thing = 0;
        this->left = nullptr;
        this->right = nullptr;
    }

    node(char thig){
        this->thing = thig;
        this->left = nullptr;
        this->right = nullptr;
    }

    node(char thig, node* let, node* rit){
        this->thing = thig;
        this->left = let;
        this->right = rit;
    }

} *root = nullptr;

void deleteMem(node* &cur){
    if(cur->left != nullptr){
        deleteMem(cur->left);
    }
    if(cur->right != nullptr){
        deleteMem(cur->right);
    }
    delete cur;
    cur = nullptr;
}

void parc(string &r, node* cur){
    r.push_back(cur->thing);
    if(cur->left != nullptr)
        parc(r, cur->left);
    if(cur->right != nullptr)
        parc(r, cur->right);
}

string convert(string exp){
    string result = "";
    reverse(exp.begin(), exp.end());
    stack<node*>cur;
    for(auto it : exp){

        node *curent = new node(it);
        if(root == nullptr)
            root = curent;
        TOP:
        if(!cur.empty() && cur.top()->right == nullptr) cur.top()->right = curent;
        else if(!cur.empty() && cur.top()->left == nullptr) cur.top()->left = curent;
        else if(!cur.empty()){ cur.pop(); goto TOP;}
        if(!cur.empty() && cur.top()->left != nullptr && cur.top()->right != nullptr) cur.pop();
        if(operators.find(it) != operators.npos)
            cur.push(curent);
    }
    parc(result, root);
    deleteMem(root);
    return result;
}

int main() {
    for (int acsl = 0; acsl < 10; ++acsl) {
        string expresie;
        getline(f, expresie);
        cout<<convert(expresie)<<'\n';
    }
    return 0;
}
