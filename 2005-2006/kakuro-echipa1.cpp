#include <iostream>
#include <cstdio>
#include <cstring>
using namespace std;
int n, m;
struct matrice{
    int inf;
    int ds, dd;
}sol[100][100];
int vizc[100][10], vizl[100][10];
int a[100][100];
int numar_diag(char s[10])
{
    int l=strlen(s);
    int nr=0;
    for(int i=0; i<l-1; i++)
        nr=nr*10+(s[i]-'0');
    return nr;
}
void citire()
{
    scanf("%d, %d\n", &n, &m);
    int nr;
    scanf("%d", &nr);
    for(int i=0; i<nr; i++)
    {
        int x;
        scanf(", %d", &x);
        int l=x/m+1;
        if(x%m==0)
            l--;
        int c=x%m;
        if(c==0)
            c=m;
        sol[l][c].inf=-1;
    }
    scanf("\n");
    int poz;
    char mac[10];
    do
    {
        scanf("%d, %s", &poz, mac);
        if(poz==0)
            break;
        int l=poz/m+1;
        if(poz%m==0)
            l--;
        int c=poz%m;
        if(c==0)
            c=m;
        int lung=strlen(mac);
        mac[lung-1]=0;
        lung--;
        if(mac[lung-1]=='A')
        {
            int nr=numar_diag(mac);
            sol[l][c].dd=nr;
            sol[l][c].inf=-2;
        }
        else
        {
            int nr=numar_diag(mac);
            sol[l][c].ds=nr;
            sol[l][c].inf=-2;
        }
    }while(poz!=0);
}

void afisare(){
    for(int i=1; i<=n; i++){
        for(int j=1; j<=m; j++)
            a[i][j] = sol[i][j].inf;
    }
}
int okk=0;
int verificare()
{
    for(int i=1; i<=n; i++)
        for(int j=1; j<=m; j++)
            if(sol[i][j].inf==-2)
            {
                if(sol[i][j].ds!=0)
                {
                    int s=0;
                    for(int k=i+1; k<=n; k++)
                        if(sol[k][j].inf>0)
                            s+=sol[k][j].inf;
                    if(s!=sol[i][j].ds)
                        return 0;
                }
                if(sol[i][j].dd!=0)
                {
                    int s=0;
                    for(int k=j+1; k<=m; k++)
                        if(sol[i][k].inf>0)
                            s+=sol[i][k].inf;
                    if(s!=sol[i][j].dd)
                        return 0;

                }
            }
    return 1;
}
void bak(int i, int j, int ok)
{
    if(okk==1)
        return;
    if(ok==1)
        return;
    if(i==n+1)
    {
        if(verificare()==1){
            ok=1;
            if(okk == 0){
                afisare();
                okk=1;
            }
        }
        return;
    }
    if(j==m+1)
    {
        bak(i+1, 1, ok);
        return;
    }
    if(sol[i][j].inf<0)
    {
        bak(i, j+1, ok);
        return;
    }
    for(int v=1; v<=9; v++)
    {
        if(vizl[i][v]==0 && vizc[j][v]==0)
        {
            sol[i][j].inf=v;
            vizl[i][v]=1;
            vizc[j][v]=1;
            bak(i, j+1, ok);
            vizl[i][v]=0;
            vizc[j][v]=0;
        }
    }
}
int main()
{
    freopen("kakuro.in", "r", stdin);
    citire();
    bak(1, 1, 0);
    for(int acsl=0; acsl<8; acsl++)
    {
        int poz;
        scanf("%d\n", &poz);
        int l=poz/m+1;
        if(poz%m==0)
            l--;
        int c=poz%m;
        if(c==0)
            c=m;
        cout<<a[l][c]<<'\n';
    }
    return 0;
}
