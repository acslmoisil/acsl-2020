#include <iostream>
#include <fstream>
#include <vector>
#include <cstring>
#include <algorithm>

using namespace std;

const int N = 1041;

ifstream fin("pushpop.in");

vector<int> vec, cvec;

void read(){
	int a;char c;
	for(fin >> a; a != 0; fin >> c >> a){
		vec.push_back(a);
	}
}

char dstr;
char line[N];

void ipop(){
	if(dstr == 'S'){
		cvec.pop_back();
	}else{
		cvec.erase(cvec.begin());
	}
}

void ideletesmallest(){
	auto small = cvec.begin();
	for(auto it = cvec.begin(); it != cvec.end(); ++it){
		if(*it < *small){
			small = it;
		}
	}
	cvec.erase(small);
}

void ideletebiggest(){
	auto big = cvec.end()-1;
	for(auto it = cvec.end(); it != cvec.begin(); --it){
		if(*(it-1) > *big){
			big = it-1;
		}
	}
	cvec.erase(big);
}

int chartoint(char c){
	return (c-'0');
}

int stringtoint(char *lt, char *rt){
	int r = 0;
	int s = 1;
	if(*lt == '+'){
		s = 1;
		lt++;
	}else if(*lt == '-'){
		s = -1;
		lt++;
	}
	while(lt != rt){
		r *= 10;
		r += chartoint(*lt);
		lt++;
	}
	return r*s;
}

void ipush(int a){
	cvec.push_back(a);
}

void iinsertbelowsmallest(int a){
	auto small = cvec.begin();
	for(auto it = cvec.begin(); it != cvec.end(); ++it){
		if(*it < *small){
			small = it;
		}
	}
	cvec.insert(small, a);
}

void iinsertabovebiggest(int a){
	auto big = cvec.end()-1;
	for(auto it = cvec.end(); it != cvec.begin(); --it){
		if(*(it-1) > *big){
			big = it-1;
		}
	}
	cvec.insert(big+1, a);
}

int imedian(){
	vector<int> prajit = cvec;
	sort(prajit.begin(), prajit.end());
	int size = prajit.size();
	if(size % 2 == 1){
		return prajit[size/2];
	}else{
		return (prajit[size/2]+prajit[size/2 - 1])/2;
	}
}

void iinsertmedian(int a){
	int med = imedian();
	auto pos = cvec.begin();
	for(auto it = cvec.begin(); it != cvec.end(); ++it){
		if(*it > *pos && *it <= med){
			pos = it;
		}
	}
	cvec.insert(pos+1, a);
}

void solve(){
	cvec = vec;
	
	char c;
	fin >> dstr;
	fin.getline(line, N);
	
	char *p = strtok(line, ", ");
	while(p != NULL){
		int len = strlen(p);
		if(len == 1){
			if(p[0] == 'P'){
				ipop();
			}else if(p[0] == 'L'){
				ideletesmallest();
			}else if(p[0] == 'H'){
				ideletebiggest();
			}
		}else if(len >= 2){
			int val = stringtoint(find(p, p+len, '(')+1, find(p, p+len, ')'));
			if(p[0] == 'P'){
				ipush(val);
			}else if(p[0] == 'L'){
				iinsertbelowsmallest(val);
			}else if(p[0] == 'H'){
				iinsertabovebiggest(val);
			}else if(p[0] == 'M'){
				iinsertmedian(val);
			}
		}
		p = strtok(NULL, ", ");
	}
}

void write(){
	for(auto a : cvec){
		cout << a;
	}
	cout << "\n";
}

int main(){
	// ios_base::sync_with_stdio(false);
	read();
	for(int acsl = 0; acsl < 10; ++acsl){
		solve();
		write();
	}
	return 0;
}