#include <iostream>
#include <cstdio>
using namespace std;
int n, pi, pj;
int a[100][100];
void zerozare()
{
    for(int i=1; i<=n; i++)
        for(int j=1; j<=n; j++)
            a[i][j]=0;
}
void creare_patrat(int i, int j, int k)
{
    if(k>n*n)
        return;
    a[i][j]=k;
    if(i+1>n)
    {
        if(j+1>n)
        {
            if(a[1][1]==0)
            {
                creare_patrat(1, 1, k+1);
                return;
            }
            creare_patrat(2, j, k+1);
            return;
        }
        if(a[1][j+1]==0)
        {
            creare_patrat(1, j+1, k+1);
            return;
        }
        creare_patrat(2, j, k+1);
        return;
    }
    if(j+1>n)
    {
        if(a[i+1][1]==0)
        {
            creare_patrat(i+1, 1, k+1);
            return;
        }
        if(i+2>n)
        {
            creare_patrat(1, j, k+1);
            return;
        }
        creare_patrat(i+2, j, k+1);
    }
    if(a[i+1][j+1]==0)
    {
        creare_patrat(i+1, j+1, k+1);
        return;
    }
    if(i+2>n)
    {
        creare_patrat(1, j, k+1);
        return;
    }
    creare_patrat(i+2, j, k+1);
}
void afisare()
{
    for(int i=1; i<=n; i++)
    {
        for(int j=1; j<=n; j++)
            cout<<a[i][j]<<' ';
        cout<<'\n';
    }
}
int main()
{
    freopen("aggripa.in", "r", stdin);
    for(int acsl=0; acsl<10; acsl++)
    {
        zerozare();
        scanf("%d, %d, %d\n", &n, &pi, &pj);
        creare_patrat(n/2+2, n/2+1, 1);
        printf("%d\n", a[pi][pj]);
    }
    return 0;
}
