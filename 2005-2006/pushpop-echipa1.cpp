#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <algorithm>

using namespace std;

string date;
vector<int> elementeInit;
vector<int> structura;

int celMaiMic(){
    int mini = 0;
    for (int i = 1; i < structura.size(); ++i) {
        if(structura[i] < structura[mini])
            mini = i;
    }
    return mini;
}

int celMaiMare(){
    int maxi = 0;
    for (int i = 1; i < structura.size(); ++i) {
        if(structura[i] >= structura[maxi])
            maxi = i;
    }
    return maxi;
}

int median(){
    vector<int> copie;
    for (int i = 0; i < structura.size(); ++i) {
        copie.push_back(structura[i]);
    }
    sort(copie.begin(), copie.end());
    int median = 0;
    if(copie.size() % 2 == 0){
        median = copie[copie.size() / 2 - 1];
        median += copie[copie.size() / 2];
        median /= 2;
    }else median = copie[copie.size() / 2];
    int maxi = 0;
    for (int i = 0; i < structura.size(); ++i) {
        if(structura[i] > structura[maxi] && structura[i] <= median)
            maxi = i;
    }
    return maxi;
}


int main() {
    ifstream f("date.in");
    getline(f, date);
    for (int i = 0; elementeInit.empty() || elementeInit.back() != 0; ++i) {
        if(isdigit(date[i]))
            elementeInit.push_back(date[i] - '0');
    }
    if(!elementeInit.empty())
        elementeInit.pop_back();
    for (int acsl = 0; acsl < 10; ++acsl) {
        getline(f, date);
        structura.clear();
        for (int i = 0; i < elementeInit.size(); ++i) {
            structura.push_back(elementeInit[i]);
        }
        bool stackOrQ = false;
        if(date[0] == 'Q')
            stackOrQ = true;
        for (int i = 1; i < date.length(); ++i) {
            switch(date[i]){
                case 'P': {
                    if(date[i + 1] == '('){
                        int numar = 0;
                        for(i += 2; date[i] != ')'; ++i){
                            numar = numar*10 + (int)(date[i] - '0');
                        }
                        structura.push_back(numar);
                    }else{
                        if(stackOrQ){
                            structura.erase(structura.begin());
                        }else{
                            structura.pop_back();
                        }
                    }
                    break;
                }
                case 'L': {
                    int pos = celMaiMic();
                    if(date[i + 1] == '('){
                        int numar = 0;
                        for(i += 2; date[i] != ')'; ++i){
                            numar = numar*10 + (int)(date[i] - '0');
                        }

                        structura.insert(structura.begin() + pos, numar);
                    }else{
                        structura.erase(structura.begin() + pos);
                    }
                    break;
                }
                case 'H': {
                    int pos = celMaiMare();
                    if(date[i + 1] == '('){
                        int numar = 0;
                        for(i += 2; date[i] != ')'; ++i){
                            numar = numar*10 + (int)(date[i] - '0');
                        }

                        structura.insert(structura.begin() + pos + 1, numar);
                    }else{
                        structura.erase(structura.begin() + pos);
                    }
                    break;
                }
                case 'M': {
                    int pos = median();
                    if(date[i + 1] == '('){
                        int numar = 0;
                        for(i += 2; date[i] != ')'; ++i){
                            numar = numar*10 + (int)(date[i] - '0');
                        }

                        structura.insert(structura.begin() + pos + 1, numar);
                    }else{
                        structura.erase(structura.begin() + pos);
                    }
                    break;
                }
            }
        }
        for (int i = 0; i < structura.size(); ++i) {
            cout<<structura[i];
        }
        cout<<"\n";
    }
    return 0;
}
