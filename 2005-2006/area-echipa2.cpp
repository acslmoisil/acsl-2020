#include <iostream>
#include <fstream>
#include <queue>

using namespace std;

struct vec2{
	int x, y;
};

const int N = 141;

ifstream fin("area.in");

vec2 size;
int vi[N][N];
void nuke(){
	for(int y = 0; y <= size.y; ++y){
		for(int x = 0; x <= size.x; ++x){
			vi[x][y] = 0;
		}
	}
}

vec2 cv(int a){
	a--;
	return {a%size.x, a/size.x};
}

int cv(vec2 v){
	return v.y*size.x+v.x+1;
}

bool inb(const vec2 &v){
	return (v.x >= 0 && v.x < size.x && v.y >= 0 && v.y < size.y);
}

void read(){
	int n;
	char c;
	fin >> size.x >> c >> size.y >> c >> n;
	nuke();
	for(int i = 0; i < n; ++i){
		int a;
		fin >> c >> a;
		vec2 v = cv(a);
		vi[v.x][v.y] = 1;
	}
}

void debug(){
	for(int y = size.y-1; y >= 0; --y){
		for(int x = 0; x < size.x; ++x){
			cout << vi[x][y] << "\t";
		}
		cout << "\n";
	}
	cout << "\n";
}

vec2 dv[] = {{0, 1}, {1, 1}, {1, 0}, {1, -1}, {0, -1}, {-1, -1}, {-1, 0}, {-1, 1}};
queue<vec2> qu;
double parkour(vec2 a){
	double area = 0;
	vi[a.x][a.y] = 2;
	qu.push(a);
	while(!qu.empty()){
		a = qu.front();qu.pop();
		
		//squares
		for(int i = 0; i < 8; i += 2){
			vec2 d1 = dv[i];
			vec2 d2 = dv[(i+1)%8];
			vec2 d3 = dv[(i+2)%8];
			
			vec2 n1 = {a.x+d1.x, a.y+d1.y};
			vec2 n2 = {a.x+d2.x, a.y+d2.y};
			vec2 n3 = {a.x+d3.x, a.y+d3.y};
			if(inb(n1) && inb(n2) && inb(n3)){
				int empties = (vi[n1.x][n1.y] == 0) + (vi[n2.x][n2.y] == 0) + (vi[n3.x][n3.y] == 0);
				bool enemy = (vi[n1.x][n1.y] == 1) || (vi[n2.x][n2.y] == 1) || (vi[n3.x][n3.y] == 1);
				if(!enemy && empties >= 1){
					area += 1;
					vi[n1.x][n1.y] = 2;
					qu.push(n1);
					vi[n2.x][n2.y] = 2;
					qu.push(n2);
					vi[n3.x][n3.y] = 2;
					qu.push(n3);
					// cout << cv(a) << " " << cv(n1) << " " << cv(n2) << " " << cv(n3) << "\n";
					// debug();
				}
			}
		}
		
		//triangles
		for(int i = 0; i < 8; ++i){
			vec2 d1 = dv[i];
			vec2 d2 = dv[(i+1)%8];
			
			vec2 n1 = {a.x+d1.x, a.y+d1.y};
			vec2 n2 = {a.x+d2.x, a.y+d2.y};
			if(inb(n1) && inb(n2)){
				int empties = (vi[n1.x][n1.y] == 0) + (vi[n2.x][n2.y] == 0);
				bool enemy = (vi[n1.x][n1.y] == 1) || (vi[n2.x][n2.y] == 1);
				if(!enemy && empties >= 1){
					area += 0.5;
					if(empties >= 2){
						if(vi[n1.x][n1.y] == 0){
							vi[n1.x][n1.y] = 2;
							qu.push(n1);
						}
						if(vi[n2.x][n2.y] == 0){
							vi[n2.x][n2.y] = 2;
							qu.push(n2);
						}
					}
					// cout << cv(n1) << " " << cv(n2) << " " << cv(a) << "\n";
					// debug();
				}
			}
		}
	}
	return area;
}

void solve(){
	double ans = 0;
	for(int y = 0; y < size.y; ++y){
		for(int x = 0; x < size.x; ++x){
			if(vi[x][y] == 0){
				ans = max(ans, parkour({x, y}));
			}
		}
	}
	cout << ans << "\n";
}

int main(){
	// ios_base::sync_with_stdio(false);
	for(int ascl = 0; ascl < 10; ++ascl){
		read();
		solve();
	}
	return 0;
}