#include <iostream>
#include <fstream>
#include <map>

using namespace std;

ifstream f("data.in");

map<char, string> biti;

void assign(){
    biti['0'] = "0000";
    biti['1'] = "0001";
    biti['2'] = "0010";
    biti['3'] = "0011";
    biti['4'] = "0100";
    biti['5'] = "0101";
    biti['6'] = "0110";
    biti['7'] = "0111";
    biti['8'] = "1000";
    biti['9'] = "1001";
    biti['A'] = "1010";
    biti['B'] = "1011";
    biti['C'] = "1100";
    biti['D'] = "1101";
    biti['E'] = "1110";
    biti['F'] = "1111";
}

string transform(string x){
    string res = "";
    for(int i = 0; i < x.length(); ++i){
        res += biti[x[i]];
    }
    return res;
}

string xor1(string s1, string s2){
    string res = "";
    for (int i = 0; i < s1.length(); ++i) {
        if(s1[i] != s2[i])
            res += '1';
        else res += '0';
    }
    return res;
}

int significants(string s){
    for (int i = 0; i < s.length(); ++i) {
        if(s[i] == '1')
            return i;
    }
    return s.length();
}

void doit(string fi, string se){
    int sign1 = 0;
    int sign2 = 0;
    for(int i = 0; i < fi.length(); ++i){
        sign1 = i;
        if(fi[i] == '1') break;
    }
    for(int i = 0; i < se.length(); ++i){
        sign2 = i;
        if(se[i] == '1') break;
    }
    fi = fi.substr(sign1);
    se = se.substr(sign2);
    for(int i = 0; i + se.length() - 1< fi.length();){
        string r = xor1(fi.substr(i, se.length()), se);
        int s = significants(r);
        for (int j = 0; j < r.length(); ++j) {
            fi[i + j] = r[j];
        }
        i+=s;
    }
    cout<<fi.substr(fi.length() - se.length() - significants(se) + 1)<<"\n";
}

int main() {
    assign();
    for (int acsl = 0; acsl < 10; ++acsl) {
        string numere;
        getline(f, numere);
        int v = numere.find(',');
        string fi = numere.substr(0, v);
        fi = transform(fi);
        string se = numere.substr(v + 2);
        se = transform(se);
        doit(fi, se);
    }
    return 0;
}
