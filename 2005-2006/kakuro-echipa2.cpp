#include <iostream>
#include <fstream>

using namespace std;

struct vec2{
	int x, y;
};

const int N = 141;

ifstream fin("kakuro.in");

int mat[N][N];
int line[N], col[N];
bool linefrec[N][14], colfrec[N][14];

vec2 size;

vec2 cv(int a){
	a--;
	return {a%size.x, a/size.x};
}

void read(){
	char c;
	fin >> size.y >> c >> size.x;
	
	int n;
	fin >> n;
	for(int i = 0; i < n; ++i){
		int a;fin >> c >> a;
		vec2 v = cv(a);
		mat[v.x][v.y] = -1;
	}
	
	int a, b;
	char w;
	fin >> a >> c >> b;
	while(a != 0 || b != 0){
		fin >> w >> c;
		vec2 v = cv(a);
		if(w == 'A'){
			line[v.y] = b;
		}else{
			col[v.x] = b;
		}
		mat[v.x][v.y] = -1;
		fin >> a >> c >> b;
	}
}

bool bt(int x = 0, int y = 0){
	if(x == size.x){
		return bt(0, y+1);
	}
	if(y == size.y){
		for(int y = 0; y < size.y; ++y){
			if(line[y] != 0){
				return false;
			}
		}
		for(int x = 0; x < size.x; ++x){
			if(col[x] != 0){
				return false;
			}
		}
		return true;
	}
	if(mat[x][y] == -1){
		return bt(x+1, y);
	}
	for(int i = min(min(line[y], col[x]), 9); i >= 1; --i){
		if(!linefrec[y][i] && !colfrec[x][i]){
			linefrec[y][i] = colfrec[x][i] = true;
			mat[x][y] = i;
			line[y] -= i;
			col[x] -= i;
			if(bt(x+1, y)){
				return true;
			}
			linefrec[y][i] = colfrec[x][i] = false;
			mat[x][y] = 0;
			line[y] += i;
			col[x] += i;
		}
	}
	return false;
}

void write(){
	int a;
	for(int i = 0; i < 10; ++i){
		fin >> a;
		vec2 v = cv(a);
		cout << mat[v.x][v.y] << "\n";
	}
}

void debug(){
	for(int y = 0; y < size.y; ++y){
		for(int x = 0; x < size.x; ++x){
			cout << mat[x][y] << "\t";
		}
		cout << "\n";
	}
	cout << "\n";
}

int main(){
	// ios_base::sync_with_stdio(false);
	read();
	bt();
	write();
	return 0;
}