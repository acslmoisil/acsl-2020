#include <cstdio>
#include <iostream>
#include <queue>
#include <iomanip>

using namespace std;

int a[100][100];
int n, m, k;

const int dx[] = {1, 1, -1, -1};
const int dy[] = {-1, 1, 1, -1};

typedef pair<int, int> pii;

pii coord(int x){
    int lin = n-(x-1)/m;
    int col = (x-1)%m+1;
    return make_pair(lin, col);
}

void bordare(){
    for(int i=0; i<=n+1; i++)
        a[i][0] = a[i][m+1] = 2;
    for(int j=0; j<=m+1; j++)
        a[0][j] = a[n+1][j] = 2;

}

void zeroz_fara_poligon(){
    for(int i=0; i<=n+1; i++)
        for(int j=0; j<=m+1; j++)
            if(a[i][j]<2)
                a[i][j]=0;
}
queue<pii>Q;
void afisare(double s){
    queue<pii>copie;
    cout<<s<<endl;
    while(!Q.empty()){
        copie.push(Q.front());
        cout<<"{"<<Q.front().first<<' '<<Q.front().second<<"}";
        Q.pop();
    }cout<<endl;
    while(!copie.empty()){
        Q.push(copie.front());
        copie.pop();
    }

    for (int i = 1; i <= n; ++i) {
        for (int j = 1; j <= m; ++j) {
            cout<<a[i][j]<<' ';
        }
        cout<<endl;
    }
    cout<<endl;
}

double backt(pii st)
{
    while(!Q.empty())
        Q.pop();
    Q.push(st);
    a[st.first][st.second] = 1;
    double s=0;
    int ok=0;
    for(int v=0; v<4 && ok == 0; v++){
        int ix = st.first+dx[v];
        int jx = st.second+dy[v];

        if(a[ix][jx] == 0 && a[ix][st.second] == 0 && ok == 0){
            ok = 1;
            s=0.5;
            a[ix][jx] = a[ix][st.second] = 1;
            Q.push(make_pair(ix, st.second));
            Q.push(make_pair(ix, jx));

        }

    }
//    afisare(s);
    if(ok == 0)
        return 0;
    while(!Q.empty()){
        pii cur = Q.front();
        Q.pop();

        for(int v=0; v<4; v++){
            int ix = cur.first+dx[v];
            int jx = cur.second+dy[v];

            if(a[ix][jx] != 0 || (a[ix][cur.second]==2 && a[cur.first][jx] == 2))
                continue;
            bool pus = false;
            if(a[ix][cur.second] == 1 && a[cur.first][jx] == 1){
                s+=0.5;
                pus = true;
                a[ix][jx] = 1;
                Q.push(make_pair(ix, jx));
            }
            else if(a[ix][cur.second] == 1 && a[cur.first][jx] == 0){
                s+=0.5;
                pus = true;
                a[ix][jx] = 1;
                Q.push(make_pair(ix, jx));
            }else if(a[ix][cur.second] == 0 && a[cur.first][jx] == 1){
                s+=0.5;
                pus = true;
                a[ix][jx] = 1;
                Q.push(make_pair(ix, jx));
            }
            else if(a[ix][cur.second] == 2 && a[cur.first][jx] == 1){
                s+=0.5;
                pus = true;
                a[ix][jx] = 1;
                Q.push(make_pair(ix, jx));
            }
            else if(a[ix][cur.second] == 1 && a[cur.first][jx] == 2){
                s+=0.5;
                pus = true;
                a[ix][jx] = 1;
                Q.push(make_pair(ix, jx));
            }
            if(pus){
                if((cur.second < m - 1 && dy[v] > 0) || (cur.second >= 3 && dy[v] < 0)){
                    bool ok1 = true;
                    for(int j = 0; j < 3 && j > -3; j+=dy[v]){
                        ok1 = (a[cur.first][cur.second + j] == 1) && ok1;
                    }
                    if(ok1)
                        s += 0.5;
                }
                if((cur.first < n - 1 && dx[v] > 0) || (cur.first >= 3 && dx[v] < 0)){
                    bool ok1 = true;
                    for (int j = 0; j < 3 && j > -3; j+=dx[v]) {
                        ok1 = (a[cur.first + j][cur.second] == 1) && ok1;
                    }
                    if(ok1)
                        s += 0.5;
                }

            }
        }
//        afisare(s);
    }
    return s;
}

double luam_fiecare_punct(){
    double smax = 0;
    for(int i=1; i<=n; i++)
        for(int j=1; j<=m; j++)
            if(a[i][j]==0){
                smax = max(backt(make_pair(i, j)), smax);
                zeroz_fara_poligon();
            }
    return smax;
}

void zeroz(){
    for(int i=0; i<=n+1; i++)
        for(int j=0; j<=m+1; j++)
            a[i][j]=0;
}
void citire(){
    freopen("data.in", "r", stdin);
    for(int acsl=0; acsl<10; acsl++){
        zeroz();
        scanf("%d, %d, %d", &m, &n, &k);
        int x;
        for(int i=0; i<k; i++) {
            scanf(", %d", &x);
            pii co = coord(x);
            a[co.first][co.second]=2;
        }
        bordare();
        cout<<luam_fiecare_punct()<<'\n';
//        break;
    }
}


int main() {
    citire();
    return 0;
}
