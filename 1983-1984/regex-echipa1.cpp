#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stack>
#include <map>

using namespace std;

ifstream fin("regex.in");

typedef string::iterator sit;
int limit;

struct Node{
	vector<Node*> next;
	multimap<char, Node*> nextchar;
	void generate(string s){
		stack<Node*> steve;
		stack<Node*> stever;
		stack<Node*> prever;

		Node *p = this;
		Node *prev;
		Node *popped = nullptr;
		steve.push(this);
		stever.push(new Node());
		prever.push(this);

		char pc;
		for(int i = 0; i < s.size(); ++i){
			char c = s[i];
			if(c == '('){
				Node *n = new Node();
				steve.push(n);
				p->next.push_back(n);
				stever.push(new Node());
				
				p = n;
				prever.push(p);
			}else if(c == ')'){
				p->next.push_back(stever.top());
				p = stever.top();
				stever.pop();
				
				popped = steve.top();
				steve.pop();
				
				prever.pop();
			}else if(c == '*'){
				if(pc != ')'){
					p->nextchar.insert({pc, p});
				}else{
					p->next.push_back(popped);
				}
				prever.top()->next.push_back(p);
			}else if(c == 'U'){
				p->next.push_back(stever.top());
				p = steve.top();
				
				prever.pop();
				prever.push(p);
			}else{
				Node *n = new Node();
				p->nextchar.insert({c, n});
				
				prever.pop();
				prever.push(p);
				p = n;
			}
			pc = c;
		}
	}
	int parkour(sit lt, sit rt, int d=0){
		int maxi = 0;
		if(lt != rt && d < limit){
			auto range = nextchar.equal_range(*lt);
			for(auto it = range.first; it != range.second; it++){
				maxi = max(maxi, it->second->parkour(lt+1, rt, d+1)+1);
			}
			for(auto n : next){
				maxi = max(maxi, n->parkour(lt, rt, d+1));
			}
		}
		return maxi;
	}
};
Node *root;

string reggie, stringy;

void read(){
	fin >> reggie >> stringy;
	reggie.pop_back();
}

void solve(){
	root = new Node();
	root->generate(reggie);
	limit = max(reggie.size(), stringy.size())*2;
	int v = root->parkour(stringy.begin(), stringy.end());
	if(v == stringy.size()){
		cout << "Yes";
	}else{
		cout << "No, " << v;
	}
	cout << "\n";
}

int main(){
	// ios_base::sync_with_stdio(false);
	for(int acsl = 0; acsl < 10; ++acsl){
		read();
		solve();
	}
	return 0;
}