#include <iostream>
#include <fstream>
#include <string>

using namespace std;

ifstream fin("card.in");

bool mat[14][14];

int chartoint(char c){
	if(c >= '0' && c <= '9'){
		return c-'0';
	}else{
		return c-'A'+10;
	}
}

void read(){
	char c;
	for(int i = 0; i < 9; ++i){
		if(i > 0){
			fin >> c;
		}
		for(int j = 0; j < 3; ++j){
			fin >> c;
			int a = chartoint(c);
			for(int k = 0; k < 4; ++k){
				mat[i][j*4+k] = a&1;
				a >>= 1;
			}
		}
	}
}

int susta(){
	int r = 0;
	for(int j = 0; j < 12; ++j){
		int count = 0;
		for(int i = 3; i < 9; ++i){
			if(mat[i][j]){
				count++;
			}
		}
		if(count >= 1){
			r = j;
		}
	}
	return r;
}

void solve(){
	for(int i = 0; i < 3; ++i){
		int ans;
		int count = 0;
		for(int j = 0; j < 12; ++j){
			if(mat[i][j]){
				count++;
				ans = 9-j;
			}
		}
		if(count == 1 && ans >= 0){
			cout << ans;
		}else{
			cout << "*";
		}
	}
	cout << ", ";
	
	int len = susta();
	for(int j = 0; j <= len; ++j){
		char ans;
		int count = 0;
		for(int i = 3; i < 9; ++i){
			if(mat[i][j]){
				ans = 'A'+i-3;
				count++;
			}
		}
		if(count == 1){
			cout << ans;
		}else{
			cout << "*";
		}
		if(j == 0){
			cout << ". ";
		}
	}
	cout << "\n";
}

void deb(){
	for(int i = 3; i < 9; ++i){
		for(int j = 0; j < 12; ++j){
			cout << mat[i][j];
		}
		cout << "\n";
	}
	cout << "\n";
}

int main(){
	// ios_base::sync_with_stdio(false);
	for(int acsl = 0; acsl < 5; ++acsl){
		read();
		solve();
		// deb();
	}
	return 0;
}