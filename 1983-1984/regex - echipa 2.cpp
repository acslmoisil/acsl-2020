#include <iostream>
#include <regex>
#include <string>
#include <fstream>
#include <vector>

using namespace std;

struct node{
    char val;
    node *par;
    vector<node*> unde;
    node(node* p){
        this->val = '\0';
        this->par = p;
    }
    node(char a, node* p){
        this->val = a;
        this->par = p;
    }
    void deletus(){
        if(this->unde.size() > 0){
            for (int i = this->unde.size() - 1; i >= 0; --i) {
                if(this->unde[i] != this) {
                    this->unde[i]->deletus();
                    delete this->unde[i];
                    this->unde.pop_back();
                }
            }
        }
        delete this;
    }
};

void conversie(string chestie, node* root){
    node *curent = root;
    vector<node*> paranteze;
    vector<pair<node*, node*>> stele;
    bool sterge = false;
    bool verificat = false;
    for(int i = 0; i < chestie.length(); ++i){
        if(sterge && verificat && !paranteze.empty()){
            sterge = verificat = false;
            paranteze.pop_back();
        }else if(sterge && verificat) sterge = verificat = false;
        switch(chestie[i]){
            case '(': {
                if(sterge && !paranteze.empty()) {
                    sterge = verificat = false;
                    paranteze.pop_back();
                }else if(sterge) sterge = verificat = false;
                curent->unde.push_back(new node(curent));
                curent = curent->unde.back();
                paranteze.push_back(curent);
                break;
            }
            case '1': {
                curent->unde.push_back(new node('1', curent));
                curent = curent->unde.back();
                break;
            }
            case '0': {
                curent->unde.push_back(new node('0', curent));
                curent = curent->unde.back();
                break;
            }
            case ')': {
                sterge = true;
                node *p = curent;
                while(p->par != paranteze.back()){
                    node *t = p;
                    p->par = paranteze.back();
                    p = t;
                }
                break;
            }
            case '*': {
                if(sterge){
                    verificat = true;
                    curent->unde.push_back(paranteze.back());
                    stele.push_back(make_pair(paranteze.back(), curent));
                }else{
                    curent->unde.push_back(curent);
                    curent = curent->par;
                }
                break;
            }
            case '|': {
                if(sterge){
                    verificat = true;
                    curent = paranteze.back();
                }else if(curent->par->val == '\0') curent = curent->par->par;
                else curent = curent->par;
            }
        }

    }
    for (int i = 0; i < stele.size(); ++i) {
        for (int j = 0; j < stele[i].second->unde.size(); ++j) {
            if(stele[i].second->unde[j] != stele[i].first)
                stele[i].first->par->unde.push_back(stele[i].second->unde[j]);
        }
    }
}

int longest(node* curent, string curnt, string &matcher){
    int length = curnt.length();
//    cout<<curnt<<"\t";
    if(length > matcher.length())
        return 0;
    for (int i = 0; i < curent->unde.size(); ++i) {
//        cout<<curent->unde[i]->val<<'\t';
        if(curent->unde[i]->val == '\0' || curent->unde[i]->val == 1){
            length = max(length, longest(curent->unde[i], curnt, matcher));
        }else if(curnt + curent->unde[i]->val == matcher.substr(0, curnt.length() + 1)){
//            cout<<curnt + curent->unde[i]->val<<" retardat"<< matcher.substr(0, length + 1)<<"\t";
            length = max(length + 1, longest(curent->unde[i], curnt + curent->unde[i]->val, matcher));
        }
    }
    return length;
}

int main() {
    ifstream f("regex.in");
    for (int acsl = 0; acsl < 10; ++acsl) {
        string reg, rez;
        getline(f, reg, ',');
        getline(f, rez);
        rez = rez.substr(1);
        for (int i = 0; i < reg.length(); ++i)
            if(reg[i] == 'U') reg[i] = '|';
        regex r(reg);
        if(regex_match(rez, r)){
            cout<<"Yes"<<"\n";
        }else{
            node *root = new node(1,nullptr);
            for (int i = 0; i < reg.size(); ++i) {
                if(reg[i] == '*' && reg[i - 1] != ')'){
                    reg.insert(i - 1, "(");
                    reg.insert(i + 1, ")");
                }
            }
            conversie(reg, root);
            cout<<"No, "<<longest(root, "", rez)<<'\n';
        }

    }
    return 0;
}
